package com.meelo2.mobi;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.meelo2.mobi.GUI.CustomImageView;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.Utils.Rest;;
import com.meelo2.mobi.activities.MainActivity;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.HangoutLocation;
import com.squareup.picasso.Picasso;

/**
 * Created by jzvia on 29/08/2017.
 */

public class NearbyLocationsAdapter extends GeneralAdapter<HangoutLocation>{

    private final static int radius = 100;

    private int selectedPosition = -1;


    public NearbyLocationsAdapter(Context context, ItemClickedCallback itemClickedCallback, DataLoadedCallback dataLoadedCallback, final LatLng latLng){

        super(HangoutLocation.class, context, itemClickedCallback, dataLoadedCallback, "?radius=" + radius + "&lat=" + latLng.latitude +
                "&lng="+latLng.longitude);

    }


        class LocationAdapterHolder extends RecyclerView.ViewHolder{

            public TextView locationName;
            public CustomImageView locationImage;
            public ImageView checkInImage;
            //public TextView locationDistance;
            public LocationAdapterHolder(View itemView) {
                super(itemView);
            }

        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolderImpl(final ViewGroup parent, int viewType) {


            return new LocationAdapterHolder(LayoutInflater.from(context).inflate(R.layout.home_screen_nearby_location_item,parent, false)){
                {
                    locationName = (TextView) itemView.findViewById(R.id.text_view_location_name);
                    //locationDistance = (TextView) itemView.findViewById(R.id.text_view_location_distance);
                    locationImage = (CustomImageView) itemView.findViewById(R.id.image_view_location_logo);
                    checkInImage = itemView.findViewById(R.id.image_view_check_in);
                    //checkInImage.setMaskResId(R.drawable.small_picture_icon_frame);

                }
            };

        }

        @Override
        public void onBindViewHolderImpl(RecyclerView.ViewHolder holder, final int position) {
            final HangoutLocation hangoutLocation = (HangoutLocation) entities.get(position);
            ((LocationAdapterHolder)holder).locationName.setText(hangoutLocation.getName());
            //((LocationAdapterHolder)holder).locationDistance.setText(String.format("%.1f", hangoutLocation.getDistance()) + " KM");
           // ((LocationAdapterHolder)holder).locationImage.setImageResource(hangoutLocation.getImageResourceId());

            if (position == selectedPosition){
                ((LocationAdapterHolder)holder).checkInImage.setVisibility(View.VISIBLE);
                ((LocationAdapterHolder)holder).checkInImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Thread.runOnExecutor(new Runnable() {
                            @Override
                            public void run() {
                                Rest.getInstance().checkIn(context, hangoutLocation.getId());
                            }
                        });

                        //UIcontroller.getInstance().checkIn();
                    }
                });

            }else{
                ((LocationAdapterHolder)holder).checkInImage.setVisibility(View.GONE);
            }


            holder.itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.d("TAG", "onTouch: ");
                    if (event.getAction() == MotionEvent.ACTION_DOWN){
                        GUI.scaleTo(v,1.1f, MainActivity.fastAnimationDuration, null);
                    }
                    else {
                        GUI.scaleTo(v,1.00f, MainActivity.fastAnimationDuration, null);
                    }
                    return false;
                }
            });



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPosition = position;
                    notifyDataSetChanged();
                    Thread.runOnExecutor(new Runnable() {
                        @Override
                        public void run() {
                            Thread.sleep(1500);
                            Thread.runOnUi(new Runnable() {
                                @Override
                                public void run() {
                                    selectedPosition = -1;
                                    notifyDataSetChanged();
                                }
                            });
                        }
                    });
                }
            });

            ((LocationAdapterHolder)holder).locationImage.setMaskResId(R.drawable.small_picture_icon_frame);
            //GUI.loadRoundedCornersImage(hangoutLocation.getImageUrl(), ((LocationAdapterHolder)holder).locationImage, GUI.Quality.HALF_WIDTH_565,((LocationAdapterHolder)holder).locationImage.getLayoutParams().width /2,((LocationAdapterHolder)holder).locationImage.getLayoutParams().width,((LocationAdapterHolder)holder).locationImage.getLayoutParams().height);
            Picasso.with(context).load(hangoutLocation.getImageUrl()).into(((LocationAdapterHolder)holder).locationImage);

            //Picasso.with(context).load(hangoutLocation.getImageUrl()).into(((LocationAdapterHolder)holder).locationImage);

            Log.d("TAG", "onBindViewHolder: "  +  hangoutLocation.getImageUrl());




        }


}
