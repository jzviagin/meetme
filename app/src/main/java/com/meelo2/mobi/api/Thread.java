package com.meelo2.mobi.api;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by jzvia on 05/09/2017.
 */

public class Thread {


    static class myThreadFactory implements ThreadFactory{
        @Override
        public java.lang.Thread newThread(@NonNull Runnable r) {
            return new java.lang.Thread(r, "my_thread_" + threadCount);
        }
    }

    //private static HandlerThread handlerThread;// = new HandlerThread("t1");
    //private static Handler handler ;//= new Handler(handlerThread.getLooper());
    private final static ExecutorService threadPoolExecutor = Executors.newCachedThreadPool(new myThreadFactory());


    public static void init(){

       // handlerThread = new HandlerThread("t2");
        //handlerThread.start();
       // handler = new Handler(handlerThread.getLooper());
        //threadCount.set(0);

    }


    private  static AtomicInteger threadCount = new AtomicInteger(0);


    public static void runOnExecutor(final Runnable runnable){
        /*if (java.lang.Thread.currentThread().getId() == handler.getLooper().getThread().getId()){
            runnable.run();
        }else{
            handler.post(runnable);
        }*/



        threadPoolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                //Log.d("TAG", "runOnExecutor: number of threads start" + threadCount);
                threadCount.incrementAndGet();
                runnable.run();
                threadCount.decrementAndGet();
                //Log.d("TAG", "runOnExecutor: number of threads done" + threadCount);

            }
        });




    }



    public static void runOnUi(Runnable runnable){
        if (java.lang.Thread.currentThread().getId() == Looper.getMainLooper().getThread().getId()){
            runnable.run();
        }else{
            new Handler(Looper.getMainLooper()).post(runnable);

        }

    }

    public static void waitAndRunOnUi(final Runnable runnable, final long time){
        runOnExecutor(new Runnable() {
            @Override
            public void run() {
                sleep(time);
                runOnUi(runnable);
            }
        });
    }

    public static void sleep(long millis){
        try{
            java.lang.Thread.sleep(millis);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }




}
