package com.meelo2.mobi.api;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jzvia on 31/07/2018.
 */

public class FacebookApi {

    public static void permissionsRevoked(final Set<String> currentPermissions, final Runnable onChanged, final Runnable onNotChanged){

        System.out.println("token: " + AccessToken.getCurrentAccessToken().getToken());
        System.out.println("token: " + AccessToken.getCurrentAccessToken().getUserId());

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),


                AccessToken.getCurrentAccessToken().getUserId() + "/permissions",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            final JSONArray jsonArray = response.getJSONObject().getJSONArray("data");

                            System.out.println(jsonArray);

                            Set<String> updatedPermissions = new HashSet<>();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                String permission = jsonArray.getJSONObject(i).getString("permission");
                                String granted = jsonArray.getJSONObject(i).getString("status");
                                if (granted.equals("granted")) {
                                    updatedPermissions.add(permission);
                                }
                            }

                            if (updatedPermissions.containsAll(currentPermissions)) {
                                onNotChanged.run();
                            } else {
                                onChanged.run();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }
}
