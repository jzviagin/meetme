package com.meelo2.mobi.api;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.amazonaws.util.IOUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jzvia on 02/12/2017.
 */

public class Var {


    public static double parseDouble(Object o)
    {
        if (o instanceof Number)
        {
            return ((Number)o).doubleValue();
        }
        try
        {
            return Double.parseDouble(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return 0;
        }
        catch(NullPointerException ex)
        {
            return 0;
        }
    }

    public static float parseFloat(Object o)
    {
        if (o instanceof Number)
        {
            return ((Number)o).floatValue();
        }
        try
        {
            return Float.parseFloat(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return 0;
        }
        catch(NullPointerException ex)
        {
            return 0;
        }
    }

    public static long parseLong(Object o)
    {
        if (o instanceof Number)
        {
            return ((Number)o).longValue();
        }
        try
        {
            return Long.parseLong(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return 0;
        }
        catch(NullPointerException ex)
        {
            return 0;
        }
    }

    public static int parseInt(Object o)
    {
        if (o instanceof Number)
        {
            return ((Number)o).intValue();
        }
        try
        {
            return Integer.parseInt(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return 0;
        }
        catch(NullPointerException ex)
        {
            return 0;
        }
    }

    public static Integer[] parseIntArray(Object[] o)
    {
        Integer[] ret = new Integer[o.length];
        for (int i = 0 ; i < ret.length ; i++)
        {
            ret[i] = parseInt(o[i]);
        }
        return ret;
    }

    public static boolean parseBoolean(Object o)
    {
        if (o instanceof Boolean)
        {
            return (Boolean)o;
        }
        try
        {
            return Boolean.parseBoolean(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        catch(NullPointerException ex)
        {
            return false;
        }
    }
    public static void sendEmail(Context context, String address, String cc, String subject, String body){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("message/rfc822");
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
        if (cc != null){
            intent.putExtra(Intent.EXTRA_CC, new String[]{cc});
        }

        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        try {
            context.startActivity(Intent.createChooser(intent, "Send e-mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static void shareApp(Context context){


        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "Get MeeloApp");
        share.putExtra(Intent.EXTRA_TEXT, "Meet Your Instant Crush! Download at: \n https://play.google.com/store/apps/details?id=com.meelo.mobi");

        context.startActivity(Intent.createChooser(share, "Share MeeloApp!"));


    }


    public static File createTempImageFile(Context context) throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend = "IMAGE_" + timestamp + "_";
        File imageFile = File.createTempFile(prepend, ".jpg", context.getExternalCacheDir());
        return imageFile;
    }



    public static byte[] readFile(File file) {
        Log.d("TAG", "readFile: file" + file);
        Log.d("TAG", "readFile: size" + file.length());
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    public static void writeFile(File file, byte [] buffer){
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(buffer);
            bos.flush();
            bos.close();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }


    }

    public static void copyFile(File source, File dest){
        writeFile(dest, readFile(source));
    }

    public static void copyFile(Context context, Uri source, File dest){
        try {
            InputStream inputStream;
           // if (source.toString().contains("file:")){
           //     inputStream = new FileInputStream(source.getPath());
           // }else{
                inputStream = context.getApplicationContext().getContentResolver().openInputStream(source);
          //  }
            writeFile(dest, IOUtils.toByteArray(inputStream));

        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}
