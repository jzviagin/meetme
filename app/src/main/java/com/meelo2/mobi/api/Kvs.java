package com.meelo2.mobi.api;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.meelo2.mobi.MeeloApp;

/**
 * Created by jzvia on 05/09/2017.
 */

public class Kvs
{

    private static SQLiteOpenHelper sqLiteOpenHelper = new SQLiteOpenHelper(MeeloApp.getContext(), "GOTIT", null, 1)
    {
        @Override
        public void onCreate(SQLiteDatabase database)
        {
            database.execSQL("CREATE TABLE IF NOT EXISTS KVS (key text PRIMARY KEY, value text, type text);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion)
        {
        }
    };

    private static SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

    private static synchronized void replace(String key, Object value, String type)
    {
        Cursor cursor = db.rawQuery("SELECT * FROM KVS WHERE key = ?", new String[]{key});
        if (cursor != null && cursor.getCount() != 0){
            cursor.close();
            update(key, value, type);
        }else{
            cursor.close();
            insert(key, value , type);
        }
    }


    private static synchronized void insert(String key, Object value, String type)
    {
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();
            contentValues.put("key", key);
            contentValues.put("value", value == null ? null : value.toString());
            contentValues.put("type", type);
            db.insertOrThrow("KVS", null, contentValues);
            db.setTransactionSuccessful();
        }finally {
            db.endTransaction();
        }
    }


    private static synchronized void update(String key, Object value, String type)
    {
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();
            contentValues.put("key", key);
            contentValues.put("value", value == null ? null : value.toString());
            contentValues.put("type", type);
            db.update("KVS", contentValues, "key = ? ", new String[]{key});
            db.setTransactionSuccessful();
        }finally {
            db.endTransaction();
        }
    }

    public static void delete(String key){
        db.delete("KVS", "key = ?",new String[]{key} );
    }

    public static void put(String key, Object value)
    {
        if (value instanceof Boolean)
        {
            replace(key, value, "Boolean");
        }
        else if (value instanceof Float)
        {
            replace(key, value, "Float");
        }
        else if (value instanceof Integer)
        {
            replace(key, value, "Integer");
        }
        else if (value instanceof Long)
        {
            replace(key, value, "Long");
        }
        else
        {
            replace(key, value, "String");
        }
    }

    public static synchronized Object get(String key)
    {
        Object value = null;
        Cursor cursor = db.rawQuery("SELECT * FROM KVS WHERE key = ?", new String[]{key});
        if (cursor.getCount() == 0){
            cursor.close();
            return null;
        }
        try {
            if (cursor.moveToFirst())
            {
                if (cursor.getString(1) != null)
                {
                    switch (cursor.getString(2))
                    {
                        case "Boolean":
                            value = Var.parseBoolean(cursor.getString(1));
                            break;
                        case "Float":
                            value = Var.parseFloat(cursor.getString(1));
                            break;
                        case "Integer":
                            value = Var.parseInt(cursor.getString(1));
                            break;
                        case "Long":
                            value = Var.parseLong(cursor.getString(1));
                            break;
                        default:
                            value = cursor.getString(1);
                            break;
                    }
                }
            }
        }catch (Exception e){
            db.delete("KVS", "key = ?",new String[]{key} );
            cursor.close();
            return null;
        }
        return value;
    }

    public static void deleteDatabase(){
        db.execSQL("DELETE FROM KVS;");
    }
}
