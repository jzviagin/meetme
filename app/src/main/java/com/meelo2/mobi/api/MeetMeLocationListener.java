//package com.me.meet.meetme.api;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.Date;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by jzvia on 26/12/2016.
 */

/*public class MeetMeLocationListener implements LocationListener {

    private static MeetMeLocationListener instance = null;


    private Location location;

    private static final long locationTimeout = 1000*20;

    private LocationManager locationManager;

    private final static float minDistanceThreshold = 500;
    private final static float fixDistanceThreshold = 50;

    private Context context;

    public interface LocationCallback{
        void onLocationReceive(Location location);
    }

    private static class LocationRequest{
        LocationCallback locationFound;
        Runnable exception;
    }


    Queue<LocationRequest> requestQueue;

    private MeetMeLocationListener(Context context){

        super();
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
        //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 50, this);



        requestQueue = new ConcurrentLinkedQueue<>();
        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                while (true){
                    if (requestQueue.isEmpty() == false) {
                        handleRequest(requestQueue.remove());

                    }
                    Thread.sleep(500);
                }
            }
        });
    }
    private void handleRequest(final LocationRequest locationRequest){

            location = null;
            long startTime = new Date().getTime();
            Thread.runOnUi(new Runnable() {
                @Override
                public void run() {
                    try {
                        requestUpdates();
                    } catch (SecurityException e) {
                        e.printStackTrace();
                        locationRequest.exception.run();
                    }
                }
            });
            boolean gotLocation = false;

            while (gotLocation == false && new Date().getTime() < startTime + locationTimeout ){
                if (location != null && location.getAccuracy() < fixDistanceThreshold){
                    gotLocation = true;
                }
                Thread.sleep(50);
            }

            removeUpdates();

            if (location == null){
                Thread.runOnExecutor(locationRequest.exception);
            }else{
                Thread.runOnExecutor(new Runnable() {
                    @Override
                    public void run() {
                        locationRequest.locationFound.onLocationReceive(location);
                    }
                });
            }


    }

    private void requestUpdates() throws SecurityException{

        location = null;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, MeetMeLocationListener.this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, MeetMeLocationListener.this);


        Location networkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        float networkAccuracy = -1;
        if (networkLocation != null){
            networkAccuracy = networkLocation.getAccuracy();
        }
        Location gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        float gpsAccuracy = -1;
        if (gpsLocation != null){
            gpsAccuracy = gpsLocation.getAccuracy();
        }

        if (gpsAccuracy != -1 && (gpsAccuracy < networkAccuracy || networkAccuracy == -1 ) && gpsAccuracy < minDistanceThreshold){
            location = gpsLocation;
        }
        if (networkAccuracy != -1 && (networkAccuracy < gpsAccuracy || gpsAccuracy == -1 ) && networkAccuracy < minDistanceThreshold){
            location = gpsLocation;
        }

        if (location != null){
            location.setAccuracy(minDistanceThreshold - 1);
        }

        final FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                fusedLocationProviderClient.requestLocationUpdates()
            }
        })
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, MeetMeLocationListener.this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, MeetMeLocationListener.this);


        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, MeetMeLocationListener.this);
        //if (locationManager.getLastKnownLocation(bestProvider) != null && locationManager.getLastKnownLocation(bestProvider).getAccuracy() < minDistanceThreshold){
        //    location = locationManager.getLastKnownLocation(bestProvider);
        //    location.setAccuracy(minDistanceThreshold - 1);
       // }


    }

    private void removeUpdates(){
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                locationManager.removeUpdates(MeetMeLocationListener.this);

            }
        });
    }

    public static synchronized MeetMeLocationListener getInstance(Context context){
        if (instance == null){
            instance = new MeetMeLocationListener(context);
        }
        return instance;
    }

    public void getLocation(final LocationCallback _locationFound, final Runnable _exception){
        requestQueue.add(new LocationRequest(){{
            locationFound = _locationFound;
            exception = _exception;
        }});
        //final LatLng latLng1 = new LatLng(32.186312,34.812172);
       // if (location.getLongitude() == 0 && location.getLatitude() == 0){
        //    location.setLatitude(32.186312);
        //    location.setLongitude(34.812172);
       // }
       // return location;
    }

    @Override
    public synchronized void onLocationChanged(Location location) {
        Log.d("tag", "onLocationChanged22: ");
        if (location.getAccuracy() != 0 && location.getAccuracy() < minDistanceThreshold &&
                (location == null || location.getAccuracy()< this.location.getAccuracy())){
            this.location = location;

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
*/