package com.meelo2.mobi.api;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.Utils.Rest;
import com.meelo2.mobi.activities.MainActivity;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

public class MeetMeLocationService extends Service {
    private RequestQueue requestQueue;
    private Rest rest;
    private boolean isForeground = false;
    private Location location;
    private final LocationServiceBinder binder = new LocationServiceBinder();
    private final static float locationDistanceThreshold = 500;
    private static final long locationTimeout = 1000*20;
    private Location tempLocation;
    private final static float minDistanceThreshold = 500;
    private final static float fixDistanceThreshold = 50;
    private AtomicBoolean started = new AtomicBoolean(false);

    @Override
    public void onCreate() {
        super.onCreate();
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                moveToBackground();
            }
        }, new IntentFilter(MainActivity.checked_out_by_server_broadcast));
    }


    private void handleLocationUpdate(Location location, AtomicBoolean isDone){
        if (location != null) {
            Log.d("locationnnn", "received update:  accuracy : " + location.getAccuracy() + "" +
                    " lng " + location.getLongitude() + " lat " + location.getLatitude());
        }else{
            Log.d("locationnnn", "received update:  null ");
        }
        if (location != null && location.getAccuracy() < minDistanceThreshold &&(tempLocation == null ||  (location.getAccuracy() < tempLocation.getAccuracy())  )){
            tempLocation = location;
            if (tempLocation.getAccuracy() < fixDistanceThreshold){
                Log.d("locationnnn", "done before time ");
                isDone.set(true);
                //unregister
            }
        }
    }




    private void pollLocation(){
        try {


            Log.d("locationnn", "receiving started ");
            tempLocation = null;
            long startTime = new Date().getTime();
            final AtomicBoolean isDone= new AtomicBoolean(false);
            final LocationCallback locationCallback = new LocationCallback(){
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    //super.onLocationResult(locationResult);
                    Log.d("locationnnn", "got location pereodic update num of locations: " + locationResult.getLocations().size());
                    handleLocationUpdate(locationResult.getLastLocation(), isDone);
                }

                @Override
                public void onLocationAvailability(LocationAvailability locationAvailability) {
                    super.onLocationAvailability(locationAvailability);
                }
            };

            final FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {


                    final Location location = task.getResult();
                    handleLocationUpdate(location, isDone);
                    final LocationRequest locationRequest = new LocationRequest();
                    SettingsClient client = LocationServices.getSettingsClient(MeetMeLocationService.this);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    locationRequest.setInterval(0);
                    locationRequest.setFastestInterval(0);
                    locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
                    Task<LocationSettingsResponse> task2 = client.checkLocationSettings(builder.build());

                    //locationRequest.setExpirationDuration(locationTimeout);
                    //locationRequest.setMaxWaitTime(500);

                    task2.addOnSuccessListener( new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback , Looper.getMainLooper());

                        }


                    });

                    task2.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Intent broadcastIntent = new Intent();
                            broadcastIntent.setAction(MainActivity.location_disabled_broadcast);
                            sendBroadcast(broadcastIntent);
                        }
                    });


                }
            });

            while (isDone.get() == false && startTime + locationTimeout > new Date().getTime()){
                Thread.sleep(50);
            }
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
            if (tempLocation != null) {
                Log.d("locationnnn", "receiving location finished ");
                LatLng latLng = new LatLng(tempLocation.getLatitude(), tempLocation.getLongitude());
                //rest.query(HangoutLocation.class, "update_user_location", "?lat=" + latLng.latitude +
                 //       "&lng=" + latLng.longitude, true);
                if (location == null || Math.abs(location.distanceTo(tempLocation)) > locationDistanceThreshold) {
                    location = tempLocation;
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.putExtra("lat", location.getLatitude());
                    broadcastIntent.putExtra("lng", location.getLongitude());
                    broadcastIntent.setAction(MainActivity.location_changed_broadcast);
                    sendBroadcast(broadcastIntent);
                    Log.d("locationnnn", "broadcast sent ");
                }else{
                    Log.d("locationnnn", "broadcast not sent ");

                }
            }else{
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(MainActivity.not_received_location_broadcast);
                sendBroadcast(broadcastIntent);
            }

        }catch (SecurityException e){

        }

    }

    @Override
    public boolean stopService(Intent name) {
        Log.d("TAG", "stopService: ");
        started.set(false);
        return super.stopService(name);
    }

    public void moveToForeground(){


        if (isForeground == true){
            return;
        }
        Intent intent = new Intent(this,MeetMeLocationService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        }else{
            startService(intent);
        }

        NotificationChannel locationChannel;
        String locationChannelName = "locationChannel";

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            CharSequence name = "Messages";
            String description = "Incoming messages and new Matches";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            locationChannel = new NotificationChannel(locationChannelName, name, importance);
            locationChannel.setDescription(description);
            notificationManager.createNotificationChannel(locationChannel);


        }


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, new Intent(this, MainActivity.class), PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, null)
                .setSmallIcon(R.drawable.icon_notification)
                .setContentText("App is running in the background as long as you are checked in")
                .setStyle(new NotificationCompat.BigTextStyle().bigText("App is running in the background as long as you are checked in"))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setOngoing(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this

            builder.setChannelId(locationChannelName);
        }

        Notification notification = builder.build();


        startForeground(1,
                notification);
        isForeground = true;
    }

    public void moveToBackground(){
        if (isForeground == false){
            return;
        }
        stopForeground(true);
        stopSelf();

        isForeground = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }


    private void run(){

        if (started.get() == false) {
            started = new AtomicBoolean(true);
            final AtomicBoolean thisStarted = started;
            location = null;
            rest = Rest.getInstance();


            Thread.runOnExecutor(new Runnable() {
                @Override
                public void run() {
                    long pollTime = 0;
                    final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);;
                    final WifiManager.WifiLock wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF,"myLock");
                    wifiLock.acquire();
                    while(thisStarted.get() == true) {
                        //Log.d("locationnnn", "ocationservice: run: service is running");
                        if (pollTime + 60*1000 < new Date().getTime()){
                            pollTime = new Date().getTime();
                            pollLocation();
                        }

                        Thread.sleep(10);
                    }
                    wifiLock.release();
                }
            });
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("tag", "In locationServiceBinder");
        started.set(false);
    }




    public class LocationServiceBinder extends Binder{
        public MeetMeLocationService getService(){
            return MeetMeLocationService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Used only in case of bound services.
        Log.d("locationnnn", "location: onbind");
        run();
        return binder;
    }


}