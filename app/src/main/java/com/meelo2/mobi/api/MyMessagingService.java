package com.meelo2.mobi.api;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.Utils.Rest;
import com.meelo2.mobi.activities.MainActivity;
import com.meelo2.mobi.entities.HangoutLocation;
import com.meelo2.mobi.entities.Match;
import com.meelo2.mobi.entities.Message;
import com.meelo2.mobi.entities.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by jzvia on 30/12/2017.
 */

public class MyMessagingService extends FirebaseMessagingService {

    public NotificationChannel normalChannel;
    public NotificationChannel silentChannel;
    public NotificationChannel noVibrationChannel;
    public NotificationChannel noVibrationNoSoundChannel;

    public final static String normalChannelName = "normalChannel";
    public final static String silentChannelName = "silentChannel";
    public final static String noVibrationChannelName = "noVibrationChannel";
    public final static String noVibrationNoSoundChannelName = "noVibrationNoSoundChannel";

    private NotificationManager notificationManager;
    {

    }


    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            CharSequence name = "Messages";
            String description = "Incoming messages and new Matches";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            normalChannel = new NotificationChannel(normalChannelName, name, importance);
            normalChannel.setDescription(description);
            notificationManager.createNotificationChannel(normalChannel);

            silentChannel = new NotificationChannel(silentChannelName, name, importance);
            silentChannel.setDescription(description);
            silentChannel.setSound(null,null);
            notificationManager.createNotificationChannel(silentChannel);

            noVibrationChannel = new NotificationChannel(noVibrationChannelName, name, importance);
            noVibrationChannel.setDescription(description);
            noVibrationChannel.enableVibration(false);
            notificationManager.createNotificationChannel(noVibrationChannel);

            noVibrationNoSoundChannel = new NotificationChannel(noVibrationNoSoundChannelName, name, importance);
            noVibrationNoSoundChannel.setDescription(description);
            noVibrationNoSoundChannel.enableVibration(false);
            noVibrationNoSoundChannel.setSound(null,null);
            notificationManager.createNotificationChannel(noVibrationChannel);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        int notificationId = (int)(new Date().getTime()% 1000000);
        Map data = remoteMessage.getData();
        NotificationCompat.Builder notificationBuilder = null;
        System.out.print(data);
        try {
            JSONObject jsonObject = new JSONObject((String) data.get("message"));
            if (Rest.getInstance() == null){
                Rest.init(getApplicationContext());
            }
            String notificationType = jsonObject.getString("type");
            if (notificationType.equalsIgnoreCase("user_action") == true){
                int actionId = jsonObject.getInt("actionId");
                Intent intent;
                PendingIntent pendingIntent;
                String title;
                Intent broadcastIntent;
                switch (actionId){
                    case 0:
                        title = jsonObject.getString("title");
                        String locationId = jsonObject.getString("locationId");
                        String text = jsonObject.getString("text");
                        intent = new Intent(this, MainActivity.class);
                        intent.putExtra("notificationType", MainActivity.checked_in_by_server_broadcast);
                        intent.putExtra("locationId", locationId);
                        intent.putExtra("text", text);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);


                        notificationBuilder = new NotificationCompat.Builder(this, null)
                                .setSmallIcon(R.drawable.icon_notification)
                                .setContentText(title)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getData().get("text")))
                                .setAutoCancel(true)
                                // .setSound(soundUri)
                                .setContentIntent(pendingIntent);


                        broadcastIntent = new Intent();
                        broadcastIntent.putExtra("id", notificationId);
                        broadcastIntent.putExtra("locationId", locationId);
                        broadcastIntent.putExtra("text", text);
                        broadcastIntent.putExtra("title", title);
                        broadcastIntent.putExtra("remoteMessage", remoteMessage);
                        intent.putExtra("locationId", locationId);
                        intent.putExtra("text", text);
                        broadcastIntent.setAction(MainActivity.checked_in_by_server_broadcast);
                        sendBroadcast(broadcastIntent);

                        break;
                    case 1:
                        Log.d("push_not", "onMessageReceived: " + jsonObject.toString());
                        Rest.getInstance().convertAll(User.class,jsonObject);
                        title = jsonObject.getString("title");
                        intent = new Intent(this, MainActivity.class);
                        intent.putExtra("notificationType", MainActivity.admin_message_broadcast);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 1 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);


                        notificationBuilder = new NotificationCompat.Builder(this, null)
                                .setSmallIcon(R.drawable.icon_notification)
                                .setContentText(title)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getData().get("text")))
                                .setAutoCancel(true)
                                // .setSound(soundUri)
                                .setContentIntent(pendingIntent);


                        broadcastIntent = new Intent();
                        broadcastIntent.putExtra("id", notificationId);
                        broadcastIntent.setAction(MainActivity.admin_message_broadcast);
                        sendBroadcast(broadcastIntent);

                        break;
                    default:
                        break;
                }

            }
            if (notificationType.equalsIgnoreCase("message") == true){
                List<Message> messages = Rest.getInstance().convertAll(Message.class,jsonObject);
                if (messages != null && messages.size() != 0) {

                    Message message = messages.get(0);
                    Match match = Rest.getInstance().getCached(Match.class, message.getMatchId());


                    User userA = Rest.getInstance().getCached(User.class, match.getUserA());
                    User userB = Rest.getInstance().getCached(User.class, match.getUserB());
                    User sender = userA;
                    User currentUser = Rest.getInstance().getCached(User.class, User.getCurrentUserId());
                    if (currentUser.getId().equals(userA.getId())) {
                        sender = userB;
                    }

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("notificationType", MainActivity.message_broadcast);
                    intent.putExtra("matchId", match.getId());
                    intent.putExtra("data", (String) data.get("message"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 2 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);


                    notificationBuilder = new NotificationCompat.Builder(this, null)
                            .setSmallIcon(R.drawable.icon_notification)
                            .setContentText(sender.getFirstName() + " sent you a message")
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getData().get("text")))
                            .setAutoCancel(true)
                            // .setSound(soundUri)
                            .setContentIntent(pendingIntent);


                    Intent broadcastIntent = new Intent();
                    broadcastIntent.putExtra("id", notificationId);
                    broadcastIntent.putExtra("messageId", message.getId());
                    broadcastIntent.putExtra("data", (String) data.get("message"));
                    broadcastIntent.setAction(MainActivity.message_broadcast);
                    sendBroadcast(broadcastIntent);
                }
            }
            if (notificationType.equalsIgnoreCase("match") == true){
                List<Match> matches = Rest.getInstance().convertAll(Match.class,jsonObject);
                if (matches != null && matches.size() != 0){

                    Match match = matches.get(0);



                    User userA = Rest.getInstance().getCached(User.class, match.getUserA());
                    User userB = Rest.getInstance().getCached(User.class, match.getUserB());
                    User sender = userA;
                    User currentUser = Rest.getInstance().getCached(User.class, User.getCurrentUserId());
                    if (currentUser.getId().equals(userA.getId())){
                        sender = userB;
                    }

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("notificationType", MainActivity.message_broadcast );
                    intent.putExtra("matchId", match.getId() );
                    intent.putExtra("data", (String) data.get("message"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 3 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);


                    notificationBuilder = new NotificationCompat.Builder(this, null)
                            .setSmallIcon(R.drawable.icon_notification)
                            .setContentText(  "Congratulations! You have a new Match with " + sender.getFirstName())
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getData().get("text")))
                            .setAutoCancel(true)
                            // .setSound(soundUri)
                            .setContentIntent(pendingIntent);
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.putExtra("id", notificationId);
                    broadcastIntent.putExtra("data", (String) data.get("message"));
                    broadcastIntent.setAction(MainActivity.message_broadcast);
                    sendBroadcast(broadcastIntent);
                }

            }
            if (notificationType.equalsIgnoreCase("checkOut") == true){
                Intent intent1 = new Intent(getApplicationContext(), MeetMeLocationService.class);
                stopService(intent1);
                List<HangoutLocation> hangoutLocations = Rest.getInstance().convertAll(HangoutLocation.class,jsonObject);
                if (hangoutLocations != null && hangoutLocations.size() != 0){

                    HangoutLocation hangoutLocation = hangoutLocations.get(0);
                    Intent intent = new Intent(this, MainActivity.class);
                    //intent.putExtra("notificationType", MainActivity.checked_out_by_server_broadcast );
                    //intent.putExtra("locationId", hangoutLocation.getId() );
                    //intent.putExtra("data", (String) data.get("message"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 4 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);


                    notificationBuilder = new NotificationCompat.Builder(this, null)
                            .setSmallIcon(R.drawable.icon_notification)
                            .setContentText(  "You were checked out of " + hangoutLocation.getName() + (" because " +
                                    "you moved to another location"))
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getData().get("text")))
                            .setAutoCancel(true)
                            // .setSound(soundUri)
                            .setContentIntent(pendingIntent);
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.putExtra("id", notificationId);
                    broadcastIntent.putExtra("text", "You were checked out of " + hangoutLocation.getName() + (" because " +
                            "you moved to another location"));
                    broadcastIntent.setAction(MainActivity.checked_out_by_server_broadcast);
                    sendBroadcast(broadcastIntent);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendNotification(notificationManager,notificationBuilder, notificationId);


    }


    public static void sendNotification(NotificationManager notificationManager, NotificationCompat.Builder notificationBuilder, int notificationId){
        if (Kvs.get("disablePush") == null) {
            // notificationBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                // Create the NotificationChannel
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this

                notificationBuilder.setChannelId(normalChannelName);

                if (Kvs.get("disableSound") == null){
                   /* Channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                            .build());*/
                }else{
                    //Channel.setSound(null, null);
                    notificationBuilder.setChannelId(silentChannelName);
                }
                if (Kvs.get("disableVibration") == null){
                    //notificationBuilder.setVibrate(new long []{1000, 1000, 1000, 1000, 1000 });
                }else{
                    //notificationBuilder.setVibrate(new long []{0});
                    if (Kvs.get("disableSound") == null){
                        notificationBuilder.setChannelId(noVibrationChannelName);
                    }else {
                        notificationBuilder.setChannelId(noVibrationNoSoundChannelName);
                    }
                }

            }else{
                if (Kvs.get("disableSound") == null){
                    notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
                }else{
                    notificationBuilder.setSound(null);
                }
                if (Kvs.get("disableVibration") == null){
                    notificationBuilder.setVibrate(new long []{100, 100, 100, 100, 100 });
                }else{
                    notificationBuilder.setVibrate(new long []{0});
                }
            }

            notificationManager.notify(notificationId, notificationBuilder.build());
        }
    }
}
