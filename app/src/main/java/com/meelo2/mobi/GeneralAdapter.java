package com.meelo2.mobi;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meelo2.mobi.Utils.Rest;;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.Entity;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by jzvia on 29/08/2017.
 */

public abstract class GeneralAdapter<E extends Entity> extends RecyclerView.Adapter{

    public static abstract class ItemClickedCallback<E extends Entity>{
        public abstract void onClick(E entity, int position);
    }

    public static abstract class DataLoadedCallback<E>{
        public abstract void onLoaded(List<E> entities);
    }


    Class<E> clazz;
    Handler handler;
    HandlerThread handlerThread;
    private ItemClickedCallback itemClickedCallback;

    private int itemsPerPage = 10000;

    RecyclerView recyclerView;

    private boolean paged = false;


    private AtomicBoolean isLoading = new AtomicBoolean(false);


    private String params;


    protected Context context = null;

    protected DataLoadedCallback dataLoadedCallback;

    class Spinner extends Entity{
        int pageId;
        Spinner(int page){
            pageId = page;
        }
         public void loadPage(){
            if (pageId == 0)
            {
                return;
            }
            Thread.runOnExecutor(new Runnable() {
                @Override
                public void run() {
                    Thread.sleep(2000);
                    loadData(params,pageId);
                }
            });
         }
    }

    protected List<Entity> entities;// = new ArrayList<>(1);

    public GeneralAdapter(Class<E> clazz, Context context, ItemClickedCallback itemClickedCallback, final DataLoadedCallback dataLoadedCallback, final String params, int pageSize) {
        this(clazz, context, itemClickedCallback, dataLoadedCallback, params);

        itemsPerPage = pageSize;
        paged = true;
    }

    public GeneralAdapter(Class<E> clazz, Context context, ItemClickedCallback itemClickedCallback, final DataLoadedCallback dataLoadedCallback, final String params){
        //hangoutLocations.addAll(Arrays.asList(hangoutLocationsArray));

        this.clazz = clazz;
        this.context = context;
        this.itemClickedCallback = itemClickedCallback;
        this.dataLoadedCallback = dataLoadedCallback;

        handlerThread = new HandlerThread("loader");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
        setHasStableIds(true);

        entities = new LinkedList<>();
        entities.add(new Spinner(0));
        notifyDataSetChanged();


        handler.post(new Runnable() {
            @Override
            public void run() {
                loadData(params,0);


            }
        });


    }

    public abstract void onBindViewHolderImpl(RecyclerView.ViewHolder holder, int position);
    @Override
    public final void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (entities.get(position) instanceof GeneralAdapter.Spinner){
            ((GeneralAdapter.Spinner)entities.get(position)).loadPage();
            return;
        }
        onBindViewHolderImpl(holder, position);

        if (itemClickedCallback != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickedCallback.onClick(entities.get(position), position);
                }
            });
        }





    }

    public void loadData(final String params, final int page){
        if (GeneralAdapter.this instanceof LocationsAdapter == true) {
            Log.d("TAG", "showNearbyLayout: locations adapter" + System.identityHashCode(this));
        }
        this.params = params;
        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                if (isLoading.get() == true){
                    return;
                }
                isLoading.set(true);
                Rest.ServerRetVal<E> retVal = Rest.getInstance().get(clazz, params + (params.contains("?") == true ? "&" : "?") + "limit="+ itemsPerPage + "&page=" + page, true);
                final List<E> newEntities = retVal.getValues();
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {

                        if (page == 0) {
                            if (entities != null) {
                                entities.clear();
                            }else{
                                entities = new LinkedList<>();
                                entities.add(newEntities.get(0));
                                notifyDataSetChanged();
                                entities.clear();

                            }
                            entities.addAll(newEntities);

                        }else{
                            entities.remove(entities.size() - 1);
                            entities.addAll(newEntities);
                        }
                        Log.d("TAG", "spinner: " + newEntities.size() + "  " + itemsPerPage);
                        if (newEntities.size() > 0 && paged == true && newEntities.size() >= itemsPerPage) {
                            entities.add(new Spinner(page + 1));
                        }
                        notifyDataSetChanged();
                        if (page == 0) {
                            if (dataLoadedCallback != null) {
                                if (recyclerView.isAttachedToWindow()){

                                    recyclerView.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            dataLoadedCallback.onLoaded(entities);
                                        }
                                    });
                                }else{
                                    Thread.waitAndRunOnUi(new Runnable() {
                                        @Override
                                        public void run() {
                                            dataLoadedCallback.onLoaded(entities);
                                        }
                                    }, 1);
                                }

                            }
                        }
                    }
                });
                isLoading.set(false);
            }
        });

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public abstract RecyclerView.ViewHolder onCreateViewHolderImpl(final ViewGroup parent, int viewType) ;

    @Override
    public final RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        if (viewType == 0) {
            RecyclerView.ViewHolder holder = onCreateViewHolderImpl(parent, viewType);
            return holder;
        }else {
            return new RecyclerView.ViewHolder(LayoutInflater.from(context).inflate(R.layout.spinner_cell,parent,false)) {

            };
        }


    }

    @Override
    public int getItemViewType(int position) {
        if (entities != null && entities.get(position) instanceof GeneralAdapter.Spinner){
            return 1;
        }
        return 0;
    }

    public void addItem(E entity){
        entities.add(entity);
    }


    @Override
    public int getItemCount() {

        if (entities == null){
            return 0;
        }
        return entities.size();
    }


    public E getEntry(int index){
        return (E)entities.get(index);
    }

}
