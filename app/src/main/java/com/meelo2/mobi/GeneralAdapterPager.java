package com.meelo2.mobi;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.meelo2.mobi.Utils.Rest;;
import com.meelo2.mobi.entities.Entity;

import java.util.List;

/**
 * Created by jzvia on 29/08/2017.
 */

/**
 * Created by jzvia on 29/08/2017.
 */

public abstract class GeneralAdapterPager<E extends Entity> extends PagerAdapter {

    public static abstract class ItemClickedCallback<E extends Entity>{
        public abstract void onClick(E entity, int position);
    }


    private Class<E> clazz;

    Handler handler;
    HandlerThread handlerThread;
    private ItemClickedCallback itemClickedCallback;


    protected Context context = null;

    protected List<E> entities;// = new ArrayList<>(1);


    /*private static HangoutLocation[] hangoutLocationsArray = new HangoutLocation[]{
            new HangoutLocation("Dizzy", "Dizenghoff", 34 , null, "tons of pussy", null, null, R.mipmap.dizzy_frishdon,1),
            new HangoutLocation("El vacino", "Ibn Gabirol", 189 , null, "wine bar", null, null, R.mipmap.elvecino,2),
            new HangoutLocation("Inga", "Galgaley Ha-Plada", 16 , null, "MILFs", null, null, R.mipmap.inga,9),
            new HangoutLocation("Drink Point", "Ben yehuda", 142 , null, "Israeli Arsawats", null, null, R.mipmap.drink_point,0)
    };*/

    public GeneralAdapterPager(Class<E> clazz, Context context, ItemClickedCallback itemClickedCallback, final String params){
        //hangoutLocations.addAll(Arrays.asList(hangoutLocationsArray));


        this.clazz = clazz;
        this.context = context;
        this.itemClickedCallback = itemClickedCallback;

        handlerThread = new HandlerThread("loader");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {

                loadData(params);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });

            }
        });


    }


    public void loadData(String params){
        Rest.ServerRetVal<E> retVal = Rest.getInstance().get( clazz , params, true);
        entities = retVal.getValues();
    }




    @Override
    public int getCount() {

        if (entities == null){
            return 0;
        }
        return entities.size();
    }


    public abstract Object instantiateItemImpl(ViewGroup container, int position);

    @Override
    public final Object instantiateItem(ViewGroup container, int position) {
        return instantiateItemImpl(container, position);
    }



    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}

