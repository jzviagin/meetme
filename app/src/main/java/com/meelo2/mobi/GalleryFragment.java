package com.meelo2.mobi;

/**
 * Created by jzvia on 18/12/2017.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;

import com.meelo2.mobi.GUI.GalleryView;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.api.Var;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.User;

import java.io.File;
import java.util.concurrent.atomic.AtomicReference;


public class GalleryFragment extends BaseNonRootFragment {




    {
        toolbarResId = R.id.app_bar_layout;
    }


    @Override
    protected Drawable fetchBackground(){
        final AtomicReference<Drawable> fetched = new AtomicReference<>(null);
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.loadDrawable(context, User.getCurrent().getPicList()[0], new GUI.DrawableDownloadCallback() {
                    @Override
                    public void onDone(Drawable drawable) {
                        fetched.set(drawable);
                    }
                });
            }
        });
        ;

        while (fetched.get() == null){
            Thread.sleep(1);
        }

        return fetched.get();

    }

    public static void showGallery(Context context){

        final GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.init(R.layout.gallery_fragment,context);
    }


    @Override
    protected void postShowInit() {}

    @Override
    protected void preShowInit() {

        Log.d("TAG", "preShowInit: ");
        ((GalleryView)rootView.findViewById(R.id.gallery_view)).setSelectedItemCallback(new GalleryView.SelectedItemCallback() {
            @Override
            public void itemSelected(Uri uri, boolean b) {
                try {
                    File tempFile = Var.createTempImageFile(getContext());
                    Uri destUri = Uri.fromFile(tempFile);
                    CropFragment.cropImage(context, uri, destUri, false);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });




    }













}



