package com.meelo2.mobi.entities;

/**
 * Created by jzvia on 05/09/2017.
 */

public class Message extends Entity {



    String matchId;
    boolean direction;
    String text;


    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public boolean getDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}


