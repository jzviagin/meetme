package com.meelo2.mobi.entities;

import android.util.Log;

import com.meelo2.mobi.api.Kvs;

import org.joda.time.LocalDate;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by jzvia on 05/09/2017.
 */

public class User extends Entity {

    private static User currentUser;

    public String[] getUnseenMatches() {
        return unseenMatches;
    }

    public void setUnseenMatches(String[] unseenMatches) {
        this.unseenMatches = unseenMatches;
    }

    private String [] unseenMatches;
    public static void setCurrent(User user){
        currentUser = user;
        if (user == null){
            Kvs.delete("userId");
            Kvs.delete("meeloToken");
        }
        if (user != null) {
            Kvs.put("userId", currentUser.getId());
            if (user.getMeeloToken() != null && user.getMeeloToken().trim().equals("") == false) {
                Kvs.put("meeloToken", currentUser.getMeeloToken());
            }
        }
    }

    public static User getCurrent(){
        return currentUser;
    }

    public static String getCurrentUserId(){
        if (currentUser != null) {
            return currentUser.getId();
        }
        return (String)Kvs.get("userId");
    }




    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    String userName;
    String firstName;
    String lastName;
    LocalDate birthDate;
    String bio;
    String gender = "undefined";

    public String getMeeloToken() {
        return meeloToken;
    }

    public void setMeeloToken(String meeloToken) {
        this.meeloToken = meeloToken;
    }

    String meeloToken;
    String [] genderPreference = new String[0];

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    private String facebookId;



    public String[] getPicList() {
        for (String pic:picList){
            Log.d("TAG", "getPicList: " + pic);
        }
        return picList;
    }


    public void removePhoto(int i){
        List<String> list = Arrays.asList(picList);
        LinkedList<String> linkedList = new LinkedList<>(list);
        linkedList.remove(i);
        picList = linkedList.toArray(new String[0]);
    }

    public void addPhoto(String s){
        List<String> list = Arrays.asList(picList);
        LinkedList<String> linkedList = new LinkedList<>(list);
        linkedList.add( linkedList.size(), s);
        picList = linkedList.toArray(new String[0]);
    }

    public void setPicList(String[] picList) {
        this.picList = picList;
    }

    String [] picList = new String[0];

    public String getToken() {
        return token;
    }

    public void setToken(String sessionToken) {
        this.token = sessionToken;
    }

    public String [] getGenderPreference() {
        return genderPreference;
    }

    public void setGenderPreference(String [] genderPreference) {
        this.genderPreference = genderPreference;
    }

    String token;
}


