package com.meelo2.mobi.entities;

/**
 * Created by jzvia on 05/09/2017.
 */

public class Entity {
    //final String hangoutLocationName = HangoutLocation.class.toString();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected String id;




    public static Entity allocate(Class clazz){

        if (clazz.equals(HangoutLocation.class)) {
            return new HangoutLocation();
        }
        if (clazz.equals(User.class)) {
            return new User();
        }

        if (clazz.equals(Match.class)) {
            return new Match();
        }


        if (clazz.equals(Message.class)) {
            return new Message();
        }

        return null;
    }
}
