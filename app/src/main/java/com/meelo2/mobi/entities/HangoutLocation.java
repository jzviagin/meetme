package com.meelo2.mobi.entities;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by jzvia on 29/08/2017.
 */

public class HangoutLocation extends Entity {
    private String name;
    private String address;
    private String description;
    private LatLng location;
    private String bio;
    private String imageUrl;

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    private Float distance;
    private Integer[] ratings; // 0 - general 1 - sex ration 2 - opposite sex popularity 3- match ratio



    public HangoutLocation (){

    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getImageUrl() {
        Log.d("TAG", "getImageUrl: " + imageUrl);
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer[] getRatings() {
        return ratings;
    }

    public void setRatings(Integer[] ratings) {
        this.ratings = ratings;
    }
}
