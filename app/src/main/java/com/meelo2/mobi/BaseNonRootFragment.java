package com.meelo2.mobi;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.meelo2.mobi.GUI.UIcontroller;
import com.meelo2.mobi.api.Thread;



/**
 * Created by j on 05/09/2017.
 */

public abstract class BaseNonRootFragment extends BaseFragment {

    //protected View rootView;
    protected Context context;
    public BaseNonRootFragment() {
        super();
    }



    @Nullable
    @Override
    public final View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View linearLayout = createEmptyLayout(inflater,container);




        if (rootView.getParent() != null){
            ((LinearLayout)rootView.getParent()).removeAllViews();
        }
        rootView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((LinearLayout)linearLayout).addView(rootView);
        return linearLayout;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                UIcontroller.getInstance().goBack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




    protected Drawable fetchBackground(){
        return null;
    }


    protected void init(int resId, final Context context){
        this.context = context;
        createRootView(resId, context, null);
        setHasOptionsMenu(true);
        preShowInit();

        UIcontroller.getInstance().preRenderView(rootView, new UIcontroller.PreRenderCallback() {
            public void onRenderDone(){

                Thread.runOnExecutor(new Runnable() {
                    @Override
                    public void run() {
                        Thread.sleep(1000);
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {

                                postShowInit();
                                Thread.runOnExecutor(new Runnable() {
                                    @Override
                                    public void run() {
                                        Thread.sleep(200);
                                        Drawable drawable = fetchBackground();
                                        setBackgroundDrawable(context, drawable);
                                    }
                                });

                            }
                        });
                    }
                });

                UIcontroller.getInstance().startFragment(BaseNonRootFragment.this);

            }
        });

    }

    protected abstract void preShowInit();

    protected abstract void postShowInit();



}
