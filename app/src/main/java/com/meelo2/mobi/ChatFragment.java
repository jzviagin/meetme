package com.meelo2.mobi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.Utils.Rest;
import com.meelo2.mobi.activities.MainActivity;
//import com.me.meet.meetme.api.MeetMeLocationListener;
;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.Entity;
import com.meelo2.mobi.entities.Match;
import com.meelo2.mobi.entities.Message;
import com.meelo2.mobi.entities.User;

import java.util.List;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


public class ChatFragment extends BaseFragment {


    //private View rootView;

    private BroadcastReceiver receiver;

    // private OnFragmentInteractionListener mListener;

    public ChatFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private RecyclerView recyclerView;




    protected void preShowInit(){

    }

    protected  void postShowInit(){

    }

    @Nullable
    @Override
    public final View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View linearLayout = createEmptyLayout(inflater,container);


        if (rootView == null){
            //setBackgroundDrawable(getContext(), getResources().getDrawable( R.drawable.chat_bg4 ));

            createRootView(R.layout.fragment_chat, getContext(), container);
            //setIsVisible();
            recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            registerReceiver();
            initAdapter();



        }
        if (rootView.getParent() != null){
            ((LinearLayout)rootView.getParent()).removeAllViews();
        }
        ((LinearLayout)linearLayout).addView(rootView);
        return linearLayout;
    }


    private  void initAdapter(){


        recyclerView.setAdapter(new GeneralAdapter<Match>(Match.class, getContext(), new GeneralAdapter.ItemClickedCallback() {
            @Override
            public void onClick(Entity entity, int position) {
                if (entity.getId().equalsIgnoreCase("admin") == false) {
                    MessagesFragment.showMessages(getContext(), entity.getId());
                }else{
                    MessagesFragment.showAdminMessages(getContext());
                }
            }
        }, new GeneralAdapter.DataLoadedCallback() {
            @Override
            public void onLoaded(List entities) {
                if (entities.size() == 0){
                    rootView.findViewById(R.id.no_messages_layout).setVisibility(View.VISIBLE);
                }else{
                    rootView.findViewById(R.id.no_messages_layout).setVisibility(View.GONE);
                }
            }
        }, "") {




            class MatchAdapterHolder extends RecyclerView.ViewHolder{


                public ImageView matchImage;
                public TextView textViewUser;
                public TextView textViewMessage;
                public ImageView newImage;

                public MatchAdapterHolder(View itemView) {
                    super(itemView);
                }

            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolderImpl(final ViewGroup parent, int viewType) {


                return new MatchAdapterHolder(LayoutInflater.from(context).inflate(R.layout.match_recycler_item,parent, false)){
                    {
                        matchImage = (ImageView) itemView.findViewById(R.id.image_view);
                        textViewMessage = itemView.findViewById(R.id.text_view_message);
                        textViewUser = itemView.findViewById(R.id.text_view_from);
                        newImage = itemView.findViewById(R.id.new_message_image);

                    }
                };

            }

            @Override
            public void onBindViewHolderImpl(RecyclerView.ViewHolder holder, int position) {

                Match match = (Match) entities.get(position);
                if (match.getId().equalsIgnoreCase("admin") == false) {
                    User userA = Rest.getInstance().getCached(User.class, match.getUserA());
                    User userB = Rest.getInstance().getCached(User.class, match.getUserB());
                    User user = null;
                    // HangoutLocation hangoutLocation = Rest.getCached(HangoutLocation.class, match.getLocationId());
                    if (userA.getId().equals(User.getCurrent().getId())) {
                        user = userB;
                    } else {
                        user = userA;
                    }
                    ((MatchAdapterHolder) holder).newImage.setVisibility(View.GONE);
                    if (User.getCurrent().getUnseenMatches() != null) {
                        for (int i = 0; i < User.getCurrent().getUnseenMatches().length; i++) {
                            if (User.getCurrent().getUnseenMatches()[i].equals(match.getId())) {
                                ((MatchAdapterHolder) holder).newImage.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    ((MatchAdapterHolder) holder).textViewMessage.setVisibility(View.VISIBLE);

                    GUI.loadRoundedCornersImage(user.getPicList()[0], ((MatchAdapterHolder) holder).matchImage, GUI.Quality.HALF_WIDTH_565, ((MatchAdapterHolder) holder).matchImage.getLayoutParams().width / 2, ((MatchAdapterHolder) holder).matchImage.getLayoutParams().width, ((MatchAdapterHolder) holder).matchImage.getLayoutParams().height);
                    ((MatchAdapterHolder) holder).textViewUser.setVisibility(View.GONE);
                    //((MatchAdapterHolder) holder).textViewMessage.setVisibility(View.GONE);
                    if (match.getLastMessage() != null) {
                        ((MatchAdapterHolder) holder).textViewUser.setVisibility(View.VISIBLE);
                        //((MatchAdapterHolder) holder).textViewMessage.setVisibility(View.VISIBLE);
                        Message message = Rest.getInstance().getCached(Message.class, match.getLastMessage());
                        ((MatchAdapterHolder) holder).textViewMessage.setText(message.getText());
                        User userFrom = null;

                        //User userTo = null;

                        if (message.getDirection() == true) {
                            userFrom = Rest.getInstance().getCached(User.class, match.getUserA());
                            /// userTo = Rest.getCached(User.class, match.getUserB());
                        } else {
                            userFrom = Rest.getInstance().getCached(User.class, match.getUserB());
                            /// userTo = Rest.getCached(User.class, match.getUserA());
                        }
                        if (userFrom.getId().equals(User.getCurrent().getId())) {
                            ((MatchAdapterHolder) holder).textViewUser.setText("Me:");
                        } else {
                            ((MatchAdapterHolder) holder).textViewUser.setText(userFrom.getFirstName() + ":");
                        }

                    } else {
                        User otherUser = Rest.getInstance().getCached(User.class, match.getUserA());
                        if (otherUser.getId().equals(User.getCurrent().getId())) {
                            otherUser = Rest.getInstance().getCached(User.class, match.getUserB());
                        }
                        ((MatchAdapterHolder) holder).textViewMessage.setText("You have a new match with " + otherUser.getFirstName() + "!");
                    }
                }else{

                    ((MatchAdapterHolder) holder).newImage.setVisibility(View.GONE);
                    if (User.getCurrent().getUnseenMatches() != null) {
                        for (int i = 0; i < User.getCurrent().getUnseenMatches().length; i++) {
                            if (User.getCurrent().getUnseenMatches()[i].equals(match.getId())) {
                                ((MatchAdapterHolder) holder).newImage.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    GUI.loadRoundedCornersImageResource(R.drawable.admin_profile, ((MatchAdapterHolder) holder).matchImage, GUI.Quality.HALF_WIDTH_565, ((MatchAdapterHolder) holder).matchImage.getLayoutParams().width / 2, ((MatchAdapterHolder) holder).matchImage.getLayoutParams().width, ((MatchAdapterHolder) holder).matchImage.getLayoutParams().height);
                    ((MatchAdapterHolder) holder).textViewUser.setVisibility(View.VISIBLE);
                    ((MatchAdapterHolder) holder).textViewUser.setText("Admin");
                    ((MatchAdapterHolder) holder).textViewMessage.setVisibility(View.GONE);

                    //((MatchAdapterHolder) holder).textViewMessage.setVisibility(View.GONE);

                }

            }




        });

        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);


    }


    @Override
    public void onResume() {
        super.onResume();
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        registerReceiver();

    }


    private void registerReceiver(){
        if (receiver == null && getContext() != null && rootView != null) {
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(final Context context, final Intent intent) {
                    ((GeneralAdapter<Match>) recyclerView.getAdapter()).loadData("", 0);

                }
            };
            getContext().registerReceiver(receiver,
                    new IntentFilter(MainActivity.message_broadcast));
            getContext().registerReceiver(receiver,
                    new IntentFilter(MainActivity.admin_message_broadcast));
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser ==true){
            registerReceiver();
            if (rootView != null) {
                if (recyclerView.getAdapter() != null) {
                    ((GeneralAdapter<Match>) recyclerView.getAdapter()).loadData("",0);
                }else{
                    initAdapter();
                }
            }
        }else{
            if (getContext()!= null && receiver != null){
                getContext().unregisterReceiver(receiver);
                receiver = null;
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }




}
