package com.meelo2.mobi;

/**
 * Created by jzvia on 18/12/2017.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.meelo2.mobi.GUI.CustomImageView;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.Utils.Rest;
//import com.me.meet.meetme.api.MeetMeLocationListener;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.User;
import com.squareup.picasso.Picasso;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.util.concurrent.atomic.AtomicReference;

;


public class UserDetailsFragment extends BaseNonRootFragment {

    private ViewPager viewPager;
    private String userId;


    {
        toolbarResId = R.id.app_bar_layout;
    }

    public static void showUserDetailsFragment(Context context, String userId){

        final UserDetailsFragment userDetailsFragment = new UserDetailsFragment();
        userDetailsFragment.userId = userId;
        userDetailsFragment.init(R.layout.user_details_fragment,context);
    }






    @Override
    protected void postShowInit() {

    }

    @Override
    protected void preShowInit() {

        User user = Rest.getInstance().getCached(User.class, userId);

        ((TextView)getToolbar().findViewById(R.id.title_name)).setText(user.getFirstName());
        ImageView imageView = getToolbar().findViewById(R.id.image_view);
        //Picasso.with(context).load(user.getPicList()[0]).into(imageView);

        GUI.loadRoundedCornersImage(user.getPicList()[0], imageView, GUI.Quality.HALF_WIDTH_565,imageView.getLayoutParams().width /2,imageView.getLayoutParams().width,imageView.getLayoutParams().height);


        ((TextView)rootView.findViewById(R.id.text_view_bio)).setText(user.getBio());
        LocalDate birthdate = user.getBirthDate();
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(birthdate, now);
        ((TextView)rootView.findViewById(R.id.text_view_name_and_age)).setText(user.getFirstName() + ", " + age.getYears());
        viewPager = (ViewPager) rootView.findViewById(R.id.view_pager);

        viewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (viewPager.isLaidOut() == true && viewPager.getMeasuredHeight() != 0
                        && viewPager.getMeasuredWidth() != 0) {

                    viewPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    initAdapter();
                }

            }
        });



    }






    private  void initAdapter(){
        final User user = Rest.getInstance().getCached(User.class, userId);


       // viewPager.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                {
                    return user.getPicList().length;
                }
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup container, int position) {
                View v = LayoutInflater.from(getContext()).inflate(R.layout.user_details_small_picture,container, false);

                CustomImageView imageView = (CustomImageView) v.findViewById(R.id.image_view);
                v.getLayoutParams().width = viewPager.getMeasuredWidth();
                Log.d("TAG", "instance initializer: " + viewPager.getMeasuredWidth() +
                        "   "  + viewPager.getMeasuredHeight());
                imageView.setMaskResId(R.drawable.swipe_card_background_internal_fade);

                Picasso.with(getContext()).load(user.getPicList()[position]).placeholder(R.drawable.smiley).into(imageView);
                container.addView(v);
                return v;


            }

            @Override
            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
                container.removeView((View)object);
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == object;
            }
        } /*{



            class UserPictureHolder extends RecyclerView.ViewHolder{

                public CustomImageView imageView;
                public UserPictureHolder(View itemView) {
                    super(itemView);
                }

            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {


                return new UserPictureHolder(LayoutInflater.from(getContext()).inflate(R.layout.user_details_small_picture,parent, false)){
                    {
                        imageView = (CustomImageView) itemView.findViewById(R.id.image_view);
                        itemView.getLayoutParams().width = viewPager.getMeasuredWidth();
                        Log.d("TAG", "instance initializer: " + viewPager.getMeasuredWidth() +
                        "   "  + viewPager.getMeasuredHeight());



                    }
                };

            }




            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
                CustomImageView imageView = ((UserPictureHolder)holder).imageView;
                //imageView.setMaskResId(R.drawable.swipe_card_background_internal);
                imageView.setMaskResId(R.drawable.swipe_card_background_internal_fade);

                Picasso.with(getContext()).load(user.getPicList()[position]).into(imageView);




            }

            @Override
            public int getItemCount()
        }*/);

        TabLayout tabLayout = rootView.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager, true);
        if (user.getPicList().length < 2){
            tabLayout.setVisibility(View.GONE);
        }else{
            tabLayout.setVisibility(View.VISIBLE);
        }

        //OverScrollDecoratorHelper.setUpOverScroll(viewPager, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);




    }

    @Override
    protected Drawable fetchBackground(){


        final User user = Rest.getInstance().getCached(User.class, userId);

        final AtomicReference<Drawable> fetched = new AtomicReference<>(null);
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.loadDrawable(context, user.getPicList()[0], new GUI.DrawableDownloadCallback() {
                    @Override
                    public void onDone(Drawable drawable) {
                        fetched.set(drawable);
                    }
                });
            }
        });
  ;

        while (fetched.get() == null){
            Thread.sleep(1);
        }

        return fetched.get();

    }





}

