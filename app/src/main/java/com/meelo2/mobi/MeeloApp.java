package com.meelo2.mobi;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;

import com.meelo2.mobi.api.Thread;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jzvia on 02/12/2017.
 */

public class MeeloApp extends MultiDexApplication
{
    private static Application application;

    public static Context getContext(){
        return application.getApplicationContext();

    }
    @Override
    public void onCreate()
    {
        super.onCreate();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.meelo2.mobi",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e) {
        }
        catch (NoSuchAlgorithmException e) {
        }


        application = this;
        Thread.init();


    }
}