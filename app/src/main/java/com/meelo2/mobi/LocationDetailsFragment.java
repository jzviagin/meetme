package com.meelo2.mobi;

/**
 * Created by jzvia on 18/12/2017.
 */

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.meelo2.mobi.GUI.CustomImageView;
import com.meelo2.mobi.GUI.ZoomRecyclerView;
import com.meelo2.mobi.activities.MainActivity;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.HangoutLocation;
import com.meelo2.mobi.entities.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


public class LocationDetailsFragment extends BaseNonRootFragment {

    {
        defaultBackgroundResId = 0;
    }
    private String locationId;
    HangoutLocation hangoutLocation;

    private boolean ratingsExpanded = false;
    private boolean bioExpanded = false;

    private final static int photosInRecyclerRow = 3;

    private ZoomRecyclerView recyclerView;

    {
        toolbarResId = R.id.app_bar_layout;
    }



    @Override
    protected Drawable fetchBackground(){
        final AtomicReference<Drawable> fetched = new AtomicReference<>(null);
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.loadDrawable(context, hangoutLocation.getImageUrl(), new GUI.DrawableDownloadCallback() {
                    @Override
                    public void onDone(Drawable drawable) {
                        fetched.set(drawable);
                    }
                });
            }
        });
        ;

        while (fetched.get() == null){
            Thread.sleep(1);
        }

        return fetched.get();

    }

    public static void showLocationDetails(Context context, HangoutLocation hangoutLocation){

        final LocationDetailsFragment locationDetailsFragment = new LocationDetailsFragment();
        locationDetailsFragment.locationId = hangoutLocation.getId();
        locationDetailsFragment.hangoutLocation = hangoutLocation;
        locationDetailsFragment.init(R.layout.location_details_fragment,context);
    }

    private void setRating(ViewGroup starsLayout ,float rating){
        for (int i = 0 ; i < 5 ; i++){
            ImageView starImageView = (ImageView) starsLayout.getChildAt(i);

                int resId = (i * 2) - rating >= 0 ? R.drawable.heart_emp :
                        (((i * 2) - rating == -1) ? R.drawable.heart_med : R.drawable.heart_full);
                //resId = R.mipmap.rating_star_on;
                starImageView.setImageResource(resId);


        }
    }



    @Override
    protected void postShowInit() {


        ((TextView)rootView.findViewById(R.id.text_view_bio_long)).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {


            @Override
            public void onGlobalLayout() {

                if (((TextView)rootView.findViewById(R.id.text_view_bio_long)).isLaidOut()  == true) {


                    Layout layout = ((TextView) rootView.findViewById(R.id.text_view_bio_long)).getLayout();
                    int lineCount = layout.getLineCount();
                    boolean bioExpandable = lineCount > 1;

                    Log.d("TAG", "onGlobalLayout: " + lineCount);

                    if (bioExpandable == true) {

                        rootView.findViewById(R.id.image_view_bio_expand).setVisibility(View.VISIBLE);
                        rootView.findViewById(R.id.bio_layout).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                toggleBio();
                            }
                        });
                    } else {
                        rootView.findViewById(R.id.image_view_bio_expand).setVisibility(View.GONE);

                    }

                    rootView.findViewById(R.id.long_bio_layout).setVisibility(View.GONE);
                    ((TextView)rootView.findViewById(R.id.text_view_bio_long)).getViewTreeObserver().removeOnGlobalLayoutListener(this);



                }




            }
        });









        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        initAdapter("by_event_id/?event_id=" + locationId, false);
        getToolbar().findViewById(R.id.map_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng latLng = hangoutLocation.getLocation();
                PackageManager pm = context.getPackageManager();
                Intent navIntent = new Intent(Intent.ACTION_VIEW);
                String baseUri = "google.navigation:q=" + latLng.latitude +"," + latLng.longitude;//"geo: " + latLng.latitude + "," + latLng.longitude;
                navIntent.setData(Uri.parse(baseUri ));
                List<ResolveInfo> resInfo = pm.queryIntentActivities(navIntent, 0);
                List<LabeledIntent> intentList = new ArrayList<>();
                for (int i = 0; i < resInfo.size(); i++) {
                    ResolveInfo ri = resInfo.get(i);
                    String packageName = ri.activityInfo.packageName;
                    Intent specificIntent=new Intent(Intent.ACTION_VIEW);
                    specificIntent.setComponent(new ComponentName(packageName, ri.activityInfo.name));

                    specificIntent.setPackage(packageName);
                    String uri;
                    uri = new String(baseUri);

                    if (packageName.contains("waze")){
                        uri = "https://waze.com/ul?ll=" + latLng.latitude + "," + latLng.longitude + "&navigate=yes";
                    }

                  /*  if (packageName.contains("android.apps.maps")){

                        //https://maps.google.com/?saddr=My%20Location&daddr=myDestinationAddress
                       // uri = "http://maps.google.com/maps?saddr=My Location&daddr=" + latLng.latitude +
                       //  "," + latLng.longitude;
                        uri = "google.navigation:q=" + latLng.latitude +"," + latLng.longitude;
                    }*/
                    specificIntent.setData(Uri.parse(uri));
                    intentList.add(new LabeledIntent(specificIntent, packageName, ri.loadLabel(pm), ri.icon));
                }

                if (intentList.size() != 0){
                    LabeledIntent firstIntent = intentList.get(0);
                    intentList.remove(0);
                    Intent openInChooser = Intent.createChooser(firstIntent, "Navigate...");
                    openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray( new LabeledIntent[ intentList.size()]));
                    startActivity(openInChooser);
                }
            }
        });
    }

    @Override
    protected void preShowInit() {

        ((TextView)getToolbar().findViewById(R.id.title_name)).setText(hangoutLocation.getName());
        setRating((ViewGroup) rootView.findViewById(R.id.stars_layout_general),
                hangoutLocation.getRatings()[0]);
        setRating((ViewGroup) rootView.findViewById(R.id.stars_layout_matches),
                hangoutLocation.getRatings()[3]);
        setRating((ViewGroup) rootView.findViewById(R.id.stars_layout_popularity),
                hangoutLocation.getRatings()[2]);
        setRating((ViewGroup) rootView.findViewById(R.id.stars_layout_ratio),
                hangoutLocation.getRatings()[1]);


        ImageView imageView = getToolbar().findViewById(R.id.image_view_title);

        GUI.loadRoundedCornersImage(hangoutLocation.getImageUrl(), imageView, GUI.Quality.HALF_WIDTH_565,imageView.getLayoutParams().width /2,imageView.getLayoutParams().width,imageView.getLayoutParams().height);

        Log.d("TAG", "preShowInit: ");



        rootView.findViewById(R.id.ratings_layout).setOnClickListener(new View.OnClickListener() {
            //boolean expanded = false;
            @Override
            public void onClick(View view) {

                toggleRatings();
            }
        });

        ((TextView)rootView.findViewById(R.id.text_view_brief_description)).setText(hangoutLocation.getDescription());
        ((TextView)rootView.findViewById(R.id.text_view_address)).setText(hangoutLocation.getAddress());
        ((TextView)rootView.findViewById(R.id.text_view_location_distance)).setText(String.format("%.1f", hangoutLocation.getDistance()) + " KM");
        ((TextView)rootView.findViewById(R.id.text_view_bio_short)).setText(hangoutLocation.getBio());
        ((TextView)rootView.findViewById(R.id.text_view_bio_long)).setText(hangoutLocation.getBio());

        ((TextView)rootView.findViewById(R.id.text_view_bio_long)).forceLayout();

        rootView.findViewById(R.id.text_view_no_people).setVisibility(View.GONE);

        recyclerView = rootView.findViewById(R.id.recycler_view);




        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (recyclerView.isLaidOut() == true) {

                    recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int recyclerWidth = recyclerView.getMeasuredWidth();
                    recyclerView.getLayoutParams().height = recyclerView.getMeasuredWidth() / photosInRecyclerRow;
                    recyclerView.setZoom(photosInRecyclerRow);
                    recyclerView.requestLayout();
                    recyclerView.setInitialHeight(recyclerView.getLayoutParams().height);
                }

            }
        });




    }

    private void initAdapter(String params, final boolean recentVisitors){
        rootView.findViewById(R.id.text_view_no_people).setVisibility(View.GONE);

        recyclerView.setAdapter(new GeneralAdapter<User>(User.class, context, null, new GeneralAdapter.DataLoadedCallback<User>() {
            @Override
            public void onLoaded(List<User> entities) {
                if (entities.isEmpty() == true){
                    rootView.findViewById(R.id.text_view_no_people).setVisibility(View.VISIBLE);
                    if (recentVisitors == false) {
                        ((TextView) rootView.findViewById(R.id.text_view_no_people)).setText("Currenly there are no people at " + hangoutLocation.getName() +
                                ". Click here to see recent visitors.");
                        rootView.findViewById(R.id.text_view_no_people).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                initAdapter("by_event_id_recent_visitors/?event_id=" + locationId, true);
                            }
                        });
                    }else{
                        ((TextView) rootView.findViewById(R.id.text_view_no_people)).setText("No people visited  " + hangoutLocation.getName() +
                                " yet.");
                    }
                }

            }
        },params) {

            class UserPictureHolder extends RecyclerView.ViewHolder{

                public CustomImageView imageView;
                public UserPictureHolder(View itemView) {
                    super(itemView);
                }

            }
            @Override
            public RecyclerView.ViewHolder onCreateViewHolderImpl(final ViewGroup parent, int viewType) {


                return new UserPictureHolder(LayoutInflater.from(getContext()).inflate(R.layout.location_screen_small_picture,parent, false)){
                    {
                        imageView = (CustomImageView) itemView.findViewById(R.id.image_view);
                        imageView.setSquare(true);

                    }
                };

            }




            @Override
            public void onBindViewHolderImpl(final RecyclerView.ViewHolder holder, final int position) {
                final User user = (User)entities.get(position);
                CustomImageView imageView = ((UserPictureHolder)holder).imageView;
                imageView.setMaskResId(R.drawable.small_picture_icon_frame);
                Picasso.with(getContext()).load(user.getPicList()[0]).into(imageView);
                //imageView.setBackgroundColor(Color.RED * position);

            }
        } );

        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);

    }






    private void expandBio(){
        if (bioExpanded == true){
            return;
        }
        collapseRatings();
        GUI.rotateView(rootView.findViewById(R.id.image_view_bio_expand), 0, 180,MainActivity.defaultAnimationDuration, null);
        GUI.fadeOutView(rootView.findViewById(R.id.short_bio_layout), 0, MainActivity.defaultAnimationDuration, null);
        GUI.fadeInView(rootView.findViewById(R.id.long_bio_layout), 0, MainActivity.defaultAnimationDuration, null);
        GUI.expandView(rootView.findViewById(R.id.long_bio_layout),MainActivity.defaultAnimationDuration,  new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {

            }
        });

        bioExpanded = true;
    }

    private void collapseBio(){

        if (bioExpanded == false)
        {
            return;
        }
        GUI.rotateView(rootView.findViewById(R.id.image_view_bio_expand), 180,0, MainActivity.defaultAnimationDuration, null);
        GUI.fadeOutView(rootView.findViewById(R.id.long_bio_layout), 0, MainActivity.defaultAnimationDuration, null);
        GUI.fadeInView(rootView.findViewById(R.id.short_bio_layout), 0, MainActivity.defaultAnimationDuration, null);


        GUI.collapseView(rootView.findViewById(R.id.long_bio_layout),MainActivity.defaultAnimationDuration,  new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {

                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        //rootView.findViewById(R.id.general_ratings_layout).setVisibility(View.VISIBLE);

                    }
                });

            }
        });

        bioExpanded = false;
    }

    private void toggleBio(){
        if (bioExpanded == false){
            //rootView.findViewById(R.id.general_ratings_layout).setVisibility(View.INVISIBLE);
            expandBio();

        }else{
            collapseBio();
        }
    }

    private void expandRatings(){
        if (ratingsExpanded == true){
            return;
        }
        collapseBio();
        GUI.rotateView(rootView.findViewById(R.id.image_view_ratings_expand), 0,180, MainActivity.defaultAnimationDuration, null);
        GUI.fadeOutView(rootView.findViewById(R.id.general_ratings_layout),0, MainActivity.defaultAnimationDuration, null);
        GUI.fadeInView(rootView.findViewById(R.id.detailed_ratings_layout),0, MainActivity.defaultAnimationDuration, null);
        GUI.expandView(rootView.findViewById(R.id.detailed_ratings_layout),MainActivity.defaultAnimationDuration,  new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {

            }
        });
        ratingsExpanded = true;
    }

    private void collapseRatings(){

        if (ratingsExpanded == false)
        {
            return;
        }
        GUI.rotateView(rootView.findViewById(R.id.image_view_ratings_expand), 180, 0, MainActivity.defaultAnimationDuration, null);
        GUI.fadeOutView(rootView.findViewById(R.id.detailed_ratings_layout),0, MainActivity.defaultAnimationDuration, null);
        GUI.fadeInView(rootView.findViewById(R.id.general_ratings_layout), 0, MainActivity.defaultAnimationDuration, null);


        GUI.collapseView(rootView.findViewById(R.id.detailed_ratings_layout),MainActivity.defaultAnimationDuration,  new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {

                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        //rootView.findViewById(R.id.general_ratings_layout).setVisibility(View.VISIBLE);

                    }
                });

            }
        });
        ratingsExpanded = false;
    }




    private void toggleRatings(){
        if (ratingsExpanded == false){
            //rootView.findViewById(R.id.general_ratings_layout).setVisibility(View.INVISIBLE);
            expandRatings();

        }else{
            collapseRatings();
        }
    }








}



