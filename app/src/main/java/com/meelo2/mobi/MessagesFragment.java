package com.meelo2.mobi;

/**
 * Created by jzvia on 18/12/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meelo2.mobi.GUI.CustomImageView;
import com.meelo2.mobi.GUI.UIcontroller;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.activities.MainActivity;
//import com.me.meet.meetme.api.MeetMeLocationListener;
import com.meelo2.mobi.Utils.Rest;;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.Entity;
import com.meelo2.mobi.entities.Match;
import com.meelo2.mobi.entities.Message;
import com.meelo2.mobi.entities.User;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


public class MessagesFragment extends BaseNonRootFragment {

    private RecyclerView recyclerView;


    private BroadcastReceiver receiver;


    private String matchId;

    {
        toolbarResId = R.id.app_bar_layout;
    }

    public static void showMessages(Context context, String matchId){

        final MessagesFragment messagesFragment = new MessagesFragment();
        messagesFragment.matchId = matchId;
        messagesFragment.init(R.layout.messages_fragment,context);
    }

    public static void showAdminMessages(Context context){

        final MessagesFragment messagesFragment = new MessagesFragment();
        messagesFragment.matchId = "admin";
        messagesFragment.init(R.layout.messages_fragment,context);
    }


    @Override
    protected void postShowInit() {

    }

    @Override
    protected void preShowInit() {

        Match match = Rest.getInstance().getCached(Match.class, matchId);


        if (matchId.equalsIgnoreCase("admin") == false) {
            final User user;
             User userA = Rest.getInstance().getCached(User.class, match.getUserA());
             User userB = Rest.getInstance().getCached(User.class, match.getUserB());
             // HangoutLocation hangoutLocation = Rest.getCached(HangoutLocation.class, match.getLocationId());
             if (userA.getId().equals(User.getCurrent().getId()) == true) {
                 user = userB;
             } else {
                 user = userA;
             }

            ((EditText)rootView.findViewById(R.id.edit_text)).setTypeface(ResourcesCompat.getFont(context, R.font.custom2), Typeface.BOLD);

            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserDetailsFragment.showUserDetailsFragment(context, user.getId());
                }
            };

            ((TextView)getToolbar().findViewById(R.id.title_name)).setText(user.getFirstName());
            ((TextView)getToolbar().findViewById(R.id.title_name)).setOnClickListener(onClickListener);
            ImageView imageView = getToolbar().findViewById(R.id.image_view);
            imageView.setOnClickListener(onClickListener);
            getToolbar().findViewById(R.id.block_button).setVisibility(View.VISIBLE);
            getToolbar().findViewById(R.id.block_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GUI.showConfirmDialog(context, "Are you sure you want to block " + user.getFirstName() +
                            "? Any further messages will be ignored", new Runnable() {
                        @Override
                        public void run() {
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    Rest.getInstance().block(user.getId());
                                    Thread.runOnUi(new Runnable() {
                                        @Override
                                        public void run() {
                                            UIcontroller.getInstance().goBack();
                                        }
                                    });
                                }

                            });
                        }
                    });
                }
            });
            //Picasso.with(context).load(user.getPicList()[0]).into(imageView);
            rootView.findViewById(R.id.send_message_layout).setVisibility(View.VISIBLE);

            GUI.loadRoundedCornersImage(user.getPicList()[0], imageView, GUI.Quality.HALF_WIDTH_565,imageView.getLayoutParams().width /2,imageView.getLayoutParams().width,imageView.getLayoutParams().height);
            rootView.findViewById(R.id.button_send).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendMessage();
                }
            });


            final EditText edittext = (EditText) rootView.findViewById(R.id.edit_text);
            edittext.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        sendMessage();
                        return true;
                    }

                    return false;
                }
            });
        }else{
            ((TextView)getToolbar().findViewById(R.id.title_name)).setText("Admin");
            ((TextView)getToolbar().findViewById(R.id.title_name)).setOnClickListener(null);

            rootView.findViewById(R.id.send_message_layout).setVisibility(View.GONE);
            getToolbar().findViewById(R.id.block_button).setVisibility(View.GONE);
            getToolbar().findViewById(R.id.block_button).setOnClickListener(null);
            ImageView imageView = getToolbar().findViewById(R.id.image_view);
            imageView.setOnClickListener(null);
            GUI.loadRoundedCornersImageResource(R.drawable.admin_profile, imageView, GUI.Quality.HALF_WIDTH_565,imageView.getLayoutParams().width /2,imageView.getLayoutParams().width,imageView.getLayoutParams().height);

        }





        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true);
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);



        initAdapter();


    }


    private void sendMessage(){
        EditText editText = rootView.findViewById(R.id.edit_text);
        if (editText.getText() != null && editText.getText().toString().trim().equalsIgnoreCase("") == false){
            String messageText = editText.getText().toString();
            final Message message = new Message();
            message.setText(messageText);
            Match match = Rest.getInstance().getCached(Match.class, matchId);
            message.setDirection(false);
            if (match.getUserA().equals(User.getCurrent().getId()) == true){
                message.setDirection(true);
            }

            message.setMatchId(matchId);

            Thread.runOnExecutor(new Runnable() {
                @Override
                public void run() {
                    try {
                        Rest.getInstance().add(Message.class, message, null);
                    }catch (Rest.PasscodeException e){
                        e.printStackTrace();
                    }
                    Thread.runOnUi(new Runnable() {
                        @Override
                        public void run() {
                            refresh();
                        }
                    });
                }
            });


            editText.setText("");
            //((GeneralAdapter<Message>)recyclerView.getAdapter()).addItem(message);
            //recyclerView.getAdapter().notifyDataSetChanged();
            //scrollToBottom();






        }
    }

    @Override
    public void scrollToBottom(){
        //((LinearLayoutManager)recyclerView.getLayoutManager()).smoothScrollToPosition(recyclerView,recyclerView.,recyclerView.getAdapter().getItemCount() - 1);
        Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                if (recyclerView.getAdapter().getItemCount() != 0) {
                    recyclerView.smoothScrollToPosition(0);
                }
            }
        },1);

    }



    private  void initAdapter(){


        recyclerView.setAdapter(new GeneralAdapter<Message>(Message.class, context, new GeneralAdapter.ItemClickedCallback() {
            @Override
            public void onClick(Entity entity, int position) {

            }
        }, new GeneralAdapter.DataLoadedCallback() {
            @Override
            public void onLoaded(List entities) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(MainActivity.message_read_broadcast);
                context.sendBroadcast(broadcastIntent);
                //scrollToBottom();
            }
        }, "?match_id=" + matchId, 10) {




            class MessageAdapterHolder extends RecyclerView.ViewHolder{


                public ImageView leftImage;


                public ImageView rightImage;

                public  TextView textView;

                public CustomImageView backgroundImage;

                public LinearLayout textWrapper;

                public MessageAdapterHolder(View itemView) {
                    super(itemView);
                }

            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolderImpl(final ViewGroup parent, int viewType) {


                return new MessageAdapterHolder(LayoutInflater.from(context).inflate(R.layout.message_recycler_item,parent, false)){
                    {
                        leftImage = (ImageView) itemView.findViewById(R.id.image_view);
                        rightImage = (ImageView) itemView.findViewById(R.id.image_view2);
                        textView = (TextView) itemView.findViewById(R.id.text_view);
                        textWrapper = (LinearLayout) itemView.findViewById(R.id.text_wrapper);
                        backgroundImage = (CustomImageView)itemView.findViewById(R.id.chat_box_background_image);
                    }
                };

            }



            @Override
            public void onBindViewHolderImpl(RecyclerView.ViewHolder holder, int position) {

                Message message = (Message)entities.get(position);

                boolean first = false;
                boolean last = false;


                if (position == 0){
                    first = true;
                }else{
                    if (message.getDirection() != ((Message)entities.get(position-1)).getDirection()){
                        first = true;
                    }
                }


                int realSize = entities.size();
                if (entities.get(entities.size() - 1) instanceof  GeneralAdapter.Spinner){
                    realSize = entities.size() - 1;
                }
                if (position == realSize - 1){
                    last = true;
                }else{
                    if (message.getDirection() != ((Message)entities.get(position+1)).getDirection()){
                        last = true;
                    }
                }

                if (matchId.equalsIgnoreCase("admin") == false) {
                    //Match match = entities.get(position);
                    Match match = Rest.getInstance().getCached(Match.class, matchId);
                    User userFrom = null;

                    //User userTo = null;

                    if (message.getDirection() == true) {
                        userFrom = Rest.getInstance().getCached(User.class, match.getUserA());
                        /// userTo = Rest.getCached(User.class, match.getUserB());
                    } else {
                        userFrom = Rest.getInstance().getCached(User.class, match.getUserB());
                        /// userTo = Rest.getCached(User.class, match.getUserA());
                    }


                    ((MessageAdapterHolder) holder).leftImage.setVisibility(View.INVISIBLE);
                    ((MessageAdapterHolder) holder).rightImage.setVisibility(View.INVISIBLE);

                    ImageView imageView = null;
                    ImageView otherImageView = null;
                    if (userFrom.getId().equals(User.getCurrent().getId()) == true) {
                        // ((MessageAdapterHolder)holder).backgroundImage.setImageDrawable(context.getResources().getDrawable(R.drawable.message_background_ltr));
                        imageView = ((MessageAdapterHolder) holder).leftImage;
                        otherImageView = ((MessageAdapterHolder) holder).rightImage;
                        //((LinearLayout.LayoutParams)((MessageAdapterHolder)holder).textView.getLayoutParams()).gravity = Gravity.LEFT;
                        ((MessageAdapterHolder) holder).textView.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                        ((MessageAdapterHolder) holder).textWrapper.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                        if (first == true) {

                            Drawable drawable = context.getResources().getDrawable(R.drawable.bubble_left_last);
                            drawable.setColorFilter(new
                                    PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP));
                            //((MessageAdapterHolder)holder).textView.setBackground(drawable);
                            //((MessageAdapterHolder)holder).textView.setBackground(drawable);
                            ((MessageAdapterHolder) holder).backgroundImage.setMaskResId(R.drawable.bubble_left_last);

                        } else {
                            Drawable drawable = context.getResources().getDrawable(R.drawable.bubble_left);
                            drawable.setColorFilter(new
                                    PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP));
                            //((MessageAdapterHolder)holder).textView.setBackground(drawable);
                            ((MessageAdapterHolder) holder).backgroundImage.setMaskResId(R.drawable.bubble_left);

                        }

                    } else {
                        //((MessageAdapterHolder)holder).backgroundImage.setImageDrawable(context.getResources().getDrawable(R.drawable.message_background_rtl));

                        imageView = ((MessageAdapterHolder) holder).rightImage;
                        otherImageView = ((MessageAdapterHolder) holder).leftImage;
                        //((LinearLayout.LayoutParams)((MessageAdapterHolder)holder).textView.getLayoutParams()).gravity = Gravity.RIGHT;
                        ((MessageAdapterHolder) holder).textView.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        ((MessageAdapterHolder) holder).textWrapper.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        if (first == true) {

                            Drawable drawable = context.getResources().getDrawable(R.drawable.bubble_right_last);
                            drawable.setColorFilter(new
                                    PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP));
                            // ((MessageAdapterHolder)holder).textView.setBackground(drawable);
                            ((MessageAdapterHolder) holder).backgroundImage.setMaskResId(R.drawable.bubble_right_last);
                        } else {
                            Drawable drawable = context.getResources().getDrawable(R.drawable.bubble_right);
                            drawable.setColorFilter(new
                                    PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP));
                            //((MessageAdapterHolder)holder).textView.setBackground(drawable);
                            ((MessageAdapterHolder) holder).backgroundImage.setMaskResId(R.drawable.bubble_right);

                        }


                    }

                    if (first == true) {
                        imageView.setVisibility(View.VISIBLE);
                    }
                    otherImageView.setVisibility(View.GONE);

                    GUI.loadRoundedCornersImage(userFrom.getPicList()[0], imageView, GUI.Quality.HALF_WIDTH_565, imageView.getLayoutParams().width / 2, imageView.getLayoutParams().width, imageView.getLayoutParams().height);
                    ((MessageAdapterHolder) holder).textView.setText(message.getText());
                }else {
                    ((MessageAdapterHolder) holder).rightImage.setVisibility(View.VISIBLE);
                    ((MessageAdapterHolder) holder).leftImage.setVisibility(View.GONE);
                    //((LinearLayout.LayoutParams)((MessageAdapterHolder)holder).textView.getLayoutParams()).gravity = Gravity.RIGHT;
                    ((MessageAdapterHolder) holder).textView.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    ((MessageAdapterHolder) holder).textWrapper.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    if (first == true) {

                        Drawable drawable = context.getResources().getDrawable(R.drawable.bubble_right_last);
                        drawable.setColorFilter(new
                                PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP));
                        // ((MessageAdapterHolder)holder).textView.setBackground(drawable);
                        ((MessageAdapterHolder) holder).backgroundImage.setMaskResId(R.drawable.bubble_right_last);
                    } else {
                        Drawable drawable = context.getResources().getDrawable(R.drawable.bubble_right);
                        drawable.setColorFilter(new
                                PorterDuffColorFilter(ContextCompat.getColor(context, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP));
                        //((MessageAdapterHolder)holder).textView.setBackground(drawable);
                        ((MessageAdapterHolder) holder).backgroundImage.setMaskResId(R.drawable.bubble_right);

                    }
                    GUI.loadRoundedCornersImageResource(R.drawable.admin_profile, ((MessageAdapterHolder) holder).rightImage, GUI.Quality.HALF_WIDTH_565, ((MessageAdapterHolder) holder).rightImage.getLayoutParams().width / 2, ((MessageAdapterHolder) holder).rightImage.getLayoutParams().width, ((MessageAdapterHolder) holder).rightImage.getLayoutParams().height);
                    ((MessageAdapterHolder) holder).textView.setText(message.getText());
                }

            }




        });
        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);


    }

    @Override
    protected Drawable fetchBackground(){
        if (matchId.equalsIgnoreCase("admin" ) == false) {
            Match match = Rest.getInstance().getCached(Match.class, matchId);

            User userA = Rest.getInstance().getCached(User.class, match.getUserA());
            User userB = Rest.getInstance().getCached(User.class, match.getUserB());
            final User user;
            // HangoutLocation hangoutLocation = Rest.getCached(HangoutLocation.class, match.getLocationId());
            if (userA.getId().equals(User.getCurrent().getId()) == true) {
                user = userB;
            } else {
                user = userA;
            }

            final AtomicReference<Drawable> fetched = new AtomicReference<>(null);
            Thread.runOnUi(new Runnable() {
                @Override
                public void run() {
                    GUI.loadDrawable(context, user.getPicList()[0], new GUI.DrawableDownloadCallback() {
                        @Override
                        public void onDone(Drawable drawable) {
                            fetched.set(drawable);
                        }
                    });
                }
            });
            ;

            while (fetched.get() == null) {
                Thread.sleep(1);
            }

            return fetched.get();
        }else{
            return getResources().getDrawable(R.drawable.admin_profile);
        }

    }

    public void refresh(){
        if (recyclerView.getAdapter() != null){
            ((GeneralAdapter<Message>)recyclerView.getAdapter()).loadData("?match_id=" + matchId,0);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser == true) {

            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(final Context context, final Intent intent) {
                    Thread.waitAndRunOnUi(new Runnable() {
                        @Override
                        public void run() {
                            Intent broadcastIntent = new Intent();
                            broadcastIntent.setAction(MainActivity.message_read_broadcast);
                            context.sendBroadcast(broadcastIntent);
                        }
                    },1500);
                    Log.d("TAG", "onReceive: " + intent.getStringExtra("id"));
                    Message message = Rest.getInstance().getCached(Message.class, intent.getStringExtra("messageId"));
                    if ((matchId.equalsIgnoreCase("admin") && intent.getAction().equalsIgnoreCase(MainActivity.admin_message_broadcast)) || matchId.equals(message.getMatchId())) {
                        refresh();
                    }

                }
            };
            context.registerReceiver(receiver,
                    new IntentFilter(MainActivity.message_broadcast));
            context.registerReceiver(receiver,
                    new IntentFilter(MainActivity.admin_message_broadcast));
        }else{
            if (context!= null && receiver != null){
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(MainActivity.message_read_broadcast);
                context.sendBroadcast(broadcastIntent);
                refresh();
                context.unregisterReceiver(receiver);
                receiver = null;
            }
        }





      /*  if (isVisibleToUser == true && rootView != null){
            scrollToBottom();
        }*/
    }

}

