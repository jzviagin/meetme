package com.meelo2.mobi.GUI;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.meelo2.mobi.app.R;
import com.steelkiwi.cropiwa.AspectRatio;
import com.steelkiwi.cropiwa.CropIwaView;
import com.steelkiwi.cropiwa.config.CropIwaSaveConfig;
import com.steelkiwi.cropiwa.image.CropIwaResultReceiver;

/**
 * Created by jzvia on 24/01/2018.
 */

public class CropView extends LinearLayout {
    private CropIwaView cropView;
    private View view;
    private Uri destUri;

    private Uri uri;
    private CropIwaResultReceiver.Listener listener;
    private CropIwaResultReceiver resultReceiver;




    public CropView(Context context) {
        super(context);
        init();

    }

    public CropView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CropView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public CropView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){



    }


    @Override
    protected void onDetachedFromWindow() {
        Log.d("TAG", "onDetachedFromWindow: ");
        if (resultReceiver != null){
            resultReceiver.unregister(getContext());
            resultReceiver = null;
        }
        super.onDetachedFromWindow();
        removeView(view);

    }

    @Override
    protected void onAttachedToWindow() {
        Log.d("TAG", "onAttachedToWindow: ");
        super.onAttachedToWindow();
        view  = LayoutInflater.from(getContext()).inflate(R.layout.crop_view, this, false);
        addView(view);
        cropView = view.findViewById(R.id.cropiwa_view);
        if (uri != null) {
          setDataInternal();
        }
    }


    private void setDataInternal(){

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }*/

        cropView.setImageUri(uri);
        cropView.configureOverlay()
                .setDynamicCrop(false)
                .setAspectRatio(new AspectRatio(3,4))
                .apply();
        //cropView.configureImage().setScale(1f).apply();



        resultReceiver = new CropIwaResultReceiver();
        resultReceiver.setListener(new CropIwaResultReceiver.Listener() {
            @Override
            public void onCropSuccess(Uri croppedUri) {
                if (resultReceiver!= null) {
                    resultReceiver.unregister(getContext());
                    resultReceiver = null;
                }
                Log.d("TAG", "onCropSuccess: ");
                listener.onCropSuccess(croppedUri);
            }

            @Override
            public void onCropFailed(Throwable e) {
                if (resultReceiver!= null) {
                    resultReceiver.unregister(getContext());
                    resultReceiver = null;
                }
                listener.onCropFailed(e);
            }
        });

        resultReceiver.register(getContext());
    }

    public void setData(Uri uri, Uri destUri, final CropIwaResultReceiver.Listener listener){


        this.uri = uri;
        this.destUri = destUri;
        this.listener = listener;
        if (isAttachedToWindow() == true){
            setDataInternal();

        }

        /*Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                while (isAttachedToWindow() == false){
                    Thread.sleep(1);

                }
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        setDataInternal();
                    }
                });
            }
        });*/

    }


    public void save(){
        cropView.crop(new CropIwaSaveConfig.Builder(destUri)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setQuality(70) //Hint for lossy compression formats
                //.setSize(768,1024)
                .build());
    }


}
