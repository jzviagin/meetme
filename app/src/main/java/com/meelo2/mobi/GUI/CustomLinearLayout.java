package com.meelo2.mobi.GUI;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.meelo2.mobi.Utils.GUI;


public class CustomLinearLayout extends LinearLayout {




    public void setMaskResId(int maskResId) {
        this.maskResId = maskResId;
    }


    private int maskResId;


    public CustomLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

    }







    @Override
    public void dispatchDraw(Canvas canvas) {

        if ( maskResId != 0 && getMeasuredHeight() > 0 && getMeasuredWidth() > 0) {
        /*    Bitmap offscreenBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas offscreenCanvas = new Canvas(offscreenBitmap);

            super.onDraw(offscreenCanvas);


            Bitmap maskBitmap = GUI.convertToAlphaMask(GUI.drawableToBitmap(getContext(), maskResId, getMeasuredWidth(), getMeasuredHeight()));

            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

            Paint maskPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
            maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));


            offscreenCanvas.drawBitmap(maskBitmap, 0f, 0f, maskPaint);
            canvas.drawBitmap(offscreenBitmap, 0f, 0f, paint);*/
            int save = canvas.save();
            Bitmap offscreenBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas offscreenCanvas = new Canvas(offscreenBitmap);

            super.dispatchDraw(offscreenCanvas);
            BitmapShader shader;
            shader = new BitmapShader(offscreenBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

           // shader.setLocalMatrix(getImageMatrix());
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(shader);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawBitmap(GUI.convertToAlphaMask(GUI.drawableToBitmap(getContext(), maskResId, getMeasuredWidth() , getMeasuredHeight() )), 0, 0, paint);
        }else{
            super.dispatchDraw(canvas);
        }
    }


}