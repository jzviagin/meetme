package com.meelo2.mobi.GUI;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.meelo2.mobi.api.Thread;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jzvia on 12/01/2018.
 */

public class TwoFingersRecyclerView extends RecyclerView {


    private final static int STATE_IDLE = 0;
    private final static int STATE_WAITING_SECOND_FINGER = 1;
    private final static int STATE_SECOND_FINGER_PLACED = 2;
    private final static int STATE_EVENTS_RETURNED_TO_ACTIVITY = 3;
    private final static int secondFingerThreshold = 150;
    private final static int discardAfterFlushThreshold = 300;



    private int state = STATE_IDLE;

    private List<MotionEvent> events = new ArrayList<MotionEvent>(10);





    public TwoFingersRecyclerView(Context context) {
        super(context);
    }

    public TwoFingersRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TwoFingersRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }



    public void init(){

    }





    private void flushEvents(){
        if (state == STATE_WAITING_SECOND_FINGER) {
            final MotionEvent eCopy = MotionEvent.obtain(
                    events.get(0).getDownTime() ,
                    events.get(0).getEventTime(),
                    MotionEvent.ACTION_UP,
                    events.get(0).getX(),
                    events.get(0).getY(),
                    events.get(0).getMetaState());

            boolean click = true;
            for (  MotionEvent event:events){
                if (event.getAction() == MotionEvent.ACTION_MOVE){
                    click = false;
                }
            }
            if (click == true){
                super.dispatchTouchEvent(eCopy);
            }

        }



        for (  MotionEvent event:events) {




            //Log.d("TAG", "dispatchTouchEvent: " + ev.getDownTime() + "  " + downTime);
           /* MotionEvent motionEvent = MotionEvent.obtain(
                    event.getDownTime(),
                    event.getEventTime(),
                    event.getAction(),
                    event.getX(),
                    event.getY(),
                    event.getMetaState()
            );*/


            if (state == STATE_SECOND_FINGER_PLACED){
                super.onTouchEvent(event);
            }else {
                state = STATE_EVENTS_RETURNED_TO_ACTIVITY;
                Thread.runOnExecutor(new Runnable() {
                    @Override
                    public void run() {
                        Thread.sleep(discardAfterFlushThreshold);
                        state = STATE_IDLE;
                    }
                });
                ((Activity) getContext()).dispatchTouchEvent(event);
                Log.d("TAG", "sending event back:  " + event);
            }
        }
        events.clear();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent e) {

        MotionEvent.PointerProperties [] pointerPropertiesArray = new MotionEvent.PointerProperties[e.getPointerCount()];
        MotionEvent.PointerCoords [] pointerCoordsArray = new MotionEvent.PointerCoords[e.getPointerCount()];
        for (int i = 0 ; i < e.getPointerCount(); i++)
        {
            MotionEvent.PointerProperties pointerProperties = new MotionEvent.PointerProperties();
            MotionEvent.PointerCoords pointerCoords = new MotionEvent.PointerCoords();
            e.getPointerProperties(i, pointerProperties);
            pointerPropertiesArray[i] = pointerProperties;
            pointerCoordsArray[i] = pointerCoords;
        }

     /*   MotionEvent eCopy = MotionEvent.obtain(
                e.getDownTime() + secondFingerThreshold,
                e.getEventTime() + secondFingerThreshold,
                e.getAction(),
                e.getPointerCount(),
                pointerPropertiesArray,
                pointerCoordsArray,
                e.getMetaState(),
                e.getButtonState(),
                e.getXPrecision(),
                e.getYPrecision(),
                e.getDeviceId(),
                e.getEdgeFlags(),
                e.getSource(),
                e.getFlags());*/


        final MotionEvent eCopy = MotionEvent.obtain(
                e.getDownTime() + secondFingerThreshold,
                e.getEventTime() + secondFingerThreshold,
                e.getActionMasked(),
                e.getRawX(),
                e.getRawY(),
                e.getMetaState());

        boolean retVal;

        switch (state){
            case STATE_IDLE:
                events.add(eCopy);
                Log.d("TAG", "dispatchTouchEvent: " + e);
                super.dispatchTouchEvent(e);
                if (e.getAction() == MotionEvent.ACTION_DOWN){

                    state = STATE_WAITING_SECOND_FINGER;
                    Thread.waitAndRunOnUi(new Runnable() {
                        @Override
                        public void run() {
                            flushEvents();
                        }
                    },secondFingerThreshold);

                }
                return true;

            case STATE_WAITING_SECOND_FINGER:

                //retVal = super.dispatchTouchEvent(e);

                events.add(eCopy);
                if (e.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN ){
                    state = STATE_SECOND_FINGER_PLACED;
                }
                return true;
            case STATE_SECOND_FINGER_PLACED:
                super.onTouchEvent(e);
                if (e.getActionMasked() == MotionEvent.ACTION_UP){
                    state = STATE_IDLE;
                }
                return true;

            case STATE_EVENTS_RETURNED_TO_ACTIVITY:
                if (e.getActionMasked() == MotionEvent.ACTION_UP){
                    super.dispatchTouchEvent(e);
                }
                return false;

            default:
                return true;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent e){
        switch (state) {
            case STATE_SECOND_FINGER_PLACED:
                return super.onTouchEvent(e);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {


        switch (state){
            case STATE_SECOND_FINGER_PLACED:
                return super.onInterceptTouchEvent(e);
        }
        return false;

    }


}
