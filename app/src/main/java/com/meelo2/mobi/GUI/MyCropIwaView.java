package com.meelo2.mobi.GUI;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import com.steelkiwi.cropiwa.CropIwaView;

/**
 * Created by jzvia on 25/01/2018.
 */

public class MyCropIwaView extends CropIwaView {
    public MyCropIwaView(Context context) {
        super(context);
    }

    public MyCropIwaView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCropIwaView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyCropIwaView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDetachedFromWindow() {
        Log.d("TAG", "onDetachedFromWindow: " + this);
        super.onDetachedFromWindow();

    }
}
