package com.meelo2.mobi.GUI;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.meelo2.mobi.BaseFragment;
import com.meelo2.mobi.ChatFragment;
import com.meelo2.mobi.FullScreenFragment;
import com.meelo2.mobi.HomeFragment;
import com.meelo2.mobi.MeetFragment;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.SettingsFragment;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.api.Thread;
import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by jzvia on 16/01/2018.
 */

public class MyFragNavController {


    private boolean runThread = true;

    private FragNavController fragNavController;

    private AppCompatActivity activity;


    private static MyFragNavController instance;

    private int container;

    static {



    }

    //private Queue<Runnable> executionQueue = new ConcurrentLinkedQueue<>();

    private boolean changeToolbarRequest = false;


    private MyFragNavController(){
        super();
    }


    private void destroy(){
        runThread = false;
    }

    public static void init(int container, AppCompatActivity activity){
        if(instance != null){
            instance.destroy();
        }
        instance = new MyFragNavController();

        instance.container = container;
        instance.activity = activity;


        Fragment [] fragments = new Fragment[] { new HomeFragment(), new MeetFragment(), new ChatFragment(), new SettingsFragment()};


        FragNavTransactionOptions fragNavTransactionOptions = FragNavTransactionOptions.newBuilder().customAnimations(R.anim.fade_in,R.anim.fade_out,R.anim.fade_in,R.anim.fade_out).build();

        FragNavController.Builder fragNavControllerBuilder = FragNavController.newBuilder(null, instance.activity.getSupportFragmentManager(), instance.container).defaultTransactionOptions(fragNavTransactionOptions);

        List<Fragment> fragmentsListTmp = Arrays.asList(fragments);
        final List<Fragment> fragmentsList = new ArrayList<>();
        fragmentsList.addAll(fragmentsListTmp);

        fragNavControllerBuilder.rootFragments(fragmentsList);

        instance.fragNavController = fragNavControllerBuilder.build();

        //instance.fragNavController.switchTab(FragNavController.TAB4);

        UIcontroller.fragNavProxy = instance.new FragNavProxy();

        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                final MyFragNavController finalInstance = instance;
                while (finalInstance.runThread == true){
                    if (finalInstance.changeToolbarRequest == true){
                        finalInstance.changeToolbarRequest = false;
                        finalInstance.updateToolbar();

                    }
                    Thread.sleep(10);
                }
            }
        });



    }



    public class FragNavProxy{
        private FragNavProxy(){

        }


        public void initialize(int index) {
            instance.initialize(index);
        }

        public void switchTab(int index, @Nullable FragNavTransactionOptions transactionOptions) throws IndexOutOfBoundsException {
            instance.switchTab(index, transactionOptions);
        }

        public void switchTab(int index) throws IndexOutOfBoundsException {
            instance.switchTab(index);
        }

        public void pushFragment(@Nullable Fragment fragment, @Nullable FragNavTransactionOptions transactionOptions) {
            instance.pushFragment(fragment, transactionOptions);
        }

        public void pushFragment(@Nullable Fragment fragment) {
            instance.pushFragment(fragment);
        }

        public void popFragment(@Nullable FragNavTransactionOptions transactionOptions) throws UnsupportedOperationException {
            instance.popFragment(transactionOptions);
        }

        public void popFragment() throws UnsupportedOperationException {
            instance.popFragment();
        }

        public void popFragments(int popDepth, @Nullable FragNavTransactionOptions transactionOptions) throws UnsupportedOperationException {
            instance.popFragments(popDepth, transactionOptions);
        }

        public void popFragments(int popDepth) throws UnsupportedOperationException {
            instance.popFragments(popDepth);
        }

        public void clearStack(@Nullable FragNavTransactionOptions transactionOptions) {
            instance.clearStack(transactionOptions);
        }

        public void clearStack() {
            instance.clearStack();
        }

        public void replaceFragment(@NonNull Fragment fragment, @Nullable FragNavTransactionOptions transactionOptions) {
            instance.replaceFragment(fragment, transactionOptions);
        }

        public void replaceFragment(@NonNull Fragment fragment) {
            instance.replaceFragment(fragment);
        }


        public int getCurrentStackSize(){
            return instance.getCurrentStackSize();
        }
        public int getCurrentTab(){
            return instance.getCurrentTab();
        }

        public boolean isCurrentFragmentFullScreen(){
            return instance.isCurrentFragmentFullScreen();
        }

        public boolean isCurrentFragmentWithToolbar(){
            return instance.isCurrentFragmentWithToolbar();
        }

        public void refreshCurrentFragment(){
            Log.d("TAG", "refreshCurrentFragment: ");
            ((BaseFragment)instance.fragNavController.getCurrentFrag()).refreshContent();
        }

        public void scrollToBottom(){
            ((BaseFragment)instance.fragNavController.getCurrentFrag()).scrollToBottom();
        }

    }





    private void initialize(int index) {
        instance.fragNavController.initialize(index);
    }

    private void switchTab(final int index,final  @Nullable FragNavTransactionOptions transactionOptions) throws IndexOutOfBoundsException {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.switchTab(index, transactionOptions);

            }
        });
    }

    private void switchTab(final int index) throws IndexOutOfBoundsException {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.switchTab(index);

            }
        });
    }

    private void pushFragment(final @Nullable Fragment fragment, final @Nullable FragNavTransactionOptions transactionOptions) {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.pushFragment(fragment, transactionOptions);

            }
        });
    }

    private void pushFragment(final @Nullable Fragment fragment) {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.pushFragment(fragment);

            }
        });
    }

    private void popFragment(final  @Nullable FragNavTransactionOptions transactionOptions) throws UnsupportedOperationException {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.popFragment(transactionOptions);

            }
        });

    }

    private void popFragment() throws UnsupportedOperationException {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.popFragment();
            }
        });
    }

    private void popFragments(final int popDepth, final @Nullable FragNavTransactionOptions transactionOptions) throws UnsupportedOperationException {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.popFragments(popDepth, transactionOptions);
            }
        });
    }

    private void popFragments(final int popDepth) throws UnsupportedOperationException {

        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.popFragments(popDepth);
            }
        });
    }

    private void clearStack(final @Nullable FragNavTransactionOptions transactionOptions) {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.clearStack(transactionOptions);

            }
        });
    }

    private void clearStack() {
        instance.fragNavController.clearStack();
    }

    private void replaceFragment(final @NonNull Fragment fragment, final @Nullable FragNavTransactionOptions transactionOptions) {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.replaceFragment(fragment, transactionOptions);

            }
        });
    }

    private void replaceFragment(final @NonNull Fragment fragment) {
        fragmentChanged(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.replaceFragment(fragment);

            }
        });
    }


    private int getCurrentStackSize(){
        return instance.fragNavController.getCurrentStack().size();
    }

    private int getCurrentTab(){
        return instance.fragNavController.getCurrentStackIndex();
    }

    private boolean isCurrentFragmentFullScreen(){
        return instance.fragNavController.getCurrentFrag() instanceof FullScreenFragment;
    }


    private boolean isCurrentFragmentWithToolbar(){
        return ((BaseFragment)instance.fragNavController.getCurrentFrag()).hasToolbar();
    }

    private void updateToolbar(){

        final AtomicBoolean isDone = new AtomicBoolean(false);
        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                //Thread.sleep(2000);
                Log.d("toolbar", "toolbar: requested");
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("toolbar", "toolbar: about to clear");
                        UIcontroller.getInstance().clearToolbar(new GUI.CollapseExpandListener() {
                            @Override
                            public void onComplete(View view) {
                                Log.d("toolbar", "toolbar: cleared");
                                Thread.runOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("toolbar", "toolbar: about to add");
                                        UIcontroller.getInstance().addToolbar(((BaseFragment) instance.fragNavController.getCurrentFrag()).getToolbar(),
                                                getCurrentStackSize() > 1, new GUI.CollapseExpandListener() {
                                                    @Override
                                                    public void onComplete(View view) {
                                                        Log.d("toolbar", "toolbar: added");
                                                        isDone.set(true);
                                                    }
                                                });
                                    }
                                });
                            }
                        });
                    }
                });


            }
        });


        while (isDone.get() == false){
            Thread.sleep(10);
        }
        Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                instance.fragNavController.getCurrentStack().peek().setUserVisibleHint(true);

            }
        },1);


    }


    private void fragmentChanged(Runnable action){
        for (int i = 0; i < instance.fragNavController.getSize() ; i++){
            Stack<Fragment> stack = instance.fragNavController.getStack(i);
            for (Fragment fragment: stack){
                fragment.setUserVisibleHint(false);
            }
        }

        action.run();

        changeToolbarRequest = true;

        UIcontroller.getInstance().hideSoftKeyboard();

    }
}
