package com.meelo2.mobi.GUI;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by jzvia on 21/02/2018.
 */

public class FadeImageView extends ImageView {

    private float FadeStrength = 100.0f;
    private int FadeLength = 200;


    private void init(){
        setFadingEdgeLength(FadeLength);
        setVerticalFadingEdgeEnabled(true);
        setHorizontalFadingEdgeEnabled(true);
    }
    public FadeImageView(Context context) {
        super(context);
        init();
    }

    public FadeImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FadeImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public FadeImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @Override
    protected float getTopFadingEdgeStrength() {
        return FadeStrength;
    }

    @Override
    protected float getBottomFadingEdgeStrength() {
        return FadeStrength;
    }

    @Override
    protected float getLeftFadingEdgeStrength() {
        return FadeStrength;
    }

    @Override
    protected float getRightFadingEdgeStrength() {
        return FadeStrength;
    }
}
