package com.meelo2.mobi.GUI;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by jzvia on 13/01/2018.
 */

public class FitHeightImageView extends ImageView{
    public FitHeightImageView(Context context) {
        super(context);
    }

    public FitHeightImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FitHeightImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

  /*  public FitHeightImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }*/

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (getDrawable() != null){
            BitmapDrawable drawable = (BitmapDrawable) getDrawable();
            float drawableRatio = (float) drawable.getBitmap().getHeight() / (float) drawable.getBitmap().getWidth();
            float myRatio = (float) getMeasuredHeight() / getMeasuredWidth();
            if (drawableRatio > myRatio){
                setScaleType(ScaleType.FIT_CENTER);
            }else{
                setScaleType(ScaleType.CENTER_CROP);
            }
        }
    }
}
