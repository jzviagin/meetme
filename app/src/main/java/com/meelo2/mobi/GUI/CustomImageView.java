package com.meelo2.mobi.GUI;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import com.meelo2.mobi.Utils.GUI;


public class CustomImageView extends ImageView {

    private boolean square = false;
    private boolean matchWidth = false;



    public void setMaskResId(int maskResId) {
        this.maskResId = maskResId;
    }


    private int maskResId;


    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public void setSquare(boolean b){
        square = b;
    }
    public void setMatchWidth(boolean b){
        matchWidth = b;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        if (square == true) {
            if (matchWidth == false){
                this.setMeasuredDimension(parentHeight, parentHeight);
            }else{
                Log.d("TAG", "onMeasure: width " + parentWidth);
                this.setMeasuredDimension(parentWidth, parentWidth);
            }

        }
        else{
            this.setMeasuredDimension(parentWidth, parentHeight);

        }

        if (square == true){
            if (matchWidth ==false) {
                super.onMeasure(heightMeasureSpec, heightMeasureSpec);
            }else{
                super.onMeasure(widthMeasureSpec, widthMeasureSpec);
            }
        }else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }





    @Override
    public void onDraw(Canvas canvas){

        if ( maskResId != 0 && getMeasuredHeight() > 0 && getMeasuredWidth() > 0){
            if (getDrawable() == null){
                super.onDraw(canvas);
                return;
            }

            BitmapShader shader;
            Bitmap mainBitmap;
            if ((getDrawable()) instanceof BitmapDrawable){
                mainBitmap = ((BitmapDrawable)getDrawable()).getBitmap();
            }else{
                mainBitmap = GUI.drawableToBitmap(getContext(),getDrawable(), getMeasuredWidth(), getMeasuredHeight());
            }
            shader = new BitmapShader(mainBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

            shader.setLocalMatrix(getImageMatrix());
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(shader);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawBitmap(GUI.convertToAlphaMask(GUI.drawableToBitmap(getContext(), maskResId, getMeasuredWidth() , getMeasuredHeight() )), 0, 0, paint);
        }else{
            super.onDraw(canvas);
        }
    }
}