package com.meelo2.mobi.GUI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.meelo2.mobi.activities.BaseActivity;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.Utils.GUI;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

/**
 * Created by jzvia on 24/01/2018.
 */

public class GalleryView extends LinearLayout {
    private View view;


    private RecyclerView recyclerView;
    private RecyclerView recyclerViewAlbums;
    private List<String> picUrls = new LinkedList<String>();
    private List<String> picUrlsFullSize = new LinkedList<String>();
    private SelectedItemCallback selectedItemCallback;

    public void setSelectedItemCallback(SelectedItemCallback selectedItemCallback) {
        this.selectedItemCallback = selectedItemCallback;
    }

    private List<Album> albumList = new LinkedList<Album>();

    private class Album{
        String id;
        String name;
    }
    public interface SelectedItemCallback{
         void  itemSelected(Uri uri, boolean isWeb);
    }

    class UserPictureHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;

        public UserPictureHolder(View itemView) {
            super(itemView);
        }

    }


    class AlbumHolder extends RecyclerView.ViewHolder{
        TextView textView;

        public AlbumHolder(View itemView) {
            super(itemView);


        }

    }



    public GalleryView(Context context) {
        super(context);
        init();

    }

    public GalleryView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GalleryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public GalleryView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        view = LayoutInflater.from(getContext()).inflate(R.layout.gallery_view, this,true);
        recyclerView = findViewById(R.id.recycler_view);
        findViewById(R.id.button_from_gallery).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                //photoPickerIntent.setType(/*"image/*"*/android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Do not include the extras related to cropping here;
                // this Intent is for selecting the image only.
                ((BaseActivity)getContext()).startActivityForResult(photoPickerIntent, new BaseActivity.ActivityResultHandler() {
                    @Override
                    public void onActivityResult(int resultCode, Intent data) {
                        if (resultCode == Activity.RESULT_OK) {
                            if (selectedItemCallback != null){
                                selectedItemCallback.itemSelected(data.getData(),false);
                            }


                        }
                    }
                });
            }
        });
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (recyclerView.isLaidOut() == true) {

                    recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    recyclerView.getLayoutParams().height = recyclerView.getMeasuredWidth();
                    recyclerView.requestLayout();
                }

            }
        });
        recyclerViewAlbums = findViewById(R.id.recycler_view_albums);

        Log.d("TAG", "preShowInit: " + recyclerViewAlbums);
        loadAlbums();

    }


    private void loadAlbums() {
        final Bundle parameters = new Bundle();
        parameters.putString("limit", "1000");


        new GraphRequest(AccessToken.getCurrentAccessToken(), "me/albums", parameters, HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            System.out.println(response.getJSONObject());
                            final JSONArray jsonArray = response.getJSONObject().getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Album album = new Album();
                                album.id = jsonArray.getJSONObject(i).getString("id");
                                album.name = jsonArray.getJSONObject(i).getString("name");
                                albumList.add(album);

                            }
                            if (albumList.size()== 0 ){
                                return;
                            }
                            loadPhotos(albumList.get(0).id);

                            recyclerViewAlbums.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            recyclerViewAlbums.setAdapter(new RecyclerView.Adapter<AlbumHolder>() {

                                @Override
                                public AlbumHolder onCreateViewHolder(final ViewGroup parent, int viewType) {


                                    return new AlbumHolder(LayoutInflater.from(getContext()).inflate(R.layout.photo_album_cell,parent, false)){
                                        {

                                            textView = (TextView) itemView.findViewById(R.id.text_view);
                                        }
                                    };

                                }

                                @Override
                                public int getItemCount() {



                                    return albumList.size();
                                }

                                @Override
                                public void onBindViewHolder(final AlbumHolder holder, final int position) {
                                    final Album album = albumList.get(position);
                                    holder.textView.setText(album.name);
                                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            loadPhotos(album.id);
                                        }
                                    });



                                }
                            } );

                            OverScrollDecoratorHelper.setUpOverScroll(recyclerViewAlbums, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);


                        } catch (JSONException ex) {
                        }


                    }
                }).executeAsync();


    }

    private void loadPhotos(String albumId) {
        final Bundle parameters = new Bundle();
        parameters.putString("limit", "10000");
        parameters.putString("fields", "images");


        new GraphRequest(AccessToken.getCurrentAccessToken(), albumId + "/photos", parameters, HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            final JSONArray jsonArray = response.getJSONObject().getJSONArray("data");

                            System.out.println(jsonArray);

                            initRecycler();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONArray images = jsonArray.getJSONObject(i).getJSONArray("images");
                                String url = images.getJSONObject(images.length() - 1).getString("source");
                                String urlFullSize = images.getJSONObject(0).getString("source");
                                Log.d("TAG", "onCompleted: " + url);
                                picUrls.add(url);
                                picUrlsFullSize.add(urlFullSize);
                            }

                            recyclerView.getAdapter().notifyDataSetChanged();


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }).executeAsync();


    }
    private void initRecycler(){
        picUrls.clear();
        picUrlsFullSize.clear();
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3,LinearLayoutManager.VERTICAL, false));

        recyclerView.setAdapter(new RecyclerView.Adapter<UserPictureHolder>() {

            @Override
            public UserPictureHolder onCreateViewHolder(final ViewGroup parent, int viewType) {


                return new UserPictureHolder(LayoutInflater.from(getContext()).inflate(R.layout.gallery_small_picture,parent, false)){
                    {

                        imageView =  itemView.findViewById(R.id.image_view);
                        ((CustomImageView)imageView).setSquare(true);
                        ((CustomImageView)imageView).setMatchWidth(true);
                        ((CustomImageView)imageView).setMaskResId(R.drawable.small_picture_icon_frame);



                    }
                };

            }

            @Override
            public int getItemCount() {



                return picUrls.size();
            }

            @Override
            public void onBindViewHolder(final UserPictureHolder holder, final int position) {
                final ImageView imageView = ((UserPictureHolder) holder).imageView;
                GUI.loadImage(getContext(),picUrls.get(position), imageView, GUI.Quality.QUARTER_WIDTH_565);
                ((UserPictureHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (selectedItemCallback != null){
                            selectedItemCallback.itemSelected(Uri.parse(picUrlsFullSize.get(position)), true);
                        }

                    }
                });


            }
        } );
        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
    }








}
