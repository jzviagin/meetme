package com.meelo2.mobi.GUI;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.meelo2.mobi.api.Thread;

/**
 * Created by jzvia on 15/01/2018.
 */

public class MyEditText extends EditText {




    private boolean isFocused = false;

    public MyEditText(Context context) {
        super(context);
        init();
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setShowSoftInputOnFocus(false);
        }


        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                UIcontroller.getInstance().showSoftKeyboard();

            }
        });
    }




    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        Log.d("TAG", "onFocusChanged: " + focused);
        isFocused = focused;
        super.onFocusChanged(focused, direction, previouslyFocusedRect);

        if (focused == true){
            Thread.waitAndRunOnUi(new Runnable() {
                @Override
                public void run() {
                    if (isFocused == true){
                        UIcontroller.getInstance().showSoftKeyboard();

                    }
                }
            },200);
        }
        else{
            UIcontroller.getInstance().hideSoftKeyboard();
        }
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if ((event.getAction() == KeyEvent.ACTION_UP) && (keyCode == KeyEvent.KEYCODE_BACK)) {
            UIcontroller.getInstance().hideSoftKeyboard();
        }
        return super.onKeyPreIme(keyCode, event);
    }
}
