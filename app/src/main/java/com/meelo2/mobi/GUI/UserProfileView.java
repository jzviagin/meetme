package com.meelo2.mobi.GUI;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.meelo2.mobi.app.R;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.Utils.Rest;
import com.meelo2.mobi.entities.User;

import java.util.Arrays;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

/**
 * Created by jzvia on 24/01/2018.
 */

public class UserProfileView extends LinearLayout {
    private RecyclerView recyclerView;
    private View view;
    private Runnable runnable;
    private ProfileCompleteListener listener;


    public interface ProfileCompleteListener{
        void onProfileCompleteChanged(boolean canProceed);
    }


    private class UserPictureHolder extends RecyclerView.ViewHolder{

        public CustomImageView imageView;
        public ImageButton imageButton;
        public ImageButton imageButtonAdd;
        public UserPictureHolder(View itemView) {
            super(itemView);
        }

    }

    public UserProfileView(Context context) {
        super(context);
        init();

    }

    public UserProfileView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UserProfileView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public UserProfileView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        view = LayoutInflater.from(getContext()).inflate(R.layout.user_profile_view, this,true);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (recyclerView.isLaidOut() == true) {

                    recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    recyclerView.getLayoutParams().height = recyclerView.getMeasuredWidth()*3/4;
                    recyclerView.requestLayout();
                }

            }
        });

        ((CheckBox)view.findViewById(R.id.checkbox_men)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                prefGenderChanged(buttonView);
            }
        });

        ((CheckBox)view.findViewById(R.id.checkbox_women)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateCurrentUser();
                prefGenderChanged(buttonView);
            }
        });

        CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateCurrentUser();
                listener.onProfileCompleteChanged(readyToSave());
            }
        };
        ((RadioButton)view.findViewById(R.id.radiobutton_male)).setOnCheckedChangeListener(onCheckedChangeListener);
        ((RadioButton)view.findViewById(R.id.radiobutton_female)).setOnCheckedChangeListener(onCheckedChangeListener);

        ((CheckBox)view.findViewById(R.id.checkbox_men)).setTypeface(ResourcesCompat.getFont(getContext(), R.font.custom2), Typeface.BOLD);
        ((CheckBox)view.findViewById(R.id.checkbox_women)).setTypeface(ResourcesCompat.getFont(getContext(), R.font.custom2), Typeface.BOLD);

    }

    private boolean prefGenderChanged(CompoundButton compoundButton){
        boolean retVal = false;
        if ( ((CheckBox)view.findViewById(R.id.checkbox_men)).isChecked() == false && ((CheckBox)view.findViewById(R.id.checkbox_women)).isChecked() == false){
            compoundButton.setChecked(true);
            retVal = true;
        }else{
            retVal= false;
        }
        listener.onProfileCompleteChanged(readyToSave());
        return retVal;

    }


    public boolean readyToSave(){
        if (((((RadioButton)view.findViewById(R.id.radiobutton_male)).isChecked() == true) ||(((RadioButton)view.findViewById(R.id.radiobutton_female)).isChecked() == true))
            && (  ( (((CheckBox)view.findViewById(R.id.checkbox_men)).isChecked() == true) || (((CheckBox)view.findViewById(R.id.checkbox_women)).isChecked() == true) ) )
                && recyclerView.getAdapter() != null && recyclerView.getAdapter().getItemCount() != 1)
        {
            return true;
        }else{
            return false;
        }
    }

    private void updateCurrentUser(){
        User.getCurrent().setBio(((EditText)findViewById(R.id.edit_text_bio)).getText().toString());
        if (((RadioButton)view.findViewById(R.id.radiobutton_male)).isChecked() == true){
            User.getCurrent().setGender("man");
        }else{
            User.getCurrent().setGender("woman");
        }
        if ( (((CheckBox)view.findViewById(R.id.checkbox_men)).isChecked() == true) && (((CheckBox)view.findViewById(R.id.checkbox_women)).isChecked() == true) ){
            User.getCurrent().setGenderPreference(new String [] {"man", "woman"});
        }else if ((((CheckBox)view.findViewById(R.id.checkbox_men)).isChecked() == true)){
            User.getCurrent().setGenderPreference(new String [] {"man"});
        }else {
            User.getCurrent().setGenderPreference(new String [] {"woman"});
        }
    }

    public void saveData(){
        updateCurrentUser();
        for (int i = 0 ;i < User.getCurrent().getPicList().length ; i++){
            Uri uri = Uri.parse(User.getCurrent().getPicList()[i]);
            if (User.getCurrent().getPicList()[i].contains("file:") == true) {
                Drawable drawable = Drawable.createFromPath(uri.getPath());
                String url = Rest.getInstance().uploadPhoto(drawable);
                Log.d("TAG", "saveData: " + url);
                User.getCurrent().getPicList()[i] = Uri.parse(url).getLastPathSegment();
            }
            else {
                User.getCurrent().getPicList()[i] = uri.getLastPathSegment();
            }

        }
        Rest.getInstance().update(User.class, User.getCurrent());


    }


    public void loadData(final Runnable onPhotoClicked, final ProfileCompleteListener listener){
        Log.d("TAG", "loadPhotos: ");


        this.runnable = onPhotoClicked;

        this.listener = listener;

        ((RadioGroup)findViewById(R.id.radio_group)).clearCheck();
        ((EditText)findViewById(R.id.edit_text_bio)).setText(User.getCurrent().getBio());

        if (User.getCurrent().getGender().equalsIgnoreCase("man")){
            ((RadioButton)view.findViewById(R.id.radiobutton_male)).setChecked(true);
        }else if (User.getCurrent().getGender().equalsIgnoreCase("woman")){
            ((RadioButton)view.findViewById(R.id.radiobutton_female)).setChecked(true);
        }

        ((CheckBox)view.findViewById(R.id.checkbox_men)).setChecked(false);
        ((CheckBox)view.findViewById(R.id.checkbox_women)).setChecked(false);





        if (Arrays.asList(User.getCurrent().getGenderPreference()).contains("man")){
            ((CheckBox)view.findViewById(R.id.checkbox_men)).setChecked(true);
        }
        if (Arrays.asList(User.getCurrent().getGenderPreference()).contains("woman")){
            ((CheckBox)view.findViewById(R.id.checkbox_women)).setChecked(true);
        }

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4, LinearLayoutManager.VERTICAL, false));

        recyclerView.setAdapter(new RecyclerView.Adapter<UserPictureHolder>() {



            @Override
            public UserPictureHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                Log.d("TAG", "onCreateViewHolder: ");
                return new UserPictureHolder(LayoutInflater.from(getContext()).inflate(R.layout.photo_selection_small_picture,parent, false)){
                    {

                        imageView = (CustomImageView) itemView.findViewById(R.id.image_view);
                        imageView.setSquare(true);
                        imageView.setMatchWidth(true);
                        imageButton = itemView.findViewById(R.id.button_delete);
                        imageButtonAdd = itemView.findViewById(R.id.button_add);

                    }
                };

            }

            @Override
            public int getItemCount() {
                int itemCount = User.getCurrent().getPicList().length + 1 ;

                return itemCount;
            }

            @Override
            public void onBindViewHolder(final UserPictureHolder holder, final int position) {
                CustomImageView imageView = ((UserPictureHolder) holder).imageView;


                if (listener != null) {
                    listener.onProfileCompleteChanged(readyToSave());
                }



                Log.d("TAG", "onBindViewHolder: item count " + getItemCount());
                Log.d("TAG", "onBindViewHolder: position " + position);

                if (position < getItemCount() - 1) {
                    holder.imageButton.setVisibility(View.VISIBLE);
                    holder.imageView.setVisibility(VISIBLE);
                    holder.imageButtonAdd.setVisibility(View.GONE);
                    holder.imageButtonAdd.setOnClickListener(null);
                    holder.imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            User.getCurrent().removePhoto(position);
                            notifyDataSetChanged();
                        }
                    });
                    if (getItemCount() == 2){
                        holder.imageButton.setVisibility(View.GONE);
                    }
                    final User user = User.getCurrent();
                    imageView.setMaskResId(R.drawable.small_picture_icon_frame);
                    GUI.loadImage(getContext(),user.getPicList()[position], imageView, GUI.Quality.QUARTER_WIDTH_565);
                }else {
                    //Drawable drawable = context.getResources().getDrawable(R.drawable.plus);
                    //drawable.setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
                    holder.imageButton.setVisibility(View.GONE);
                    holder.imageButtonAdd.setVisibility(View.VISIBLE);
                    holder.imageView.setVisibility(GONE);
                    holder.imageButtonAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            recyclerView.setLayoutManager(null);
                            recyclerView.setAdapter(null);

                            runnable.run();
                        }
                    });
                    //imageView.setImageDrawable(drawable);

                }

            }
        } );

        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);


        listener.onProfileCompleteChanged(readyToSave());


    }


}
