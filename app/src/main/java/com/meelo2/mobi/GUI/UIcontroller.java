package com.meelo2.mobi.GUI;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.meelo2.mobi.app.R;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.activities.MainActivity;
import com.meelo2.mobi.api.Thread;
import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by jzvia on 16/01/2018.
 */

public class UIcontroller {
    private static UIcontroller instance;
    private MainActivity.ControlProxy controlProxy;
    public static MyFragNavController.FragNavProxy fragNavProxy;

    private boolean runThread = true;


    private Queue<Runnable> executionQueue = new ConcurrentLinkedQueue<>();


    private int selectedTab = 0;
    private boolean checkedIn =  false;
    private boolean keyboardShown = false;

    private boolean disableBackButton = false;


    public interface PreRenderCallback{
        public void onRenderDone();
    }

    public static UIcontroller getInstance(){
        return instance;
    }

    private UIcontroller(){

    }

    private void updateUI(){

        executionQueue.add(new Runnable() {
            @Override
            public void run() {
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        controlProxy.alignUI(keyboardShown,fragNavProxy.isCurrentFragmentFullScreen() , fragNavProxy.isCurrentFragmentWithToolbar() );

                        if (fragNavProxy.isCurrentFragmentFullScreen() == true){
                            enableFullscreen();
                        }else {
                            if (keyboardShown == true){
                                disableFullscreen();
                            }else {
                                enableFullscreen();
                            }
                        }
                        if (keyboardShown == true){
                            Thread.waitAndRunOnUi(new Runnable() {
                                @Override
                                public void run() {
                                    fragNavProxy.scrollToBottom();
                                }
                            },1500);

                        }
                    }
                });
            }
        });



    }

    public void startFragment(final Fragment fragment){
        final FragNavTransactionOptions fragNavTransactionOptions = FragNavTransactionOptions.newBuilder().customAnimations(R.anim.enter_from_right,R.anim.exit_out_left/*,R.anim.enter_from_right,R.anim.exit_out_left*/).build();

        fragNavProxy.pushFragment(fragment, fragNavTransactionOptions);


        updateUI();
    }

    public void addToolbar(final View toolbar,final  boolean setAsActionBar, final GUI.CollapseExpandListener collapseExpandListener){
        controlProxy.addCustomToolbar(toolbar, setAsActionBar, collapseExpandListener);
    }

    public void preRenderView(final View view, final UIcontroller.PreRenderCallback preRenderCallback){
        controlProxy.preRenderView(view, preRenderCallback);
    }

    public void clearToolbar(final GUI.CollapseExpandListener collapseExpandListener){

        controlProxy.clearCustomToolbar(collapseExpandListener);
    }

    public void showAnimation(int imageResId, int colorResId){
        controlProxy.showAnimation(imageResId, colorResId);
    }

    public void setBackground(Drawable drawable) {
        controlProxy.setBackground(drawable);
    }

    public void checkIn(){
        checkedIn = true;


        controlProxy.performWhenActive(new Runnable() {
            @Override
            public void run() {
                controlProxy.checkIn(/*new Runnable() {
                    @Override
                    public void run() {
                        switchTab(1);
                    }
                }*/);

                fragNavProxy.refreshCurrentFragment();
                //updateUI();
            }
        });
    }
    public  void performLongOperation(final Runnable runnable, final Runnable postExecute){
            executionQueue.add(new Runnable() {
                @Override
                public void run() {
                    final AtomicBoolean isDone = new AtomicBoolean(false);
                    disableBackButton = true;
                    Log.d("TAG", "run: performLongOperation started" );
                    Thread.runOnUi(new Runnable() {
                        @Override
                        public void run() {
                            controlProxy.showProgressOverlay(new GUI.CollapseExpandListener() {
                                @Override
                                public void onComplete(View view) {
                                    runnable.run();
                                    Thread.runOnUi(new Runnable() {
                                        @Override
                                        public void run() {
                                            isDone.set(true);
                                            controlProxy.hideProgressOverlay(new GUI.CollapseExpandListener() {
                                                @Override
                                                public void onComplete(View view) {
                                                    disableBackButton = false;
                                                    Thread.runOnUi(postExecute);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                    Log.d("TAG", "run: performLongOperation finished" );

                    while (isDone.get() == false){
                        Thread.sleep(50);
                    }
                }
            });


    }

    public void checkOut(){
        final int currentTab = fragNavProxy.getCurrentTab();

        Log.d("TAG", "checkOut: current tab " + currentTab);


        controlProxy.performWhenActive(new Runnable() {
            @Override
            public void run() {
                controlProxy.checkOut(new Runnable() {
                    @Override
                    public void run() {
                        if (currentTab == 0){
                            fragNavProxy.refreshCurrentFragment();
                        }/*else {
                            Thread.waitAndRunOnUi(new Runnable() {
                                @Override
                                public void run() {
                                    switchTab(0);

                                }
                            },5000);
                        }*/
                        //
                    }
                });


                //updateUI();
            }
        });
        checkedIn = false;
        //switchTab(0);

       // controlProxy.checkOut(runnable);

        //fragNavProxy.refreshCurrentFragment();
    }

    public boolean isCheckedIn(){
        return checkedIn;
    }




    public void switchTab(final int tab){
        if (tab != selectedTab) {
            selectedTab = tab;
            controlProxy.switchTab(tab);
            controlProxy.performWhenActive(new Runnable() {
                @Override
                public void run() {
                    Thread.runOnUi(new Runnable() {
                        @Override
                        public void run() {
                            switch (tab){
                                case 0:
                                    fragNavProxy.switchTab(FragNavController.TAB1);
                                    break;
                                case 1:

                                    fragNavProxy.switchTab(FragNavController.TAB2);
                                    break;
                                case 2:
                                    fragNavProxy.switchTab(FragNavController.TAB3);

                                    break;
                                case 3:
                                    fragNavProxy.switchTab(FragNavController.TAB4);
                                    break;
                                default:
                            }
                            updateUI();
                        }
                    });
                }
            });

        }else {
            FragNavTransactionOptions fragNavTransactionOptions = FragNavTransactionOptions.newBuilder().customAnimations(R.anim.enter_from_right,R.anim.exit_out_left,R.anim.enter_from_left,R.anim.exit_out_right).build();
            if (fragNavProxy.getCurrentStackSize() > 1) {
                fragNavProxy.popFragments(fragNavProxy.getCurrentStackSize() - 1, fragNavTransactionOptions);
            }

        }
    }





    public boolean goBack() {
        if (disableBackButton == true){
            return true;
        }
        FragNavTransactionOptions fragNavTransactionOptions = FragNavTransactionOptions.newBuilder().customAnimations(R.anim.enter_from_right,R.anim.exit_out_left,R.anim.enter_from_left,R.anim.exit_out_right).build();

        if (fragNavProxy.getCurrentStackSize() == 1){
            return false;
        }
        fragNavProxy.popFragment(fragNavTransactionOptions);

        updateUI();
        return  true;
    }

    public boolean goBack(int count) {
        Log.d("TAG", "goBack: ");
        if (disableBackButton == true){
            return true;
        }
        FragNavTransactionOptions fragNavTransactionOptions = FragNavTransactionOptions.newBuilder().customAnimations(R.anim.enter_from_right,R.anim.exit_out_left,R.anim.enter_from_left,R.anim.exit_out_right).build();
        if (fragNavProxy.getCurrentStackSize() == 1){
            return false;
        }
        fragNavProxy.popFragments(count, fragNavTransactionOptions);

        updateUI();
        return  true;
    }

    public void hideSoftKeyboard() {
        keyboardShown = false;

        if (controlProxy.getActivity().getCurrentFocus() == null){
            updateUI();
            return;
        }
        InputMethodManager inputMethodManager =
                (InputMethodManager) controlProxy.getActivity().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                controlProxy.getActivity().getCurrentFocus().getWindowToken(), 0);

        updateUI();

    }

    public void showSoftKeyboard() {

        keyboardShown = true;

        InputMethodManager inputMethodManager =
                (InputMethodManager) controlProxy.getActivity().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(
                controlProxy.getActivity().getCurrentFocus(), InputMethodManager.SHOW_IMPLICIT);
        updateUI();
    }


    private void enableFullscreen(){






        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = controlProxy.getActivity().getWindow(); // in Activity's onCreate() for instance
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            Log.d("TAG", "enableFullscreen: ");
        }

    }

    private void disableFullscreen(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Log.d("TAG", "disableFullscreen: ");
            final Window w = controlProxy.getActivity().getWindow(); // in Activity's onCreate() for instance
            w.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);


        }

    }


    private void destroy(){
        runThread = false;
    }


    public static void init(MainActivity.ControlProxy controlProxy){
        if(instance != null) {
            instance.destroy();

        }
        instance = new UIcontroller();
        instance.controlProxy = controlProxy;


        MyFragNavController.init(R.id.container,controlProxy.getActivity());




        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                final UIcontroller finalInstance = instance;
                Log.d("TAG", "run: UI controller thread started");
                while (finalInstance.runThread == true){
                    if (finalInstance.executionQueue.isEmpty() == false){
                        Runnable r = finalInstance.executionQueue.remove();
                        r.run();
                    }
                    Thread.sleep(10);
                }
                Log.d("TAG", "run: UI controller thread done");
            }
        });
        instance.enableFullscreen();
        instance.switchTab(3);

    }
}
