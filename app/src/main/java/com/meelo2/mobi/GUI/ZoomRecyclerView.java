package com.meelo2.mobi.GUI;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.meelo2.mobi.api.Thread;

import java.util.Date;

/**
 * Created by jzvia on 12/01/2018.
 */

public class ZoomRecyclerView extends RecyclerView {


    private int initialHeight = 200;
    LinearLayoutManager linearLayoutManager = null;
    int touchedPosition = 0;
    int touchedViewOffset = 0;
    private float startX;
    private float zoom = 3;
    private float scrollMultiplier = 1;
    private final static long zoomDelay = 150;
    private final static long noScrollZoomDelay = 300;
    private final static float scrollThreshold = 50;
    private boolean zoomRequest = false;
    private Date noScrollZoomRequest = new Date();
    private float totalScroll = 0;
    private boolean isZoomedIn = false;




    @Override
    public void setLayoutManager(LayoutManager layout) {
        if (layout instanceof LinearLayoutManager == false){
            System.out.print("setLayoutManager: only LinearLayoutManager supported");
            return;
        }
        linearLayoutManager = (LinearLayoutManager) layout;
        super.setLayoutManager(layout);
    }

    public void setInitialHeight(int height){
        initialHeight = height;
        getLayoutParams().height = initialHeight;
        requestLayout();
    }

    public ZoomRecyclerView(Context context) {
        super(context);
        init();
    }

    public ZoomRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ZoomRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    private void init(){
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        super.setLayoutManager(linearLayoutManager);

    }

    @Override
    public boolean dispatchDragEvent(DragEvent event) {

        addOnItemTouchListener(new OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }


        });

        return super.dispatchDragEvent(event);
    }



    public void setZoom(float zoom){
        this.zoom = zoom;
    }

    @Override
    public ViewHolder findViewHolderForLayoutPosition(int position) {
        return super.findViewHolderForLayoutPosition(position);
    }

    @Override
    public View findChildViewUnder(float x, float y) {
        return super.findChildViewUnder(x, y);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent e) {




        if (e.getAction() == MotionEvent.ACTION_DOWN){
            totalScroll = 0;

            View view = findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                touchedPosition = linearLayoutManager.getPosition(view);
                touchedViewOffset = linearLayoutManager.getDecoratedLeft(view);
                startX = e.getRawX();

                zoomRequest = true;

                Thread.waitAndRunOnUi(new Runnable() {
                    @Override
                    public void run() {
                        if (zoomRequest == true){
                            zoomRequest = false;
                            Log.d("TAG", "run: zoom started animation after touch " + touchedPosition + "offset " + touchedViewOffset);

                            resizeToHeight((int) (initialHeight * zoom));
                            isZoomedIn = true;
                        }
                    }
                }, zoomDelay);


            }
        }

        if (e.getAction() == MotionEvent.ACTION_UP){
            zoomRequest = false;

            if (isZoomedIn == true) {

                Log.d("TAG", "run: zoom out position " + touchedPosition);

                resizeToHeight((int) (initialHeight));
                isZoomedIn = false;
            }

            noScrollZoomRequest = null;
        }



        if (e.getAction() == MotionEvent.ACTION_MOVE){



            float delta = e.getRawX() - startX;
            totalScroll += delta;
            startX = e.getRawX();
            scrollBy( (int)delta - (int)(delta * scrollMultiplier), 0);
            if (Math.abs(totalScroll) > scrollThreshold){
                zoomRequest = false;
            }


            if (isZoomedIn == false && zoomRequest == false) {

                noScrollZoomRequest = new Date();
                Thread.waitAndRunOnUi(new Runnable() {
                    @Override
                    public void run() {
                        if (noScrollZoomRequest != null && noScrollZoomRequest.getTime() + noScrollZoomDelay < new Date().getTime()) {
                            noScrollZoomRequest = null;
                            int [] location = new int[2];
                            getLocationOnScreen(location);
                            final View view = findChildViewUnder(e.getRawX() - location[0], e.getRawY()- location[ 1]);
                            if (view != null) {


                                Log.d("TAG", "run: zoom started animation after scroll position" + linearLayoutManager.getPosition(view));
                                touchedPosition = linearLayoutManager.getPosition(view);
                                touchedViewOffset = linearLayoutManager.getDecoratedLeft(view);
                                resizeToHeight((int) (initialHeight * zoom));
                                isZoomedIn = true;
                            }

                        }
                    }
                }, noScrollZoomDelay);
            }
        }
        return super.onTouchEvent(e);



    }


    private int getMidPosition(){
        int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
        int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();
        int midPoint = getMeasuredWidth() / 2;

        for (int i = firstVisiblePosition ; i < lastVisiblePosition + 1; i++){
            View view = linearLayoutManager.findViewByPosition(i);
            int left = linearLayoutManager.getDecoratedLeft(view);
            int right = linearLayoutManager.getDecoratedRight(view);
            if (midPoint >= left && midPoint <= right){
                return i;
            }
        }
        return 0;

    }



    private  void resizeToHeight(final int height) {

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {


                final int preAnimationHeight = getMeasuredHeight();
                final int destPosition;
                final int destOffset;
                final int midPosition = getMidPosition();
                final View midPositionView = linearLayoutManager.findViewByPosition(midPosition);
                final int midPositionViewOffset = linearLayoutManager.getDecoratedLeft(midPositionView);
                final float zoomToApply = (float) height / (float)getMeasuredHeight();
                final float viewRatio = (float)midPositionView.getMeasuredWidth() / (float) midPositionView.getMeasuredHeight();
                final int finalViewHeight = (int)(midPositionView.getMeasuredHeight() * zoomToApply);
                final int finaViewWidth = (int)(finalViewHeight * viewRatio);
                final int startOffset;
                final int visibleWidthAfterZoom = (int)(getMeasuredWidth() * zoomToApply);
                final View touchedView = linearLayoutManager.findViewByPosition(touchedPosition);
                View scrollToView;

                if (height > initialHeight){
                    scrollToView = touchedView;
                    destOffset = (getMeasuredWidth() - finaViewWidth)/2;

                }else{
                    boolean hasScrolled = false;
                    if (getMeasuredHeight() < initialHeight * zoom){
                        scrollToView = touchedView;
                        destOffset = touchedViewOffset;
                    }else {
                        if (midPosition != touchedPosition ||
                                10 < Math.abs(linearLayoutManager.getDecoratedLeft(midPositionView) - (getMeasuredWidth() - midPositionView.getMeasuredWidth()) / 2)) {
                            hasScrolled = true;
                            Log.d("TAG", "zoom out view has scrolled");
                        }
                        if (hasScrolled == true/*midPosition != touchedPosition*/) {
                            scrollToView = midPositionView;
                            destOffset = (getMeasuredWidth() - visibleWidthAfterZoom) / 2 + (int) (midPositionViewOffset * zoomToApply);

                        } else {

                            scrollToView = midPositionView;
                            destOffset = touchedViewOffset;

                        }
                    }

                }

                destPosition = linearLayoutManager.getPosition(scrollToView);

                startOffset = linearLayoutManager.getDecoratedLeft(linearLayoutManager.findViewByPosition(destPosition));



                ObjectAnimator scaleY = ObjectAnimator.ofFloat(ZoomRecyclerView.this, "scaleY", 0f, 1f);
                scaleY.setDuration(100);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){

                    @Override
                    public float getInterpolation(float input) {


                        int newHeight = preAnimationHeight + (int)(input * (float) (height - preAnimationHeight));

                       // Log.d("TAG", "getInterpolation: " + newHeight);

                        scrollMultiplier = (float) newHeight / (float) initialHeight;
                        getLayoutParams().height = newHeight;
                        int scrollToOffset = startOffset - (int)((startOffset - destOffset) * input);
                        linearLayoutManager.scrollToPositionWithOffset(destPosition, scrollToOffset);
                        requestLayout();
                        return super.getInterpolation(1);
                    }
                });
                animationSet.play(scaleY);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                });
                animationSet.start();
            }
        });

    }


}
