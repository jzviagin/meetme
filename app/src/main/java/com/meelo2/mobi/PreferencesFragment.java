package com.meelo2.mobi;

/**
 * Created by jzvia on 18/12/2017.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.meelo2.mobi.api.Kvs;
import com.meelo2.mobi.app.R;


public class PreferencesFragment extends BaseNonRootFragment {

    {
        toolbarResId = R.id.app_bar_layout;
    }



    public static void showPreferences(Context context){

        PreferencesFragment preferencesFragment = new PreferencesFragment();
        preferencesFragment.init(R.layout.preferences_fragment,context);
    }


    @Override
    protected void postShowInit() {
        //loadPhotos();
    }

    @Override
    protected void preShowInit() {

        Log.d("TAG", "preShowInit: ");

        ((TextView)getToolbar().findViewById(R.id.title_name)).setText("Preferences");
        if (Kvs.get("disablePush") == null){
            ((Switch)rootView.findViewById(R.id.switch_push_notifications)).setChecked(true);
            enableToggles();
        }else{
            ((Switch)rootView.findViewById(R.id.switch_push_notifications)).setChecked(false);
            disableToggles();
        }

        ((Switch)rootView.findViewById(R.id.switch_push_notifications)).setTypeface(ResourcesCompat.getFont(context, R.font.custom2), Typeface.BOLD);
        ((Switch)rootView.findViewById(R.id.switch_vibrate)).setTypeface(ResourcesCompat.getFont(context, R.font.custom2), Typeface.BOLD);
        ((Switch)rootView.findViewById(R.id.switch_sound)).setTypeface(ResourcesCompat.getFont(context, R.font.custom2), Typeface.BOLD);

        if (Kvs.get("disableVibration") == null){
            ((Switch)rootView.findViewById(R.id.switch_vibrate)).setChecked(true);
        }else{
            ((Switch)rootView.findViewById(R.id.switch_vibrate)).setChecked(false);
        }

        if (Kvs.get("disableSound") == null){
            ((Switch)rootView.findViewById(R.id.switch_sound)).setChecked(true);
        }else{
            ((Switch)rootView.findViewById(R.id.switch_sound)).setChecked(false);
        }

        ((Switch)rootView.findViewById(R.id.switch_push_notifications)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true){
                    Kvs.delete("disablePush");
                    enableToggles();
                }else{
                    Kvs.put("disablePush","");
                    disableToggles();
                }
            }
        });

        ((Switch)rootView.findViewById(R.id.switch_vibrate)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true){
                    Kvs.delete("disableVibration");
                }else{
                    Kvs.put("disableVibration","");
                }
            }
        });

        ((Switch)rootView.findViewById(R.id.switch_sound)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true){
                    Kvs.delete("disableSound");
                }else{
                    Kvs.put("disableSound","");
                }
            }
        });


    }

    private void enableToggles(){
        ((Switch)rootView.findViewById(R.id.switch_vibrate)).setEnabled(true);
        ((Switch)rootView.findViewById(R.id.switch_sound)).setEnabled(true);

    }

    private void disableToggles(){
        ((Switch)rootView.findViewById(R.id.switch_vibrate)).setEnabled(false);
        ((Switch)rootView.findViewById(R.id.switch_sound)).setEnabled(false);

    }






}



