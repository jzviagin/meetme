package com.meelo2.mobi;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.meelo2.mobi.GUI.CustomImageView;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.activities.MainActivity;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.HangoutLocation;
import com.squareup.picasso.Picasso;

/**
 * Created by jzvia on 29/08/2017.
 */

public class LocationsAdapter extends GeneralAdapter<HangoutLocation>{





    /*private static HangoutLocation[] hangoutLocationsArray = new HangoutLocation[]{
            new HangoutLocation("Dizzy", "Dizenghoff", 34 , null, "tons of pussy", null, null, R.mipmap.dizzy_frishdon,1),
            new HangoutLocation("El vacino", "Ibn Gabirol", 189 , null, "wine bar", null, null, R.mipmap.elvecino,2),
            new HangoutLocation("Inga", "Galgaley Ha-Plada", 16 , null, "MILFs", null, null, R.mipmap.inga,9),
            new HangoutLocation("Drink Point", "Ben yehuda", 142 , null, "Israeli Arsawats", null, null, R.mipmap.drink_point,0)
    };*/

    public LocationsAdapter(Context context, ItemClickedCallback itemClickedCallback, DataLoadedCallback dataLoadedCallback, final LatLng latLng , final float radius){
        //hangoutLocations.addAll(Arrays.asList(hangoutLocationsArray));

            super(HangoutLocation.class, context, itemClickedCallback, dataLoadedCallback, "?radius=" + radius + "&lat=" + latLng.latitude +
                    "&lng=" + latLng.longitude);


    }

    public LocationsAdapter(Context context, ItemClickedCallback itemClickedCallback, DataLoadedCallback dataLoadedCallback, final LatLng latLng , final float radius, String name){
        //hangoutLocations.addAll(Arrays.asList(hangoutLocationsArray));

        super(HangoutLocation.class, context, itemClickedCallback, dataLoadedCallback, "searchByName/?name=" + name);


    }


        class LocationAdapterHolder extends RecyclerView.ViewHolder{

            public TextView locationName;
            public TextView briefDescription;
            public CustomImageView locationImage;
            public TextView locationAddress;
            public TextView locationDistance;
            public LinearLayout starsLayout;
            public LocationAdapterHolder(View itemView) {
                super(itemView);
            }

        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolderImpl(final ViewGroup parent, int viewType) {


            return new LocationAdapterHolder(LayoutInflater.from(context).inflate(R.layout.home_screen_location_item,parent, false)){
                {
                    locationName = (TextView) itemView.findViewById(R.id.text_view_location_name);
                    locationAddress = (TextView) itemView.findViewById(R.id.text_view_address);
                    briefDescription = (TextView) itemView.findViewById(R.id.text_view_brief_description);
                    locationDistance = (TextView) itemView.findViewById(R.id.text_view_location_distance);
                    locationImage = (CustomImageView) itemView.findViewById(R.id.image_view_location_logo);
                    starsLayout = (LinearLayout) itemView.findViewById(R.id.stars_layout);


                }
            };

        }

        @Override
        public void onBindViewHolderImpl(final RecyclerView.ViewHolder holder, int position) {
            HangoutLocation hangoutLocation = (HangoutLocation) entities.get(position);
            ((LocationAdapterHolder)holder).locationName.setText(hangoutLocation.getName());
            holder.itemView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    if (v.isLaidOut()){


                    }
                }
            });
            ((LocationAdapterHolder)holder).itemView.setAlpha(0);
            GUI.fadeInView(((LocationAdapterHolder)holder).itemView, 0 , MainActivity.slowAnimationDuration, null);



            ((LocationAdapterHolder)holder).briefDescription.setText(hangoutLocation.getDescription());
            ((LocationAdapterHolder)holder).locationDistance.setText(String.format("%.1f", hangoutLocation.getDistance()) + " KM");
            ((LocationAdapterHolder)holder).locationAddress.setText(hangoutLocation.getAddress());
           // ((LocationAdapterHolder)holder).locationImage.setImageResource(hangoutLocation.getImageResourceId());
            for (int i = 0 ; i < 5 ; i++){
                ImageView starImageView = (ImageView) (((LocationAdapterHolder)holder).starsLayout.getChildAt(i));
                if (hangoutLocation.getRatings() != null) {
                    int resId = (i * 2) - hangoutLocation.getRatings()[0] >= 0 ? R.drawable.heart_emp :
                            (((i * 2) - hangoutLocation.getRatings()[0] == -1) ? R.drawable.heart_med : R.drawable.heart_full);
                    //resId = R.drawable.ic_favorite_black_24dp;
                    starImageView.setImageResource(resId);
                }

            }


            //loadImageWithTransformation

            //((LocationAdapterHolder)holder).locationImage.setImageBitmap(GUI.applyFilter(context, R.mipmap.dizzy_frishdon, R.drawable.bubble_left,((LocationAdapterHolder)holder).locationImage.getLayoutParams().width, ((LocationAdapterHolder)holder).locationImage.getLayoutParams().height ));



            ((LocationAdapterHolder)holder).locationImage.setMaskResId( R.drawable.small_picture_icon_frame);



            //((LocationAdapterHolder)holder).locationImage.setMaskResId(BitmapFactory.decodeResource(context.getResources(), R.drawable.bubble_right));
            Picasso.with(context).load(hangoutLocation.getImageUrl()).into(((LocationAdapterHolder)holder).locationImage);


            //GUI.loadImageWithTransformation(context, hangoutLocation.getImageUrl(), ((LocationAdapterHolder)holder).locationImage, GUI.Quality.FULL, R.drawable.button_background);


            Log.d("TAG", "onBindViewHolder: "  +  hangoutLocation.getImageUrl());




        }


}
