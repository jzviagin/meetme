package com.meelo2.mobi;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.meelo2.mobi.GUI.CustomImageView;
import com.meelo2.mobi.GUI.UIcontroller;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.Utils.Rest;;
import com.meelo2.mobi.activities.MainActivity;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.User;
import com.squareup.picasso.Picasso;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


public class MeetFragment extends BaseFragment implements FullScreenFragment {

    private List<User> userList;
    private BaseAdapter adapter;


   // private View rootView;

    // private OnFragmentInteractionListener mListener;


    public MeetFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    private final static float recyclerImageAspect = 0.8f;
    private final static int photosInRecycler = 3;

    private AtomicInteger requestsSent = new AtomicInteger(0);



    private void showProgressOverlay(GUI.CollapseExpandListener collapseExpandListener){
        rootView.findViewById(R.id.meet_save_layout).setAlpha(0f);
        rootView.findViewById(R.id.meet_save_layout).setVisibility(View.VISIBLE);
        GUI.fadeInView(rootView.findViewById(R.id.meet_save_layout),0, MainActivity.defaultAnimationDuration, collapseExpandListener);
    }

    private void hideProgressOverlay(final GUI.CollapseExpandListener collapseExpandListener){
        GUI.fadeOutView(rootView.findViewById(R.id.meet_save_layout), 0, MainActivity.defaultAnimationDuration, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        rootView.findViewById(R.id.meet_save_layout).setVisibility(View.GONE);

                    }
                });
                if (collapseExpandListener != null){
                    collapseExpandListener.onComplete(view);
                }
            }
        });
    }


    private void loadUsers(){


        if (rootView == null ){
            return;
        }


        Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                rootView.findViewById(R.id.refresh_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadUsers();
                    }
                });

                //setBackgroundDrawable(getContext(), getResources().getDrawable( R.drawable.meet_bg2 ));
                rootView.findViewById(R.id.no_matches_layout).setVisibility(View.GONE);
                rootView.findViewById(R.id.fling_view).setVisibility(View.GONE);
                showProgressOverlay(new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Log.d("TAG", "checkin: loadUsers: ");
                        Thread.runOnExecutor(new Runnable() {
                            @Override
                            public void run() {

                                //Thread.sleep(6000);

                                while (requestsSent.get() != 0){
                                    Thread.sleep(500);
                                }
                                Rest.ServerRetVal<User> retVal = Rest.getInstance().get( User.class,"suggestions_in_event/", true);
                                userList = retVal.getValues();
                                //Thread.sleep(6000);

                                Thread.runOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        hideProgressOverlay(new GUI.CollapseExpandListener() {
                                            @Override
                                            public void onComplete(View view) {
                                                Thread.runOnUi(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (userList==null || userList.isEmpty() == true){
                                                            rootView.findViewById(R.id.no_matches_layout).setVisibility(View.VISIBLE);

                                                        }else{
                                                            rootView.findViewById(R.id.fling_view).setVisibility(View.VISIBLE);
                                                        }
                                                    }
                                                });

                                            }
                                        });

                                    }
                                });

                                Thread.runOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        final SwipeFlingAdapterView swipeFlingAdapterView = (SwipeFlingAdapterView)rootView.findViewById(R.id.fling_view);


                                        adapter = new BaseAdapter() {


                                            @Override
                                            public int getCount() {
                                                Log.d("TAG", "getCount: " + userList.size());
                                                return userList.size();
                                            }

                                            @Override
                                            public Object getItem(int position) {
                                                return userList.get(position);
                                            }

                                            @Override
                                            public long getItemId(int position) {
                                                return position;
                                            }



                                            @Override
                                            public View getView(final int position, View convertView, ViewGroup parent) {
                                                final ViewHolder viewHolder;

                                                if(convertView == null){
                                                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.meet_fragment_profile_layout, parent, false);


                                                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)convertView.getLayoutParams();

                                                    int padding = (int)getContext().getResources().getDimension(R.dimen.tab_height) +
                                                            (int)GUI.statusBarHeight;

                                                    marginLayoutParams.setMargins(0, padding ,0,GUI.navKeysHeight);

                                                    convertView.forceLayout();


                                                    viewHolder = new ViewHolder();
                                                    viewHolder.imageView = (CustomImageView) convertView.findViewById(R.id.image_view);
                                                    viewHolder.recyclerView = (RecyclerView)convertView.findViewById(R.id.recycler_view) ;
                                                    viewHolder.textViewBio = convertView.findViewById(R.id.text_view_bio);
                                                    viewHolder.textViewName = convertView.findViewById(R.id.text_view_name_and_age);

                                                    convertView.setTag(viewHolder);
                                                } else {
                                                    viewHolder = (ViewHolder) convertView.getTag();
                                                }



                                                final User user = userList.get(position % userList.size());


                                                viewHolder.imageView.setMaskResId(R.drawable.swipe_card_background_internal_fade);
                                                Picasso.with(getContext()).load(user.getPicList()[0]).placeholder(R.drawable.smiley).into(viewHolder.imageView);

                                                Thread.waitAndRunOnUi(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (position == 0) {
                                                            GUI.loadDrawable(getContext(), user.getPicList()[0], new GUI.DrawableDownloadCallback() {
                                                                @Override
                                                                public void onDone(final Drawable drawable) {
                                                                    setBackgroundDrawable(getContext(), drawable);
                                                                }
                                                            });
                                                        }
                                                    }
                                                },200);

                                                viewHolder.textViewBio.setText(user.getBio());
                                                LocalDate birthdate = user.getBirthDate();
                                                LocalDate now = new LocalDate();
                                                Years age = Years.yearsBetween(birthdate, now);
                                                viewHolder.textViewName.setText(user.getFirstName() + ", " + age.getYears());

                                                viewHolder.recyclerView.setVisibility(View.VISIBLE);
                                                if (user.getPicList().length < 2){
                                                    viewHolder.recyclerView.setVisibility(View.GONE);
                                                }

                                                viewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                                                viewHolder.recyclerView.setAdapter(new RecyclerView.Adapter() {



                                                    class UserPictureHolder extends RecyclerView.ViewHolder{

                                                        public CustomImageView imageView;
                                                        public UserPictureHolder(View itemView) {
                                                            super(itemView);
                                                        }

                                                    }

                                                    @Override
                                                    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {


                                                        return new UserPictureHolder(LayoutInflater.from(getContext()).inflate(R.layout.meet_screen_small_picture,parent, false)){
                                                            {
                                                                imageView = (CustomImageView) itemView.findViewById(R.id.image_view);
                                                                itemView.getLayoutParams().width = viewHolder.recyclerView.getMeasuredWidth()/photosInRecycler;
                                                                itemView.getLayoutParams().height = (int)((viewHolder.recyclerView.getMeasuredWidth()/photosInRecycler) *recyclerImageAspect);



                                                            }
                                                        };

                                                    }




                                                    @Override
                                                    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
                                                        CustomImageView imageView = ((UserPictureHolder)holder).imageView;
                                                        imageView.setMaskResId(R.drawable.swipe_card_background_internal_fade_small_picture);
                                                        Picasso.with(getContext()).load(user.getPicList()[position]).into(imageView);
                                                        imageView.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                Picasso.with(getContext()).load(user.getPicList()[position]).into(viewHolder.imageView);

                                                                GUI.loadDrawable(getContext(), user.getPicList()[position], new GUI.DrawableDownloadCallback() {
                                                                    @Override
                                                                    public void onDone(Drawable drawable) {
                                                                        UIcontroller.getInstance().setBackground(drawable);
                                                                    }
                                                                });

                                                            }
                                                        });



                                                    }

                                                    @Override
                                                    public int getItemCount() {
                                                        return user.getPicList().length;
                                                    }
                                                });

                                                viewHolder.recyclerView.setOnDragListener(new View.OnDragListener() {
                                                    @Override
                                                    public boolean onDrag(View view, DragEvent dragEvent) {
                                                        Log.d("TAG", "onDrag: ");
                                                        return false;
                                                    }
                                                });
                                                OverScrollDecoratorHelper.setUpOverScroll(viewHolder.recyclerView, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);




                                                //parent.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                                                return convertView;
                                            }

                                            class ViewHolder {
                                                public CustomImageView imageView;
                                                public RecyclerView recyclerView;
                                                public TextView textViewName;
                                                public TextView textViewBio;
                                            }





                                        };



                                        swipeFlingAdapterView.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
                                            @Override
                                            public void removeFirstObjectInAdapter() {
                                                Log.d("LIST", "removed object!");
                                                User user = userList.get(0);
                                                userList.remove(0);
                                                //userList.add(userList.size(),user);
                                                adapter.notifyDataSetChanged();
                                                if (userList.isEmpty() == true){
                                                    loadUsers();
                                                }
                                            }

                                            @Override
                                            public void onLeftCardExit(final Object dataObject) {
                                                requestsSent.incrementAndGet();
                                                //UIcontroller.getInstance().showAnimation(R.drawable.big_x, R.color.colorPrimaryDark);
                                                Thread.runOnExecutor(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Rest.getInstance().unlike(((User)dataObject).getId());
                                                        requestsSent.decrementAndGet();

                                                    }
                                                });
                                            }


                                            @Override
                                            public void onRightCardExit(final Object dataObject) {
                                                requestsSent.incrementAndGet();
                                                //UIcontroller.getInstance().showAnimation(R.drawable.big_heart, R.color.colorPrimaryDark);
                                                Thread.runOnExecutor(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Rest.getInstance().like(((User)dataObject).getId());
                                                        requestsSent.decrementAndGet();

                                                    }
                                                });
                                            }

                                            @Override
                                            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                                                adapter.notifyDataSetChanged();
                                                Log.d("LIST", "notified");

                                            }

                                            @Override
                                            public void onScroll(float v) {
                                                Log.d("TAG", "onScroll: " + v);
                                                TextView textView = swipeFlingAdapterView.getSelectedView().findViewById(R.id.text_view_action);
                                                if (v > 0){
                                                    textView.setText("Go");
                                                    textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                                                }else {
                                                    textView.setText("NoGo");
                                                    textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorRed));
                                                }
                                                textView.setAlpha(Math.abs(v));

                                            }
                                        });

                                        swipeFlingAdapterView.setAdapter(new BaseAdapter() {


                                            @Override
                                            public int getCount() {
                                                return 0;
                                            }

                                            @Override
                                            public Object getItem(int position) {
                                                return null;
                                            }

                                            @Override
                                            public long getItemId(int position) {
                                                return 0;
                                            }



                                            @Override
                                            public View getView(final int position, View convertView, ViewGroup parent) {
                                                return null;
                                            }







                                        }
);
                                        adapter.notifyDataSetChanged();
                                        swipeFlingAdapterView.forceLayout();
                                        swipeFlingAdapterView.invalidate();
                                        Thread.waitAndRunOnUi(new Runnable() {
                                            @Override
                                            public void run() {
                                                swipeFlingAdapterView.setAdapter(adapter);
                                                adapter.notifyDataSetChanged();
                                                swipeFlingAdapterView.forceLayout();
                                            }
                                        }, 1500);

                                        adapter.notifyDataSetChanged();
                                        swipeFlingAdapterView.forceLayout();

                                    }
                                });
                            }
                        });
                    }
                });

            }
        },1);


    }


    @Nullable
    @Override
    public final View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View linearLayout = createEmptyLayout(inflater,container);


        if (rootView == null){
            createRootView(R.layout.fragment_meet, getContext(), container);
            //setBackgroundDrawable(getContext(), getResources().getDrawable( R.drawable.hearts_background ));
        }
        if (rootView.getParent() != null){
            ((LinearLayout)rootView.getParent()).removeAllViews();
        }
        ((LinearLayout)linearLayout).addView(rootView);
        //loadUsers();
        return linearLayout;
    }



    @Override
    public void onResume() {
        super.onResume();
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TAG", "onAttach: ");
        //loadUsers();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser == true && rootView != null){
            loadUsers();
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }




}
