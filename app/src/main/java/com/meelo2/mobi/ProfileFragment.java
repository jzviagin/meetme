package com.meelo2.mobi;

/**
 * Created by jzvia on 18/12/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.meelo2.mobi.GUI.UIcontroller;
import com.meelo2.mobi.GUI.UserProfileView;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.activities.BaseActivity;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.api.Var;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.User;

import java.io.File;
import java.util.concurrent.atomic.AtomicReference;


public class ProfileFragment extends BaseNonRootFragment {


    private Runnable initRunnable;

    private MenuItem doneButton;


    {
        toolbarResId = R.id.app_bar_layout;
    }


    @Override
    protected Drawable fetchBackground(){
        final AtomicReference<Drawable> fetched = new AtomicReference<>(null);
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.loadDrawable(context, User.getCurrent().getPicList()[0], new GUI.DrawableDownloadCallback() {
                    @Override
                    public void onDone(Drawable drawable) {
                        fetched.set(drawable);
                    }
                });
            }
        });
        ;

        while (fetched.get() == null){
            Thread.sleep(1);
        }

        return fetched.get();

    }

    public static void showProfile(Context context){

        ProfileFragment profileFragment = new ProfileFragment();
        profileFragment.init(R.layout.profile_fragment,context);
    }


    @Override
    protected void postShowInit() {
        //loadPhotos();
    }

    @Override
    protected void preShowInit() {

        Log.d("TAG", "preShowInit: ");

        ((TextView)getToolbar().findViewById(R.id.title_name)).setText("My Profile");
        ImageView imageView = getToolbar().findViewById(R.id.image_view_title);
        GUI.loadRoundedCornersImage(User.getCurrent().getPicList()[0], imageView, GUI.Quality.HALF_WIDTH_565,imageView.getLayoutParams().width /2,imageView.getLayoutParams().width,imageView.getLayoutParams().height);



    }


    @Override
    public void onResume() {
        super.onResume();
        if (initRunnable != null){
            ((UserProfileView)rootView.findViewById(R.id.user_profile_view)).loadData(initRunnable, new UserProfileView.ProfileCompleteListener() {
                @Override
                public void onProfileCompleteChanged(boolean canProceed) {
                    doneButton.setEnabled(canProceed);
                    if (canProceed == true){
                        doneButton.getIcon().setAlpha(255);
                    }else{
                        doneButton.getIcon().setAlpha(100);

                    }
                }
            });
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (rootView != null && isVisibleToUser == true){
            initRunnable = new Runnable() {
                @Override
                public void run() {
                    boolean facebookGallery = AccessToken.getCurrentAccessToken() != null && AccessToken.getCurrentAccessToken().getPermissions().contains("user_photos");
                    if (facebookGallery == true) {
                        GalleryFragment.showGallery(context);
                    }else{
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        // Do not include the extras related to cropping here;
                        // this Intent is for selecting the image only.
                        ((BaseActivity)getContext()).startActivityForResult(photoPickerIntent, new BaseActivity.ActivityResultHandler() {
                            @Override
                            public void onActivityResult(int resultCode, Intent data) {
                                if (resultCode == Activity.RESULT_OK) {

                                    try {
                                        File tempFile = Var.createTempImageFile(getContext());
                                        Uri destUri = Uri.fromFile(tempFile);
                                        CropFragment.cropImage(context, data.getData(), destUri, true);
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }


                                }
                            }
                        });
                    }
                }
            };
            ((UserProfileView)rootView.findViewById(R.id.user_profile_view)).loadData(initRunnable, new UserProfileView.ProfileCompleteListener() {
                @Override
                public void onProfileCompleteChanged(boolean canProceed) {
                    doneButton.setEnabled(canProceed);
                    if (canProceed == true){
                        doneButton.getIcon().setAlpha(255);
                    }else{
                        doneButton.getIcon().setAlpha(100);

                    }
                }
            });
        }

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_menu_done);
        doneButton = menuItem;

        Drawable favoriteIcon = DrawableCompat.wrap(menuItem.getIcon());
        ColorStateList colorSelector = ResourcesCompat.getColorStateList(context.getResources(), android.R.color.white, context.getTheme());
        DrawableCompat.setTintList(favoriteIcon, colorSelector);
        menuItem.setIcon(favoriteIcon);
        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {


                UIcontroller.getInstance().performLongOperation(new Runnable() {
                    @Override
                    public void run() {
                        ((UserProfileView) rootView.findViewById(R.id.user_profile_view)).saveData();
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {


                            }
                        });
                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                        UIcontroller.getInstance().goBack();
                    }
                });


                return true;
            }
        });

    }
}



