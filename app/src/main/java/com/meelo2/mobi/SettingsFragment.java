package com.meelo2.mobi;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.facebook.AccessToken;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.activities.LoginActivity;
import com.meelo2.mobi.api.Kvs;
import com.meelo2.mobi.api.Var;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.User;

/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * {@link SettingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends BaseFragment {


    //private View rootView ;




    public final View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View linearLayout = createEmptyLayout(inflater,container);


        if (rootView == null){
            //setBackgroundDrawable(getContext(), getResources().getDrawable( R.drawable.settings_bg2 ));
            //rootView = inflater.inflate(R.layout.fragment_settings, container, false);
            createRootView(R.layout.fragment_settings , getContext(),container);
            //setIsVisible();

        }
        if (rootView.getParent() != null){
            ((LinearLayout)rootView.getParent()).removeAllViews();
        }
        ((LinearLayout)linearLayout).addView(rootView);



        rootView.findViewById(R.id.button_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut();
            }
        });

        rootView.findViewById(R.id.button_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileFragment.showProfile(getContext());
            }
        });

        rootView.findViewById(R.id.button_about).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PdfFragment.showPdf(getContext(), "terms_of_service" , "About");
            }
        });

        rootView.findViewById(R.id.button_support).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Var.sendEmail(getContext(),"support@meelo2.mobi",null, "Contact Customer Support", "Hi,\n");
            }
        });

        rootView.findViewById(R.id.button_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Var.shareApp(getContext());
            }
        });

        rootView.findViewById(R.id.button_preferences).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferencesFragment.showPreferences(getContext());
            }
        });
        return linearLayout;
    }


    private void logOut(){
        GUI.showConfirmDialog(getContext(), "Are you sure you want to log out?", new Runnable() {
            @Override
            public void run() {
                Kvs.deleteDatabase();
                AccessToken.setCurrentAccessToken(null);
                User.setCurrent(null);
                Intent intent = new Intent(getContext(), LoginActivity.class);
                ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getContext(), R.anim.enter_from_left, R.anim.exit_out_right);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent, activityOptions.toBundle());
            }
        });


    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
