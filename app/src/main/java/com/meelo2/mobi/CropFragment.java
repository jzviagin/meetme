package com.meelo2.mobi;

/**
 * Created by jzvia on 18/12/2017.
 */

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.meelo2.mobi.GUI.CropView;
import com.meelo2.mobi.GUI.UIcontroller;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.User;
import com.steelkiwi.cropiwa.image.CropIwaResultReceiver;

import java.util.concurrent.atomic.AtomicReference;


public class CropFragment extends BaseNonRootFragment {



    private Uri uri;

    private Uri destUri;

    private boolean backToPtofile;

    {
        toolbarResId = R.id.app_bar_layout;
    }


    @Override
    protected Drawable fetchBackground(){
        final AtomicReference<Drawable> fetched = new AtomicReference<>(null);
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.loadDrawable(context, User.getCurrent().getPicList()[0], new GUI.DrawableDownloadCallback() {
                    @Override
                    public void onDone(Drawable drawable) {
                        fetched.set(drawable);
                    }
                });
            }
        });
        ;

        while (fetched.get() == null){
            Thread.sleep(1);
        }

        return fetched.get();

    }

    public static void cropImage(Context context, Uri uri, Uri destUri, boolean backToProfile){

        CropFragment cropFragment = new CropFragment();
        cropFragment.uri = uri;
        cropFragment.destUri = destUri;
        cropFragment.backToPtofile = backToProfile;
        cropFragment.init(R.layout.crop_fragment,context);

    }


    @Override
    protected void postShowInit() {

    }

    @Override
    protected void preShowInit() {

        Log.d("TAG", "preShowInit: ");

        ((TextView)getToolbar().findViewById(R.id.title_name)).setText("Crop Image");
        CropView cropView = rootView.findViewById(R.id.crop_view);
        cropView.setData(uri,destUri, new CropIwaResultReceiver.Listener() {
            @Override
            public void onCropSuccess(Uri croppedUri) {
                Log.d("TAG", "onCropSuccess: " );
                UIcontroller.getInstance().performLongOperation(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        User.getCurrent().addPhoto("file://"+destUri.toString());

                                                                    }
                                                                },
                        new Runnable() {
                            @Override
                            public void run() {
                                if (backToPtofile == true){
                                    UIcontroller.getInstance().goBack(1);

                                }else {
                                    UIcontroller.getInstance().goBack(2);

                                }
                            }
                        });


            }

            @Override
            public void onCropFailed(Throwable e) {
                Log.d("TAG", "onCropFailed: ");

            }
        });





    }





    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.login_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_menu_done);

        Drawable favoriteIcon = DrawableCompat.wrap(menuItem.getIcon());
        ColorStateList colorSelector = ResourcesCompat.getColorStateList(context.getResources(), android.R.color.white, context.getTheme());
        DrawableCompat.setTintList(favoriteIcon, colorSelector);
        menuItem.setIcon(favoriteIcon);
        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                final CropView cropView = rootView.findViewById(R.id.crop_view);
                cropView.save();
                return true;
            }
        });

    }
}



