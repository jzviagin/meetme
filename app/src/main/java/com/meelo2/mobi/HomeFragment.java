package com.meelo2.mobi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.meelo2.mobi.GUI.UIcontroller;
import com.meelo2.mobi.activities.MainActivity;
import com.meelo2.mobi.Utils.GUI;
//import com.me.meet.meetme.api.MeetMeLocationListener;
import com.meelo2.mobi.Utils.Rest;;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.Entity;
import com.meelo2.mobi.entities.HangoutLocation;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


public class HomeFragment extends BaseFragment {


    //private View rootView;

    private boolean nearbyLayoutVisible = false;
    {
        toolbarResId= R.id.app_bar_layout;

    }

    private String selectedNearbyLocationId = null;

    private BroadcastReceiver receiver;

    private boolean radiusLayoutVisible = true;

   // private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private RecyclerView recyclerView;
    private RecyclerView recyclerViewNearby;
    private double radius;
    private Location location;


    private void checkIn(){
        /*Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                Rest.checkIn(getContext(), selectedNearbyLocationId);

            }
        });
        toggleNearbyLayout();*/

        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                Rest.getInstance().checkIn(getContext(), selectedNearbyLocationId);
            }


        });

        //UIcontroller.getInstance().checkIn();
        //toggleNearbyLayout();

    }



    private void updateRadius(int progress){
        radius = 1f + 99f * Math.pow(((((float)progress))/ 100f), 2f);
        ((TextView)rootView.findViewById(R.id.text_view_distance_value)).setText(Math.round(radius) + "KM");
    }
    protected void preShowInit(){

    }

    protected  void postShowInit(){

    }


    public void refresh(){

    }

    @Nullable
    @Override
    public final View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View linearLayout = createEmptyLayout(inflater,container);
        Log.d("TAG", "onCreateView: home" );

        if (rootView == null){
            Log.d("TAG", "onCreateView: home first time" );
            //setBackgroundDrawable(getContext(), getResources().getDrawable( R.drawable.home_bg2 ));
            //rootView = inflater.inflate(R.layout.fragment_home, container, false);
            ///rootView.setPadding(rootView.getPaddingLeft(), rootView.getPaddingTop() + GUI.statusBarHeight +  (int)getContext().getResources().getDimension(R.dimen.tab_height) +
               //     (int)getContext().getResources().getDimension(R.dimen.toolbar_height)    , rootView.getPaddingRight(), rootView.getPaddingBottom());

            createRootView(R.layout.fragment_home, getContext(), container);

            //setIsVisible();
            recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);


            //recyclerView.setPadding(0,0,0,GUI.navKeysHeight);
            //((CustomLinearLayout)rootView.findViewById(R.id.recycler_wrapper)).setWillNotDraw(false);
           //((CustomLinearLayout)rootView.findViewById(R.id.recycler_wrapper)).setMaskResId(R.drawable.background_gradient);
            recyclerViewNearby = (RecyclerView) rootView.findViewById(R.id.recycler_view_nearby);
            //rootView.findViewById(R.id.check_in_layout).setVisibility(View.GONE);
            ((EditText)getToolbar().findViewById(R.id.edit_text_search)).setTypeface(ResourcesCompat.getFont(getContext(), R.font.custom2), Typeface.BOLD);


            ((EditText)getToolbar().findViewById(R.id.edit_text_search)).addTextChangedListener(new TextWatcher() {
                Timer timer;
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (timer != null){
                        timer.cancel();
                    }
                    final String name = ((EditText)getToolbar().findViewById(R.id.edit_text_search)).getText().toString();
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            Thread.runOnUi(new Runnable() {
                                @Override
                                public void run() {
                                    reloadAdapter((float)radius, false, name);

                                }
                            });
                        }
                    },1000);
                }
            });


            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerViewNearby.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));


            ((AppCompatSeekBar)rootView.findViewById(R.id.seek_bar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    Log.d("TAG", "onProgressChanged: " + progress);

                    updateRadius(progress);


                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    GUI.scaleTo(((AppCompatSeekBar)rootView.findViewById(R.id.seek_bar)), 1.03f, MainActivity.fastAnimationDuration, null);

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    GUI.scaleTo(((AppCompatSeekBar)rootView.findViewById(R.id.seek_bar)), 1.0f, MainActivity.fastAnimationDuration, null);

                    reloadAdapter((float) radius, false,null);
                }
            });


            ((AppCompatSeekBar)rootView.findViewById(R.id.seek_bar)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    Log.d("TAG", "onFocusChange: ");
                    if (hasFocus == true){
                        GUI.scaleTo(((AppCompatSeekBar)rootView.findViewById(R.id.seek_bar)), 1.2f, MainActivity.defaultAnimationDuration, null);
                    }else{
                        GUI.scaleTo(((AppCompatSeekBar)rootView.findViewById(R.id.seek_bar)), 1.0f, MainActivity.defaultAnimationDuration, null);

                    }
                }
            });
            ((AppCompatSeekBar)rootView.findViewById(R.id.seek_bar)).setProgress(50);
            updateRadius( 50);
            //requestPermissions();
            registerReceiver();





        }
        if (rootView.getParent() != null){
            ((LinearLayout)rootView.getParent()).removeAllViews();
        }
        ((LinearLayout)linearLayout).addView(rootView);
        return linearLayout;
    }



    public void hideNearbyLayout(boolean animate){
        if (rootView != null) {
            if (animate == true) {
                GUI.collapseView(rootView.findViewById(R.id.nearby_layout), MainActivity.defaultAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        nearbyLayoutVisible = false;

                    }
                });
            }else{
                rootView.findViewById(R.id.nearby_layout).setVisibility(View.GONE);
                nearbyLayoutVisible = false;
            }
        }

    }


    public void showNearbyLayout(){
        Log.d("TAG", "showNearbyLayout: " + this);
        if (rootView != null) {
            GUI.expandView(rootView.findViewById(R.id.nearby_layout), MainActivity.defaultAnimationDuration, new GUI.CollapseExpandListener() {
                @Override
                public void onComplete(View view) {
                    nearbyLayoutVisible = true;
                }
            });
        }
    }


    private void hideRadiusLayout(){
        if (radiusLayoutVisible == true) {
            radiusLayoutVisible = false;
            GUI.fadeOutView(rootView.findViewById(R.id.radius_layout), 0, MainActivity.defaultAnimationDuration, new GUI.CollapseExpandListener() {
                @Override
                public void onComplete(View view) {

                }
            });
        }

    }


    private void showRadiusLayout(){
        if (radiusLayoutVisible == false) {
            radiusLayoutVisible = true;
            GUI.fadeInView(rootView.findViewById(R.id.radius_layout), 0, MainActivity.defaultAnimationDuration, new GUI.CollapseExpandListener() {
                @Override
                public void onComplete(View view) {

                }
            });
        }

    }


  /*  private void toggleNearbyLayout(){
        if (nearbyLayoutVisible == true){
            hideNearbyLayout();

        }else{
            showNearbyLayout();

        }
    }*/
    private AtomicBoolean isLoading = new AtomicBoolean(false);

    private  void reloadAdapter(final float radius , final boolean locationChanged, final String name){
        Log.d("toolbar", "toolbar: loading adapter");
        if (isLoading.get() == true){
            return;
        }
        isLoading.set(true);

        //recyclerView.setVisibility(View.GONE);

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.fadeOutView(rootView.findViewById(R.id.waiting_for_location_layout), 0, MainActivity.defaultAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                rootView.findViewById(R.id.waiting_for_location_layout).setVisibility(View.GONE);
                            }
                        });
                    }
                });
            }
        });

        location = MainActivity.getInstance().getLocation();
        if (location == null){
             //initAdapter(radius, locationChanged, name);
             return;
        }
        final LatLng latLng = new LatLng(location.getLatitude(),
                location.getLongitude());

        rootView.findViewById(R.id.no_locations_layout).setVisibility(View.GONE);
        GeneralAdapter.DataLoadedCallback dataLoadedCallback =   new GeneralAdapter.DataLoadedCallback() {
            @Override
            public void onLoaded(List entities) {
                if (entities.size() == 0){
                    rootView.findViewById(R.id.no_locations_layout).setVisibility(View.VISIBLE);
                    //rootView.findViewById(R.id.nearby_layout).setVisibility(View.VISIBLE);

                    rootView.findViewById(R.id.refresh_button_home).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reloadAdapter(radius, locationChanged, name);
                        }
                    });
                }else{
                    rootView.findViewById(R.id.no_locations_layout).setVisibility(View.GONE);
                    //rootView.findViewById(R.id.nearby_layout).setVisibility(View.VISIBLE);

                }

                if (locationChanged == true) {

                    rootView.findViewById(R.id.nearby_layout).setVisibility(View.GONE);
                    recyclerViewNearby.setAdapter(new NearbyLocationsAdapter(getContext(), null, new GeneralAdapter.DataLoadedCallback() {
                        GeneralAdapter.DataLoadedCallback dataLoadedCallback1 = this;
                        @Override
                        public void onLoaded(List entities) {
                            if (entities.size() == 0){
                                hideNearbyLayout(true);
                            }else{
                                if (UIcontroller.getInstance().isCheckedIn() == false) {
                                    Log.d("TAG", "showNearbyLayout: dataLoadedCallback1 " + System.identityHashCode(dataLoadedCallback1));
                                    showNearbyLayout();
                                }
                            }
                        }
                    },latLng));
                    OverScrollDecoratorHelper.setUpOverScroll(recyclerViewNearby, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);
                }

                isLoading.set(false);
            }
        };
        if (locationChanged == true) {
            rootView.findViewById(R.id.nearby_layout).setVisibility(View.GONE);
        }
        /*Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.expandView(recyclerView,MainActivity.extraSlowAnimationDuration, null);

            }
        },600);*/
        recyclerView.setItemAnimator(null);
        if (name == null || name.trim().toString().length() == 0) {
            Thread.runOnUi(new Runnable() {
                @Override
                public void run() {
                    showRadiusLayout();
                }
            });

            recyclerView.setAdapter(new LocationsAdapter(getContext(), new GeneralAdapter.ItemClickedCallback() {
                @Override
                public void onClick(Entity entity, final int position) {
                    LocationDetailsFragment.showLocationDetails(getContext(), ((HangoutLocation) entity));
                }
            },dataLoadedCallback,  latLng, radius));
        }else{
            Thread.runOnUi(new Runnable() {
                @Override
                public void run() {
                    hideRadiusLayout();
                }
            });

            recyclerView.setAdapter(new LocationsAdapter(getContext(), new GeneralAdapter.ItemClickedCallback() {
                @Override
                public void onClick(Entity entity, final int position) {
                    LocationDetailsFragment.showLocationDetails(getContext(), ((HangoutLocation) entity));
                }
            }, dataLoadedCallback
                  , latLng, radius, name));
        }

        //OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

       /* rootView.findViewById(R.id.button_check_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkIn();
            }
        });*/


    }


    //private  void initAdapter(final float radius , final boolean locationChanged, final String name){
       /* if (ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }*/

   /*     final AtomicBoolean gotLocation = new AtomicBoolean();
        gotLocation.set(false);

        UIcontroller.getInstance().performLongOperation(new Runnable() {
            @Override
            public void run() {
                while (gotLocation.get() == false){
                    Thread.sleep(50);
                }
            }
        }, new Runnable() {
            @Override
            public void run() {

            }
        });*/

      /*  MeetMeLocationListener.getInstance(getActivity()).getLocation(new MeetMeLocationListener.LocationCallback() {
            @Override
            public void onLocationReceive(final Location location) {
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        HomeFragment.this.location = location;
                        gotLocation.set(true);
                        reloadAdapter(radius,locationChanged, name);

                    }
                });



            }
        }, new Runnable() {
            @Override
            public void run() {

                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        gotLocation.set(true);
                        MainActivity.getInstance().showAlert("Location Not Detected. Make sure location is turned on.", new Runnable() {
                            @Override
                            public void run() {

                            }
                        });
                    }
                });

            }
        });*/


   // }

    @Override
    public void onResume() {
        super.onResume();
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    private void registerReceiver(){
        if (receiver == null && getContext() != null && rootView != null) {
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(final Context context, final Intent intent) {
                    reloadAdapter((float) radius, true, null);

                }
            };
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(MainActivity.location_changed_broadcast_internal);
            getContext().registerReceiver(receiver,
                    intentFilter);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser == true){
            if (rootView != null){
                if (location != null) {
                    reloadAdapter((float) radius, true, null);
                }
            }
            getToolbar().findViewById(R.id.edit_text_search).clearFocus();

            if (UIcontroller.getInstance().isCheckedIn()){
                hideNearbyLayout(false);
            }else{
                //showNearbyLayout();
            }
        }
    }
    @Override
    public void refreshContent(){

        if (UIcontroller.getInstance().isCheckedIn()){
            hideNearbyLayout(true);
        }else{
            showNearbyLayout();
        }

    }


}
