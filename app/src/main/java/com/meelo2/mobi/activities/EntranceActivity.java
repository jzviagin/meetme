package com.meelo2.mobi.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.meelo2.mobi.app.R;

public class EntranceActivity extends BaseActivity {


    private static EntranceActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entrance_activity);

        instance = this;

        findViewById(R.id.button_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);

                Intent intent = new Intent(EntranceActivity.this, LoginActivity.class);
                startActivity(intent, activityOptions.toBundle());

            }
        });




    }

    public static void close(){
        if (instance != null){
            instance.finish();
        }
    }




}
