package com.meelo2.mobi.activities;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.RemoteMessage;
import com.meelo2.mobi.MessagesFragment;
import com.meelo2.mobi.GUI.UIcontroller;
import com.meelo2.mobi.api.FacebookApi;
import com.meelo2.mobi.api.Kvs;
import com.meelo2.mobi.api.MyMessagingService;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.api.MeetMeLocationService;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.Utils.Rest;;
//import com.me.meet.meetme.api.MeetMeLocationListener;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.entities.Match;
import com.meelo2.mobi.entities.User;

import io.fabric.sdk.android.Fabric;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Queue;

//import eightbitlab.com.blurview.BlurView;
//import eightbitlab.com.blurview.RenderScriptBlur;

public class MainActivity extends BaseActivity {

    public static final String meet_broadcast = "com.meet.me.meetme.MATCH_RECEIVED";
    public static final String message_broadcast = "com.meet.me.meetme.MESSAGE_RECEIVED";
    public static final String admin_message_broadcast = "com.meet.me.meetme.ADMIN_MESSAGE_RECEIVED";
    public static final String checked_in_broadcast = "com.meet.me.meetme.CHECKED_IN";
    public static final String checked_out_broadcast = "com.meet.me.meetme.CHECKED_OUT";
    public static final String checked_in_by_server_broadcast = "com.meet.me.meetme.CHECKED_IN_BY_SERVER";

    public static final String checked_out_by_server_broadcast = "com.meet.me.meetme.CHECKED_OUT_BY_SERVER";
    public static final String message_read_broadcast = "com.meet.me.meetme.MESSAGE_READ";
   // public static final String location_granted_broadcast = "com.meet.me.meetme.LOCATION_GRANTED";
    public static final String location_changed_broadcast = "com.meet.me.meetme.LOCATION_CHANGED";
    public static final String location_changed_broadcast_internal = "com.meet.me.meetme.LOCATION_CHANGED_INTERNAL";
    public static final String location_disabled_broadcast = "com.meet.me.meetme.LOCATION_DISABLED";
    public static final String not_received_location_broadcast = "com.meet.me.meetme.NOT_RECEIVED_LOCATION";

    private boolean isCheckedIn = false;



    private MeetMeLocationService.LocationServiceBinder locationServiceBinder;
    private ServiceConnection serviceConnection;
    private boolean loaded = false;
    private boolean resumed = false;
    private int messageCount = 2;
    private Location location;
    private Queue<Runnable> resumeTasks = new ArrayDeque<>(10);
    private static MainActivity instance;
    private boolean matchDialogShown = false;
    private TabLayout tabLayout;
    private static long lastBackPressed;
    public final static int extraSlowAnimationDuration = 800;
    public final static int slowAnimationDuration = 450;
    public final static int defaultAnimationDuration = 250;
    public final static int fastAnimationDuration = 150;
    public final static int ultraFastAnimationDuration = 100;
    public final static int shakeAnimationDuration = 30;


    public class ControlProxy{
        private ControlProxy(){

        }

        public AppCompatActivity getActivity(){
            return MainActivity.this;
        }


        public void switchTab(final int tab){
            MainActivity.this.switchTab(tab);
        }


        public void performWhenActive(Runnable r){
            MainActivity.this.performWhenActive(r);
        }
        public void alignUI(boolean keyboardShown, boolean fullscreenFragment, boolean fragmentHasToolbar){
            MainActivity.this.alignUI(keyboardShown, fullscreenFragment, fragmentHasToolbar);
        }

        public void clearCustomToolbar(GUI.CollapseExpandListener collapseExpandListener){
            MainActivity.this.clearCustomToolbar(collapseExpandListener);
        }

        public void addCustomToolbar(View toolbar, boolean setAsActionBar, final GUI.CollapseExpandListener collapseExpandListener){
            MainActivity.this.addCustomToolbar(toolbar, setAsActionBar, collapseExpandListener);
        }

        public void checkIn(/*Runnable runnable*/){
            MainActivity.this.checkIn(/*runnable*/);
        }
        public void checkOut(Runnable runnable){
            MainActivity.this.checkOut(runnable);
        }

        public void preRenderView(final View view, final UIcontroller.PreRenderCallback preRenderCallback){
            MainActivity.this.preRenderView(view, preRenderCallback);
        }

        public void showAnimation(int imageResId, int colorResId){
            MainActivity.this.showAnimation(imageResId, colorResId);
        }

        public void setBackground(Drawable drawable) {


            MainActivity.this.setBackground(drawable);
        }

        public void showProgressOverlay(GUI.CollapseExpandListener collapseExpandListener){
            MainActivity.this.showProgressOverlay(collapseExpandListener);
        }

        public void hideProgressOverlay(GUI.CollapseExpandListener collapseExpandListener){
            MainActivity.this.hideProgressOverlay(collapseExpandListener);
        }



    }

    private void setBackground(final Drawable drawable){



        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                Drawable drawableCopy = drawable.mutate();
                Bitmap bitmap = GUI.drawableToBitmap(MainActivity.this ,drawableCopy, ((ImageView)findViewById(R.id.background_image)).getMeasuredWidth(),
                        ((ImageView)findViewById(R.id.background_image)).getMeasuredHeight());

                Bitmap bitmap1 = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 2, bitmap.getHeight() / 2, true);

                final Bitmap blurredBitmap = GUI.blurBitmap(MainActivity.this, bitmap1);




                Log.d("TAG", "run: setBackground " + bitmap1.getWidth() + " h " + bitmap1.getHeight());
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        final ImageView imageView = ((ImageView)findViewById(R.id.background_image));
                        final ImageView imageView2 = ((ImageView)findViewById(R.id.background_image2));
                        imageView2.setAlpha(1f);
                        ColorMatrix matrix = new ColorMatrix();
                        matrix.setSaturation(0);
                        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);

                        imageView.setColorFilter(filter);
                        imageView2.setColorFilter(filter);
                        imageView2.setVisibility(View.VISIBLE);
                        //imageView2.setImageDrawable(drawable);
                        imageView.setAlpha(0f);

                        imageView.setImageBitmap(blurredBitmap);
                        GUI.fadeOutView(imageView2, 0 , slowAnimationDuration, null);
                        GUI.fadeInView(imageView, 0, slowAnimationDuration, new GUI.CollapseExpandListener() {
                            @Override
                            public void onComplete(View view) {
                                Thread.runOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        //imageView2.setVisibility(View.INVISIBLE);
                                        imageView2.setImageBitmap(blurredBitmap);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });




    }





    public static MainActivity getInstance(){
        return instance;
    }





    private void alignUI(boolean keyboardShown, boolean fullscreenFragment, boolean fragmentHasToolbar){


        int bottomPadding = 0;
        int tabPadding = 0;
        float topPadding = getResources().getDimension(R.dimen.tab_height);

        LinearLayout footerLayout = findViewById(R.id.footer_layout);
        footerLayout.setVisibility(View.GONE);
        if (fragmentHasToolbar == true){
            topPadding += getResources().getDimension(R.dimen.toolbar_height);
        }
        if (keyboardShown == false) {
            //topPadding += GUI.statusBarHeight;

            topPadding += GUI.statusBarHeight;
            tabPadding += GUI.statusBarHeight;
            bottomPadding = GUI.navKeysHeight;
        }

        if (fullscreenFragment == true){
            findViewById(R.id.container).setPadding(0,0,0,0);
            findViewById(R.id.tab_container).setPadding(0, GUI.statusBarHeight,0,0);
        }else{

            if (bottomPadding != 0 ){
                footerLayout.setVisibility(View.VISIBLE);
                footerLayout.getLayoutParams().height = bottomPadding;
            }
            findViewById(R.id.container).setPadding(0,(int) topPadding,0,bottomPadding);
            findViewById(R.id.tab_container).setPadding(0, tabPadding,0,0);
        }




    }





    @Override
    protected void onSaveInstanceState(Bundle outState) {


        super.onSaveInstanceState(outState);
        if (outState != null){
            outState.clear();
        }
    }




    private void preRenderView(final View view, final UIcontroller.PreRenderCallback preRenderCallback){
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {

                ViewTreeObserver vto = ((ViewGroup)findViewById(R.id.container)).getViewTreeObserver();
                vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        ((ViewGroup)findViewById(R.id.container)).getViewTreeObserver().removeGlobalOnLayoutListener(this);

                        ((ViewGroup)findViewById(R.id.container)).removeView(view);
                        preRenderCallback.onRenderDone();
                    }
                });
                ((ViewGroup)findViewById(R.id.container)).addView(view);
            }
        });
    }


    private void clearCustomToolbar(final GUI.CollapseExpandListener collapseExpandListener){
        final ViewGroup linearLayout = findViewById(R.id.tab_container);
        if (linearLayout.getChildCount() == 1){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                linearLayout.setElevation(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics()));
                if (collapseExpandListener!= null){
                    collapseExpandListener.onComplete(linearLayout);

                }
            }
            return;
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                linearLayout.setElevation(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
            }
            final View prevToolbar = linearLayout.getChildAt(1);
            GUI.collapseView(prevToolbar, defaultAnimationDuration, new GUI.CollapseExpandListener() {
                @Override
                public void onComplete(View view) {
                    Thread.runOnUi(new Runnable() {
                        @Override
                        public void run() {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                linearLayout.setElevation(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics()));
                            }
                            linearLayout.removeView(prevToolbar);
                            if (collapseExpandListener != null) {
                                collapseExpandListener.onComplete(linearLayout);
                            }
                        }
                    });
                }
            });
        }
    }

    private void addCustomToolbar(final View toolbar, boolean setAsActionBar, final GUI.CollapseExpandListener collapseExpandListener){
        if (toolbar == null){
            Thread.runOnExecutor(new Runnable() {
                @Override
                public void run() {
                    collapseExpandListener.onComplete(toolbar);
                }
            });
            return;
        }
        final ViewGroup linearLayout = findViewById(R.id.tab_container);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            linearLayout.setElevation(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
        }

        linearLayout.addView(toolbar,linearLayout.getChildCount());

        GUI.expandView(toolbar, defaultAnimationDuration, new GUI.CollapseExpandListener() {

            @Override
            public void onComplete(final View view) {


            Thread.runOnUi(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        linearLayout.setElevation(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics()));
                    }

                }
            });
            collapseExpandListener.onComplete(view);

            }


        });


        Log.d("TAG", "addCustomToolbar: " + toolbar);

        View toolbar1 = toolbar.findViewById(R.id.toolbar);

        if (toolbar1 != null && toolbar1 instanceof android.support.v7.widget.Toolbar && setAsActionBar == true){
            Log.d("TAG", "addCustomToolbar: " + "is real toolbar");
            setSupportActionBar((android.support.v7.widget.Toolbar) toolbar1);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.arrow_back_menu).mutate();
            upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            upArrow.setAlpha(255);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }




    }






    private void selectTabUI(int tab){
        for (int i = 0 ; i < 4 ; i++) {
            ImageView imageView = ((ImageView) tabLayout.getTabAt(i).getCustomView().findViewById(R.id.icon));

            imageView.setColorFilter(new
                PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP));
            imageView.setAlpha(0.5f);
        }

        ImageView imageView = ((ImageView) tabLayout.getTabAt(tab).getCustomView().findViewById(R.id.icon));

        imageView.setColorFilter(new
                PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP));
        imageView.setAlpha(0.9f);


        Log.d("TAG", "onTabSelected: " + tab);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        instance = this;
        Rest.init(this);
        setContentView(R.layout.activity_main);

        Locale locale = Locale.ENGLISH;
        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(locale);
            getApplicationContext().createConfigurationContext(configuration);
        }
        else{
            configuration.locale=locale;
            resources.updateConfiguration(configuration,displayMetrics);
        }


        FirebaseCrash.log("Activity created");


        final View view = findViewById(R.id.activity_main);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int[] loc = new int[2];
                view.getLocationOnScreen(loc);
                GUI.statusBarHeight = loc[1];

                int displayHeight = 0;

                Point size = new Point();


                getWindowManager().getDefaultDisplay().getRealSize(size);
                displayHeight = size.y;
                GUI.navKeysHeight = displayHeight - view.getMeasuredHeight() - loc[1];
                System.out.println("dimensions: status bar: " + GUI.statusBarHeight + " navBar: " + GUI.navKeysHeight +
                        " display height: " + displayHeight);
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);



                tabLayout = ((TabLayout) findViewById(R.id.main_tab_layout));

                for (int i = 0 ; i < 4 ; i++){
                    tabLayout.addTab(tabLayout.newTab());
                }
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);



                tabLayout.getTabAt(0).setText("Home");
                tabLayout.getTabAt(1).setText("Meet");
                tabLayout.getTabAt(2).setText("Messages");
                tabLayout.getTabAt(3).setText("Settings");




                findViewById(R.id.button_checkout).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Thread.runOnExecutor(new Runnable() {
                            @Override
                            public void run() {
                                Rest.getInstance().checkOut(MainActivity.this);
                            }
                        });
                    }
                });

                findViewById(R.id.button_checkout).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Log.d("TAG", "onTouch: ");
                        if (event.getAction() == MotionEvent.ACTION_DOWN){
                            GUI.scaleTo(v,1.1f, MainActivity.fastAnimationDuration, null);
                        }
                        else if (event.getAction() == MotionEvent.ACTION_UP){
                            GUI.scaleTo(v,1.00f, MainActivity.fastAnimationDuration, null);
                        }
                        return false;
                    }
                });




                setupTabs();

                UIcontroller.init(new ControlProxy());

                if (AccessToken.getCurrentAccessToken()  != null){

                    FacebookApi.permissionsRevoked(AccessToken.getCurrentAccessToken().getPermissions(), new Runnable() {
                        @Override
                        public void run() {
                            ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);

                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent, activityOptions.toBundle());
                        }
                    }, new Runnable() {
                        @Override
                        public void run() {
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    String passcode = (String)Kvs.get("passcode");
                                    User user = null;
                                    try {
                                        user = Rest.getInstance().loginWithToken(passcode);
                                    }catch (Rest.PasscodeException e){
                                        FirebaseCrash.logcat(Log.ERROR, "TAG", "NPE caught");
                                        FirebaseCrash.report(e);

                                    }
                                    if (user != null){
                                        Thread.runOnUi(new Runnable() {
                                            @Override
                                            public void run() {
                                                init();
                                            }
                                        });
                                    }
                                    else {
                                        ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);

                                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent, activityOptions.toBundle());
                                    }
                                }
                            });
                        }
                    });



                }else{
                    Thread.runOnExecutor(new Runnable() {
                        @Override
                        public void run() {
                            String passcode = (String)Kvs.get("passcode");
                            User user = null;
                            try {
                                user = Rest.getInstance().loginWithToken(passcode);
                            }catch (Rest.PasscodeException e){
                                FirebaseCrash.logcat(Log.ERROR, "TAG", "NPE caught");
                                FirebaseCrash.report(e);

                            }
                            if (user != null) {
                                Thread.runOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        init();
                                    }
                                });
                            }else{
                                ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);

                                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                startActivity(intent, activityOptions.toBundle());
                            }
                        }
                    });

                }


            }
        });

    }

    public Location getLocation(){
        return location;
    }


    public void init(){
        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 location = new Location("");
                                 Log.d("TAG", "onReceive: lng" + intent.getDoubleExtra("lng",0f));
                                 location.setLongitude(intent.getDoubleExtra("lng",0f));
                                 location.setLatitude(intent.getDoubleExtra("lat", 0f));
                                 Intent broadcastIntent = new Intent();
                                 broadcastIntent.setAction(location_changed_broadcast_internal);
                                 sendBroadcast(broadcastIntent);


                             }
                         },
                new IntentFilter(location_changed_broadcast));

        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                Thread.runOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        GUI.showAlert(MainActivity.this, "Location is disabled, please enable location in settings and run the app again",
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        finish();
                                                    }
                                                });
                                    }
                                });


                             }
                         },
                new IntentFilter(location_disabled_broadcast));

        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 Thread.runOnUi(new Runnable() {
                                     @Override
                                     public void run() {
                                         GUI.showAlert(MainActivity.this, "Could not determine your location, please try later.",
                                                 new Runnable() {
                                                     @Override
                                                     public void run() {
                                                         finish();
                                                     }
                                                 });
                                     }
                                 });


                             }
                         },
                new IntentFilter(not_received_location_broadcast));

        requestPermissions();
        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 Thread.runOnUi(new Runnable() {
                                     @Override
                                     public void run() {
                                         NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                         notificationManager.cancelAll();

                                         showMatchDialog(intent.getStringExtra("id"));
                                     }
                                 });

                             }
                         },
                new IntentFilter(meet_broadcast));

        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 Thread.waitAndRunOnUi(new Runnable() {
                                     @Override
                                     public void run() {
                                         NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                         if (resumed == true) {
                                             notificationManager.cancel(intent.getIntExtra("id", 0));
                                         }
                                     }
                                 }, 500);

                                 Thread.runOnUi(new Runnable() {
                                     @Override
                                     public void run() {
                                       setMessageCount();
                                     }
                                 });

                             }
                         },
                new IntentFilter(message_broadcast));

        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 Thread.waitAndRunOnUi(new Runnable() {
                                     @Override
                                     public void run() {
                                         NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                         if (resumed == true) {
                                             notificationManager.cancel(intent.getIntExtra("id", 0));
                                         }
                                     }
                                 }, 500);

                                 Thread.runOnUi(new Runnable() {
                                     @Override
                                     public void run() {
                                         setMessageCount();
                                     }
                                 });

                             }
                         },
                new IntentFilter(admin_message_broadcast));
        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 Thread.waitAndRunOnUi(new Runnable() {
                                     @Override
                                     public void run() {
                                         NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                         if (resumed == true) {
                                             notificationManager.cancel(intent.getIntExtra("id", 0));
                                         }
                                     }
                                 }, 500);

                                 GUI.showAlert(MainActivity.this, intent.getStringExtra("text"), new Runnable() {
                                     @Override
                                     public void run() {
                                         Intent broadcastIntent = new Intent();
                                         broadcastIntent.setAction(MainActivity.checked_out_broadcast);
                                         context.sendBroadcast(broadcastIntent);
                                     }
                                 });



                             }
                         },
                new IntentFilter(checked_out_by_server_broadcast));

        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 Thread.waitAndRunOnUi(new Runnable() {
                                     @Override
                                     public void run() {
                                         NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                         notificationManager.cancel(intent.getIntExtra("id", 0));
                                        if (resumed == false) {
                                            Intent intent2 = new Intent(MainActivity.this, MainActivity.class);


                                            intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 5 /* Request code */, intent2, PendingIntent.FLAG_ONE_SHOT);


                                            RemoteMessage remoteMessage = intent.getParcelableExtra("remoteMessage");
                                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(MainActivity.this, null)
                                                    .setSmallIcon(R.drawable.icon_notification)
                                                    .setContentText( intent.getStringExtra("title"))
                                                    .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getData().get("text")))
                                                    .setAutoCancel(true)
                                                    // .setSound(soundUri)
                                                    .setContentIntent(pendingIntent);
                                            int notificationId = (int)(2000 + new Date().getTime()% 1000000);
                                            MyMessagingService.sendNotification(notificationManager, notificationBuilder, notificationId);

                                        }
                                     }
                                 }, 500);
                                final String locationId = intent.getStringExtra("locationId");
                                 GUI.showAlert(MainActivity.this, intent.getStringExtra("text"), new Runnable() {
                                     @Override
                                     public void run() {
                                        Thread.runOnExecutor(new Runnable() {
                                            @Override
                                            public void run() {
                                                Rest.getInstance().checkIn(MainActivity.this, locationId);
                                            }
                                        });
                                     }
                                 });



                             }
                         },
                new IntentFilter(checked_in_by_server_broadcast));




        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 Thread.runOnUi(new Runnable() {
                                     @Override
                                     public void run() {
                                         setMessageCount();
                                     }
                                 });

                             }
                         },
                new IntentFilter(message_read_broadcast));

        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 isCheckedIn = true;
                                 UIcontroller.getInstance().checkIn();
                                 loadComplete();

                             }
                         },
                new IntentFilter(checked_in_broadcast));


        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(final Context context, final Intent intent) {
                                 isCheckedIn = false;

                                 UIcontroller.getInstance().checkOut();
                                 loadComplete();
                                 Intent intent1 = new Intent(MainActivity.this, MeetMeLocationService.class);

                                 stopService(intent1);



                             }
                         },
                new IntentFilter(checked_out_broadcast));



        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                Rest.getInstance().getCheckedInLocation();
            }
        });
        selectTabUI(0);
        setMessageCount();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }


    private void handleIntent(final Intent intent){
        String notificationType = intent.getStringExtra("notificationType");
        if (notificationType == null){
            return;
        }
        if (notificationType.equalsIgnoreCase(message_broadcast)){
            Thread.runOnExecutor(new Runnable() {
                @Override
                public void run() {
                    try {
                        Rest.getInstance().convertAll(Match.class,new JSONObject(intent.getStringExtra("data")));
                        final String matchId = intent.getStringExtra("matchId");
                        Thread.waitAndRunOnUi(new Runnable() {
                            @Override
                            public void run() {
                                UIcontroller.getInstance().switchTab(2);
                                Thread.waitAndRunOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        MessagesFragment.showMessages(MainActivity.this,matchId);
                                    }
                                },1000);
                            }
                        },1000);
                    }catch(JSONException e){
                        e.printStackTrace();
                    }

                }
            });

        }

        if (notificationType.equalsIgnoreCase(admin_message_broadcast)){
            Thread.runOnExecutor(new Runnable() {
                @Override
                public void run() {
                        Thread.waitAndRunOnUi(new Runnable() {
                            @Override
                            public void run() {
                                UIcontroller.getInstance().switchTab(2);
                                Thread.waitAndRunOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        MessagesFragment.showAdminMessages(MainActivity.this);
                                    }
                                },1000);
                            }
                        },1000);


                }
            });

        }

        if (notificationType.equalsIgnoreCase(checked_in_by_server_broadcast)){
            final String locationId = intent.getStringExtra("locationId");

            GUI.showAlert(MainActivity.this, intent.getStringExtra("text"), new Runnable() {
                @Override
                public void run() {
                    Thread.runOnExecutor(new Runnable() {
                        @Override
                        public void run() {
                            Rest.getInstance().checkIn(MainActivity.this, locationId);
                        }
                    });
                }
            });
        }
    }

    private void loadComplete(){
        if (loaded == true){
            return;
        }

        handleIntent(getIntent());

        Log.d("TAG", "loadComplete: ");
        loaded = true;
        Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.fadeOutView(findViewById(R.id.splash_layout), 0, defaultAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(R.id.splash_layout).setVisibility(View.GONE);

                            }
                        });
                    }
                });
            }
        },1500);
    }


    private void showAnimation(int imageResId, int colorResId){
        final ImageView imageView = findViewById(R.id.image_view_animation);
        Drawable drawable = getResources().getDrawable(imageResId);
        drawable.setColorFilter(new
                PorterDuffColorFilter(ContextCompat.getColor(this,colorResId), PorterDuff.Mode.SRC_ATOP));
        imageView.setImageDrawable(drawable);

        final RelativeLayout relativeLayout = findViewById(R.id.image_view_animation_wrapper);
        GUI.fadeInView(relativeLayout, 0, ultraFastAnimationDuration * 15 / 100, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {
                GUI.fadeOutView(relativeLayout, 0, ultraFastAnimationDuration *185/100, null);

            }
        });
        GUI.expandView(relativeLayout, defaultAnimationDuration, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {
                //GUI.fadeOutView(relativeLayout,0.0f, defaultAnimationDuration, null);
                GUI.collapseView(relativeLayout, defaultAnimationDuration, null);
            }
        });


    }



    private void switchTab(final int tab){
        Log.d("selectTab", "selectTab: " + tab);
        performWhenActive(new Runnable() {
            @Override
            public void run() {
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        tabLayout.getTabAt(tab).select();
                        selectTabUI(tab);

                    }
                });
            }
        });


    }


    public void performWhenActive(Runnable runnable){
        if (resumed == true){
            runnable.run();
        }else{
            resumeTasks.add(runnable);
        }
    }

    private void setupTabs(){

        View view1 = getLayoutInflater().inflate(R.layout.gui_custom_tab, null);
        ((ImageView) view1.findViewById(R.id.icon)).setImageResource(R.drawable.icon_home);
        tabLayout.getTabAt(0).setCustomView(view1);




        View view2 = getLayoutInflater().inflate(R.layout.gui_custom_tab, null);
        ((ImageView) view2.findViewById(R.id.icon)).setImageResource(R.drawable.icon_heart);
        tabLayout.getTabAt(1).setCustomView(view2);







        View view3 = getLayoutInflater().inflate(R.layout.gui_custom_tab, null);
        ((ImageView) view3.findViewById(R.id.icon)).setImageResource(R.drawable.icon_chat);
        tabLayout.getTabAt(2).setCustomView(view3);






        View view4 = getLayoutInflater().inflate(R.layout.gui_custom_tab, null);
        ((ImageView) view4.findViewById(R.id.icon)).setImageResource(R.drawable.icon_settings);
        tabLayout.getTabAt(3).setCustomView(view4);


        for (int i = 0 ; i < tabLayout.getTabCount() ; i++){

            final int index = i;

            ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    GUI.shakeView(tabLayout.getTabAt(index).getCustomView(), 15f, 3, shakeAnimationDuration, new GUI.CollapseExpandListener() {
                        @Override
                        public void onComplete(View view) {
                            UIcontroller.getInstance().switchTab(index);

                        }
                    });
                }
            });
        }




    }

    private void setMessageCount(){
        messageCount = 0;
        if (User.getCurrent()!= null && User.getCurrent().getUnseenMatches() != null){
            messageCount = User.getCurrent().getUnseenMatches().length;
        }
        View view = tabLayout.getTabAt(2).getCustomView();
        view.findViewById(R.id.notification_count_layout).setVisibility(View.GONE);
        if (messageCount != 0){
            view.findViewById(R.id.notification_count_layout).setVisibility(View.VISIBLE);
            ((TextView)view.findViewById(R.id.text_view_notification_count)).setText(messageCount+"");
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        if ( locationServiceBinder != null){
            locationServiceBinder.getService().moveToBackground();
        }
        Log.d("TAG", "onResume: ");
        resumed = true;
        while (resumeTasks.isEmpty() == false){
            final Runnable runnable = resumeTasks.remove();
            Thread.waitAndRunOnUi(new Runnable() {
                @Override
                public void run() {

                    runnable.run();
                }
            },200);

        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    public void onPause(){
        super.onPause();
        if (isCheckedIn == true){
            if (locationServiceBinder != null) {
                locationServiceBinder.getService().moveToForeground();
            }
        }
        resumed = false;
    }



    private void hideMeetTab(){


        GUI.collapseTabHorizontal(tabLayout, defaultAnimationDuration,  1, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {
                UIcontroller.getInstance().switchTab(0);
            }
        });
    }

    private void showMeetTab(){
        Log.d("selectTab", "showMeetTab: ");

        GUI.expandTabHorizontal(tabLayout, defaultAnimationDuration, 1, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {

               Thread.runOnUi(new Runnable() {
                   @Override
                   public void run() {
                       UIcontroller.getInstance().switchTab(1);

                   }
               });
            }
        });

    }


    private void showProgressOverlay(GUI.CollapseExpandListener collapseExpandListener){
        Log.d("TAG", "showProgressOverlay: ");
        findViewById(R.id.save_layout).setAlpha(0f);
        findViewById(R.id.save_layout).setVisibility(View.VISIBLE);
        GUI.fadeInView(findViewById(R.id.save_layout),0, MainActivity.defaultAnimationDuration, collapseExpandListener);
    }


    private void hideProgressOverlay(final GUI.CollapseExpandListener collapseExpandListener){
        Log.d("TAG", "hideProgressOverlay: ");
        GUI.fadeOutView(findViewById(R.id.save_layout), 0, MainActivity.defaultAnimationDuration, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        findViewById(R.id.save_layout).setVisibility(View.GONE);

                    }
                });
                if (collapseExpandListener != null){
                    collapseExpandListener.onComplete(view);
                }
            }
        });
    }





    @Override
    public void onBackPressed() {

        boolean retVal = UIcontroller.getInstance().goBack();

        if (retVal == false){

            if (System.currentTimeMillis() - lastBackPressed < defaultAnimationDuration * 3) {
                super.onBackPressed();

            } else {
                findViewById(R.id.text_view_exit).setVisibility(View.VISIBLE);
                GUI.fadeInView(findViewById(R.id.text_view_exit),0, defaultAnimationDuration, null);
                Thread.waitAndRunOnUi(new Runnable() {
                    @Override
                    public void run() {
                        GUI.fadeOutView(findViewById(R.id.text_view_exit), 0, defaultAnimationDuration  , new GUI.CollapseExpandListener() {
                            @Override
                            public void onComplete(View view) {
                                Thread.runOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        findViewById(R.id.text_view_exit).setVisibility(View.GONE);

                                    }
                                });
                            }
                        });
                    }
                },defaultAnimationDuration* 2);

                lastBackPressed = System.currentTimeMillis();
            }
        }




    }



    private void hideCheckoutTab(final Runnable runnable){
        GUI.collapseViewHorizontal(findViewById(R.id.button_checkout), defaultAnimationDuration, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {
                runnable.run();
            }
        });
    }

    private void showCheckoutTab(/*final Runnable runnable*/){
        GUI.expandViewHorizontal(findViewById(R.id.button_checkout), defaultAnimationDuration, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {
                //runnable.run();
            }
        });
    }



    private void checkIn(/*Runnable runnable*/){

        showMeetTab();
        showCheckoutTab(/*runnable*/);
        //((HomeFragment)fragments[0]).hideNearbyLayout();

    }

    private void checkOut(Runnable runnable){

        hideMeetTab();
        hideCheckoutTab(runnable);
        //((HomeFragment)fragments[0]).showNearbyLayout();
    }

    private void startLocationService(){
        Log.d("locationnnn", "location: got permissions");
        stopService(new Intent(this,MeetMeLocationService.class));
        Intent serviceIntent = new Intent(this, MeetMeLocationService.class);
        bindService(serviceIntent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d("locationnnn", "location: connected to service");
                locationServiceBinder = (MeetMeLocationService.LocationServiceBinder) service;
                //locationServiceBinder.getService().moveToBackground();
                MainActivity.this.serviceConnection = this;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        },Context.BIND_AUTO_CREATE);
    }

    private void showLocationPermissionRational(){
        GUI.showAlert(MainActivity.this, "Unfortunately, without location permission thw app won't be able to operate, please allow it.",
                new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
    }

    private void requestPermissions(){
        ArrayList<String> permissionRequests = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {

            }
            permissionRequests.add(android.Manifest.permission.ACCESS_FINE_LOCATION);

        }else {
            startLocationService();
        }


        if (permissionRequests.size() != 0) {
            String[] permissionRequestsArray = new String[permissionRequests.size()];
            permissionRequestsArray = permissionRequests.toArray(permissionRequestsArray);
            ActivityCompat.requestPermissions(this,
                    permissionRequestsArray,
                    0);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {


        switch (requestCode) {
            case 0:
                for (int i = 0; i < permissions.length; i++) {
                        if (grantResults.length > i && grantResults[i] == PackageManager.PERMISSION_GRANTED){
                        switch (permissions[i]) {
                            case Manifest.permission.ACCESS_FINE_LOCATION:
                                //Intent broadcastIntent = new Intent();
                               // broadcastIntent.setAction(MainActivity.location_granted_broadcast);
                                //sendBroadcast(broadcastIntent);
                                startLocationService();
                                break;
                            default:
                                continue;
                        }
                    }else{
                            switch (permissions[i]) {
                                case Manifest.permission.ACCESS_FINE_LOCATION:
                                    showLocationPermissionRational();
                                    break;
                                default:
                                    continue;
                            }
                        }
                }
            default:
        }

        return;
    }




    public void showMatchDialog(final String matchId){
        if (matchDialogShown == true){
            return;
        }
        matchDialogShown = true;

        final Dialog dialog = new Dialog(this){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
            }

            @Override
            public void onStart() {
                super.onStart();

                int height = GUI.height;
                int width = GUI.width;


                Log.d("TAG", "onStart: " + height);

                getWindow().setLayout(width, height);
            }


            @Override
            public void show() {
                super.show();
                getWindow().getDecorView().setAlpha(0);
                this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

                this.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    boolean started = false;
                    @Override
                    public void onGlobalLayout() {
                        Thread.waitAndRunOnUi(new Runnable() {
                            @Override
                            public void run() {
                                started = true;
                                //final View blurView = findViewById(R.id.blur_view);
                                GUI.fadeInView(getWindow().getDecorView(), 0, fastAnimationDuration, null);
                                GUI.shakeView(findViewById(R.id.main_view), 10, 5, shakeAnimationDuration, new GUI.CollapseExpandListener() {
                                    @Override
                                    public void onComplete(View view) {

                                    }
                                });
                            }
                        },defaultAnimationDuration / 3);
                        getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    }
                });

            }

            @Override
            protected void onStop() {
                super.onStop();
            }

            private void superDismiss(){
                super.dismiss();
            }

            @Override
            public void dismiss() {
                GUI.collapseView(findViewById(R.id.main_view),fastAnimationDuration, null);
                GUI.fadeOutView(getWindow().getDecorView(), 0, fastAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                superDismiss();
                                matchDialogShown = false;
                            }
                        });
                    }
                });
            }
        };
        Match match = Rest.getInstance().getCached(Match.class, matchId);
        dialog.setContentView(R.layout.match_dialog);
        dialog.setTitle("Title...");

        String userId;
        if (match.getUserA().equals(User.getCurrent().getId()) == true){
            userId = match.getUserB();
        }else{
            userId = match.getUserA();
        }



        ImageButton closeButton = (ImageButton) dialog.findViewById(R.id.button_close);
        ImageButton messageButton = (ImageButton) dialog.findViewById(R.id.button_message);
        TextView textView = (TextView)dialog.findViewById(R.id.text_view);
        User user = Rest.getInstance().getCached(User.class, userId);
        textView.setText("Congratulations!\nYou matched with " + user.getFirstName() + " " + user.getLastName());
        // if button is clicked, close the custom dialog
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Thread.waitAndRunOnUi(new Runnable() {
                    @Override
                    public void run() {
                        tabLayout.getTabAt(2).select();
                        //tabReselected();
                        MessagesFragment.showMessages(MainActivity.this, matchId);
                    }
                },defaultAnimationDuration);


            }
        });


        Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                dialog.show();
                dialog.getWindow().setLayout(GUI.width, GUI.height);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
        },(int)(defaultAnimationDuration * 2f) );


    }








    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (serviceConnection != null) {
            unbindService(serviceConnection);
        }


    }



}
