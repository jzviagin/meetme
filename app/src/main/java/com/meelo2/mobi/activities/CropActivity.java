package com.meelo2.mobi.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.meelo2.mobi.GUI.CropView;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.entities.User;
import com.steelkiwi.cropiwa.image.CropIwaResultReceiver;

;

public class CropActivity extends BaseActivity {


    private Uri srcUri;
    private Uri destUri;
    private boolean redirectToProfile;



    @Override
    protected void onResume(){
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crop_activity);


        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.arrow_back_menu).mutate();
        upArrow.setColorFilter(getResources().getColor(R.color.colorText), PorterDuff.Mode.SRC_IN);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        ((TextView)findViewById(R.id.title_name)).setText("Crop Image");

        srcUri = (Uri)getIntent().getParcelableExtra("uri");
        destUri = (Uri)getIntent().getParcelableExtra("destUri");
        redirectToProfile = getIntent().getBooleanExtra("redirectToProfile",false);


        CropView cropView = findViewById(R.id.crop_view);
        cropView.setData(srcUri, destUri, new CropIwaResultReceiver.Listener() {
            @Override
            public void onCropSuccess(Uri croppedUri) {
                Log.d("TAG", "onCropSuccess: " );
                User.getCurrent().addPhoto("file://"+destUri.toString());
                Intent intent = new Intent(CropActivity.this, ProfileActivity.class);

                ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_left, R.anim.exit_out_right);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent, activityOptions.toBundle());




            }

            @Override
            public void onCropFailed(Throwable e) {
                Log.d("TAG", "onCropFailed: ");

            }
        });




    }











    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_menu_done);
        Drawable favoriteIcon = DrawableCompat.wrap(menuItem.getIcon());
        ColorStateList colorSelector = ResourcesCompat.getColorStateList(getResources(), R.color.colorText, getTheme());
        DrawableCompat.setTintList(favoriteIcon, colorSelector);
        menuItem.setIcon(favoriteIcon);
        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {


                CropView cropView = findViewById(R.id.crop_view);
                cropView.save();


                return true;
            }
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CropActivity.this, GalleryActivity.class);
        if (redirectToProfile == true){
            intent = new Intent(CropActivity.this, ProfileActivity.class);

        }
        ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_left, R.anim.exit_out_right);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent, activityOptions.toBundle());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
