package com.meelo2.mobi.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by jzvia on 23/07/2018.
 */

public class BaseActivity extends AppCompatActivity {


    private static final int customReqCode = 99;

    private ActivityResultHandler activityResultHandler;

    public static interface ActivityResultHandler {
        public void onActivityResult(int resultCode, Intent data);
    }



    private Runnable onPermissionGranted;
    private String requestedPermission;


    public void requestPermission(String  permission, Runnable runnable){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    permission)
                    != PackageManager.PERMISSION_GRANTED){
                onPermissionGranted = runnable;
                requestedPermission = permission;
                requestPermissions(new String[] {permission}, customReqCode);

            }else{
                runnable.run();
            }
        }else{
            runnable.run();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == customReqCode){
            for(int i = 0 ; i < permissions.length ; i++){
                if (permissions[i].equals(requestedPermission) && grantResults[i] == PackageManager.PERMISSION_GRANTED){
                    onPermissionGranted.run();
                }
            }
        }
    }




    public void startActivityForResult(Intent intent, ActivityResultHandler activityResultHandler ) {
        this.activityResultHandler = activityResultHandler;
        super.startActivityForResult(intent, customReqCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == customReqCode){
            if (activityResultHandler != null) {
                activityResultHandler.onActivityResult(resultCode, data);
            }else{
                Log.d("TAG", "onActivityResult: ");
            }
        }

    }
}
