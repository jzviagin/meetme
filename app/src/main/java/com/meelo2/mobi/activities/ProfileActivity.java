package com.meelo2.mobi.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.meelo2.mobi.GUI.UserProfileView;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.entities.User;

;

public class ProfileActivity extends BaseActivity {


    private Runnable initRunnable;
    private MenuItem doneButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);


        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ((TextView)findViewById(R.id.title_name)).setText("My Profile");
        ImageView imageView = findViewById(R.id.image_view_title);
        if (User.getCurrent().getPicList().length != 0) {
            GUI.loadRoundedCornersImage(User.getCurrent().getPicList()[0], imageView, GUI.Quality.HALF_WIDTH_565, imageView.getLayoutParams().width / 2, imageView.getLayoutParams().width, imageView.getLayoutParams().height);
        }




    }











    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_menu_done);
        doneButton = menuItem;
        Drawable favoriteIcon = DrawableCompat.wrap(menuItem.getIcon());
        ColorStateList colorSelector = ResourcesCompat.getColorStateList(getResources(), R.color.colorText, getTheme());
        DrawableCompat.setTintList(favoriteIcon, colorSelector);
        menuItem.setIcon(favoriteIcon);
        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (((UserProfileView)findViewById(R.id.user_profile_view)).readyToSave() == false){
                    return true;
                }

                try {

                    InputMethodManager inputMethodManager =
                            (InputMethodManager) getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(
                            getCurrentFocus().getWindowToken(), 0);

                }catch (Exception e){
                    e.printStackTrace();
                }



                showProgressOverlay(new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        ((UserProfileView)findViewById(R.id.user_profile_view)).saveData();

                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {

                                hideProgressOverlay(new GUI.CollapseExpandListener() {
                                    @Override
                                    public void onComplete(View view) {
                                        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                                        ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent, activityOptions.toBundle());
                                    }
                                });

                            }
                        });
                    }
                });




                return true;
            }
        });
        initRunnable = new Runnable() {
            @Override
            public void run() {
                boolean facebookGallery = false;
                if (facebookGallery = AccessToken.getCurrentAccessToken() == null){
                    facebookGallery = false;
                }else {
                    facebookGallery = AccessToken.getCurrentAccessToken().getPermissions().contains("user_photos");
                }
                if (facebookGallery == true) {
                    Intent intent = new Intent(ProfileActivity.this, GalleryActivity.class);
                    ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent, activityOptions.toBundle());
                }else{
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    // Do not include the extras related to cropping here;
                    // this Intent is for selecting the image only.
                    startActivityForResult(photoPickerIntent, new BaseActivity.ActivityResultHandler() {
                        @Override
                        public void onActivityResult(int resultCode, Intent data) {
                            if (resultCode == Activity.RESULT_OK) {
                                GalleryActivity.onReceiveImageUri(ProfileActivity.this, data.getData(), false, true);


                            }else{
                                ((UserProfileView)findViewById(R.id.user_profile_view)).loadData(initRunnable, new UserProfileView.ProfileCompleteListener() {
                                    @Override
                                    public void onProfileCompleteChanged(boolean canProceed) {
                                        doneButton.setEnabled(canProceed);
                                        if (canProceed == true){
                                            doneButton.getIcon().setAlpha(255);
                                        }else{
                                            doneButton.getIcon().setAlpha(100);

                                        }

                                    }
                                });
                            }
                        }
                    });
                }
            }
        };

        ((UserProfileView)findViewById(R.id.user_profile_view)).loadData(initRunnable, new UserProfileView.ProfileCompleteListener() {
            @Override
            public void onProfileCompleteChanged(boolean canProceed) {
                doneButton.setEnabled(canProceed);
                if (canProceed == true){
                    doneButton.getIcon().setAlpha(255);
                }else{
                    doneButton.getIcon().setAlpha(100);

                }
            }
        });

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return true;
    }


    private void showProgressOverlay(GUI.CollapseExpandListener collapseExpandListener){
        findViewById(R.id.save_layout).setAlpha(0f);
        findViewById(R.id.save_layout).setVisibility(View.VISIBLE);
        GUI.fadeInView(findViewById(R.id.save_layout),0, MainActivity.defaultAnimationDuration, collapseExpandListener);
    }


    private void hideProgressOverlay(final GUI.CollapseExpandListener collapseExpandListener){
        GUI.fadeOutView(findViewById(R.id.save_layout), 0, MainActivity.defaultAnimationDuration, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        findViewById(R.id.save_layout).setVisibility(View.GONE);

                    }
                });
                if (collapseExpandListener != null){
                    collapseExpandListener.onComplete(view);
                }
            }
        });
    }






}
