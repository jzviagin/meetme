package com.meelo2.mobi.activities;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;

import com.meelo2.mobi.app.R;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.api.Kvs;
import com.meelo2.mobi.Utils.Rest;;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.entities.User;

public class SplashActivity extends BaseActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);


        final View view = findViewById(R.id.main_view);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int[] loc = new int[2];
                view.getLocationOnScreen(loc);
                GUI.statusBarHeight = loc[1];

                int displayHeight = 0;

                Point size = new Point();


                getWindowManager().getDefaultDisplay().getRealSize(size);
                displayHeight = size.y;
                GUI.navKeysHeight = displayHeight - view.getMeasuredHeight() - loc[1];
                System.out.println("dimensions: status bar: " + GUI.statusBarHeight + " navBar: " + GUI.navKeysHeight +
                        " display height: " + displayHeight);
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                if (Kvs.get("sessionToken") == null){
                    Intent intent = new Intent(SplashActivity.this, EntranceActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Thread.runOnExecutor(new Runnable() {
                        @Override
                        public void run() {
                            User user = Rest.getInstance().login();
                            if (user != null){
                                Thread.runOnExecutor(new Runnable() {
                                    @Override
                                    public void run() {
                                        Thread.runOnUi(new Runnable() {
                                            @Override
                                            public void run() {
                                                MainActivity.getInstance().init();
                                            }
                                        });
                                        Thread.sleep(6000);
                                        finish();
                                    }
                                });
                            }else{
                                Intent intent = new Intent(SplashActivity.this, EntranceActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }

            }
        });







    }




}
