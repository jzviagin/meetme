package com.meelo2.mobi.activities;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.meelo2.mobi.GUI.GalleryView;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.api.Var;

;import java.io.File;
import java.util.Date;

public class GalleryActivity extends BaseActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_activity);


        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.arrow_back_menu).mutate();
        upArrow.setColorFilter(getResources().getColor(R.color.colorText), PorterDuff.Mode.SRC_IN);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        ((GalleryView)findViewById(R.id.gallery_view)).setSelectedItemCallback(new GalleryView.SelectedItemCallback() {
            @Override
            public void itemSelected(Uri uri, boolean isWeb) {
                onReceiveImageUri(GalleryActivity.this, uri, isWeb, false);
            }
        });



    }

    public static void onReceiveImageUri(Context context, Uri uri, boolean isWeb, boolean redirectToProfile){
        try {
            Log.d("TAG", "onReceiveImageUri: " + uri.toString());
            File tempFile = Var.createTempImageFile(context);
            if (isWeb == false ){
                String [] segments = uri.getLastPathSegment().split("\\.");
                String suffix = segments[segments.length - 1];
                suffix = "." + suffix;
                File sourceFile = File.createTempFile(new Date().getTime() + "", suffix, context.getExternalCacheDir());

                Var.copyFile(context, uri, sourceFile);
                Uri srcUri = Uri.fromFile(sourceFile);
                uri = srcUri;
            }
            Uri destUri = Uri.fromFile(tempFile);
            Intent intent = new Intent(context, CropActivity.class);
            intent.putExtra("uri", uri);
            intent.putExtra("destUri", destUri);
            intent.putExtra("redirectToProfile", redirectToProfile);
            ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(context.getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent, activityOptions.toBundle());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(GalleryActivity.this, ProfileActivity.class);
        ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_left, R.anim.exit_out_right);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent,activityOptions.toBundle());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }











}
