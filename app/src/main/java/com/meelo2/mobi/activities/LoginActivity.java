package com.meelo2.mobi.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.meelo2.mobi.api.Kvs;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.Utils.GUI;
import com.meelo2.mobi.Utils.Rest;
;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.entities.User;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class LoginActivity extends BaseActivity {

    public static CallbackManager callbackManager = CallbackManager.Factory.create();



    private void setServer(){

                Rest.setServer(((EditText)findViewById(R.id.edit_text_server)).getText().toString());
                Rest.setPort(((EditText)findViewById(R.id.edit_text_port)).getText().toString());



    }

    private void loadDefaultServer(){
        Rest.setDefaults();
        ((EditText)findViewById(R.id.edit_text_server)).setText(Rest.getServer());
        ((EditText)findViewById(R.id.edit_text_port)).setText(Rest.getPort());


    }

    private void loadServer(){
        ((EditText)findViewById(R.id.edit_text_server)).setText(Rest.getServer());
        ((EditText)findViewById(R.id.edit_text_port)).setText(Rest.getPort());


    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Rest.init(this);
        setContentView(R.layout.login_activity);

        loadServer();

        findViewById(R.id.button_defaults).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDefaultServer();
            }
        });

        findViewById(R.id.button_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setServer();
            }
        });

        /*findViewById(R.id.button_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWithFacebook();
            }
        });*/
        findViewById(R.id.button_log_in_with_phone_number).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWithPhone();
            }
        });

        findViewById(R.id.passcode_layout).setAlpha(0);
        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                if (Rest.getInstance().isPasscodeRequired() == true){
                    Thread.runOnUi(new Runnable() {
                        @Override
                        public void run() {
                            GUI.fadeInView(findViewById(R.id.passcode_layout),0 , MainActivity.defaultAnimationDuration, null);
                        }
                    });
                }
            }
        });

    }

    private void showProgressOverlay(GUI.CollapseExpandListener collapseExpandListener){
        findViewById(R.id.save_layout).setAlpha(0f);
        findViewById(R.id.save_layout).setVisibility(View.VISIBLE);
        GUI.fadeInView(findViewById(R.id.save_layout),0, MainActivity.defaultAnimationDuration, collapseExpandListener);
    }

    private void hideProgressOverlay(final GUI.CollapseExpandListener collapseExpandListener){
        GUI.fadeOutView(findViewById(R.id.save_layout), 0, MainActivity.defaultAnimationDuration, new GUI.CollapseExpandListener() {
            @Override
            public void onComplete(View view) {
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        findViewById(R.id.save_layout).setVisibility(View.GONE);

                    }
                });
                if (collapseExpandListener != null){
                    collapseExpandListener.onComplete(view);
                }
            }
        });
    }


    private void hideSoftKeyboard() {

        if (getCurrentFocus() == null){
            return;
        }
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                getCurrentFocus().getWindowToken(), 0);


    }

    private void showSoftKeyboard() {


        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(
                getCurrentFocus(), InputMethodManager.SHOW_FORCED);
    }


    private void loginWithPhone(){
        showProgressOverlay(null);
        final String passcode = ((EditText)findViewById(R.id.code_entry)).getText().toString();
        Kvs.put("passcode", passcode);
        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                Log.d("login", "got token, trying to log in to server: ");
                try {

                    Rest.getInstance().checkPasscode(passcode);
                }catch (Rest.PasscodeException e){
                    showErrorDialog(e.getMessage());
                    return;
                }

                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        showCodeVerificationDialog(PhoneVerificationState.PhoneVerificationStatePhoneEntry, null);
                    }
                });

            }

        });



    }


    private void login(){


        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                Log.d("login", "got token, trying to log in to server: ");
                User user = null;
                try {
                    String passcode = ((EditText)findViewById(R.id.code_entry)).getText().toString();
                    Kvs.put("passcode", passcode);
                    user =Rest.getInstance().loginWithToken(passcode);
                }catch (Rest.PasscodeException e){
                    showErrorDialog(e.getMessage());
                    return;
                }
                if (user != null){
                    Log.d("TAG", "run: user is not null");

                    ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);

                    Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent, activityOptions.toBundle());
                }else{
                    Log.d("login", "login failed, registering new user ");
                    Set<String> permissions = AccessToken.getCurrentAccessToken().getPermissions();

                    if (permissions.contains("user_birthday") == false) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                showBirthdayPicker(false);

                            }
                        });
                    }else {
                        registerNewUser(null);
                    }

                }
            }
        });




    }

    private void verifyPhone(final String phone){
        showProgressOverlay(null);
        final String passcode = ((EditText)findViewById(R.id.code_entry)).getText().toString();
        Kvs.put("passcode", passcode);
        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                if (true == Rest.getInstance().loginWithPhone(phone, passcode)){
                    Thread.runOnUi(new Runnable() {
                        @Override
                        public void run() {
                            showCodeVerificationDialog(PhoneVerificationState.PhoneVerificationStateCodeEntry, phone);
                        }
                    });
                }else{
                    showErrorDialog("The phone number was entered incorrectly");
                }

            }
        });
    }

    private void verifyCode(final String phoneNumber, final String code){
        showProgressOverlay(null);
        final String passcode = ((EditText)findViewById(R.id.code_entry)).getText().toString();
        Kvs.put("passcode", passcode);
        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {
                User user = Rest.getInstance().verifyCode(phoneNumber, code, passcode);
                if (user != null) {
                    User.setCurrent(user);
                    if (user.getBirthDate() == null){
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                showBirthdayPicker(true);
                            }
                        });
                    }else{
                        login();
                    }

                }else{
                    reVerifyCode("The code was entered incorrectly", phoneNumber);
                }
            }
        });
    }

    private void reVerifyCode(final String message, final String phone){


        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.showAlert(LoginActivity.this, message, new Runnable() {
                    @Override
                    public void run() {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressOverlay(null);
                                Thread.runOnUi(new Runnable() {
                                    @Override
                                    public void run() {
                                        showCodeVerificationDialog(PhoneVerificationState.PhoneVerificationStateCodeEntry, phone);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

    }

    private void showBirthdayPicker(final boolean phoneRegistration ){
        final Dialog dialog = new Dialog(this){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
            }

            @Override
            public void onStart() {
                super.onStart();

                int height = GUI.height;
                int width = GUI.width;


                Log.d("TAG", "onStart: " + height);

                getWindow().setLayout(width, height);
            }


            @Override
            public void show() {
                super.show();
                getWindow().getDecorView().setAlpha(0);
                if (phoneRegistration == true){
                    findViewById(R.id.name_layout).setVisibility(View.VISIBLE);
                    final EditText editTextFirstName = findViewById(R.id.edit_text_first_name);
                    final EditText editTextLastName = findViewById(R.id.edit_text_last_name);
                    final ImageButton okButton = (ImageButton) findViewById(R.id.button_positive);
                    editTextFirstName.requestFocus();
                    editTextLastName.setOnKeyListener(new View.OnKeyListener() {
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                                if (okButton.isEnabled()){
                                    okButton.callOnClick();
                                    return true;
                                }
                                return false;
                            }
                            return false;
                        }
                    });
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    okButton.setEnabled(false);


                    TextWatcher textWatcher = new TextWatcher() {

                        public void afterTextChanged(Editable s) {
                            if (editTextFirstName.getText().toString().trim().equals("") == true
                                    || editTextLastName.getText().toString().trim().equals("") == true){
                                okButton.setEnabled(false);
                            }else{
                                okButton.setEnabled(true);

                            }
                        }

                        public void beforeTextChanged(CharSequence s, int start,
                                                      int count, int after) {
                        }

                        public void onTextChanged(CharSequence s, int start,
                                                  int before, int count) {
                        }
                    };

                    editTextFirstName.addTextChangedListener(textWatcher);
                    editTextLastName.addTextChangedListener(textWatcher);
                }else{
                    findViewById(R.id.name_layout).setVisibility(View.GONE);
                }
                this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

                this.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    boolean started = false;
                    @Override
                    public void onGlobalLayout() {
                        Thread.waitAndRunOnUi(new Runnable() {
                            @Override
                            public void run() {
                                started = true;
                                //final View blurView = findViewById(R.id.blur_view);
                                GUI.fadeInView(getWindow().getDecorView(), 0,MainActivity.fastAnimationDuration, null);
                                GUI.shakeView(findViewById(R.id.main_view), 10, 5, MainActivity.shakeAnimationDuration, new GUI.CollapseExpandListener() {
                                    @Override
                                    public void onComplete(View view) {

                                    }
                                });
                            }
                        },MainActivity.defaultAnimationDuration / 3);
                        getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    }
                });

            }

            @Override
            protected void onStop() {
                super.onStop();
            }

            private void superDismiss(){
                super.dismiss();
            }

            private void superCancel(){
                super.cancel();
            }

            public void accept(){
                GUI.collapseView(findViewById(R.id.main_view),MainActivity.fastAnimationDuration, null);
                GUI.fadeOutView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                dismiss();
                                //matchDialogShown = false;
                            }
                        });
                    }
                });
            }

            @Override
            public void cancel() {
                GUI.collapseView(findViewById(R.id.main_view),MainActivity.fastAnimationDuration, null);
                GUI.fadeOutView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                dismiss();
                                hideProgressOverlay(null);
                                //matchDialogShown = false;
                            }
                        });
                    }
                });
            }

            @Override
            public void dismiss() {
                super.dismiss();
               /* GUI.collapseView(findViewById(R.id.main_view),MainActivity.fastAnimationDuration, null);
                GUI.fadeOutView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                superDismiss();
                                hideProgressOverlay(null);
                                //matchDialogShown = false;
                            }
                        });
                    }
                });*/
            }
        };
        dialog.setContentView(R.layout.date_picker_dialog);
        dialog.setTitle("Title...");

        DatePicker datePicker = dialog.findViewById(R.id.date_picker);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -18);
        datePicker.setMaxDate(calendar.getTime().getTime());
        calendar.add(Calendar.YEAR, -7);
        datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) , calendar.get(Calendar.DATE));



        ImageButton okButton = (ImageButton) dialog.findViewById(R.id.button_positive);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {

                        EditText editTextFirstName = dialog.findViewById(R.id.edit_text_first_name);
                        EditText editTextLastName = dialog.findViewById(R.id.edit_text_last_name);

                        final String firstName = editTextFirstName.getText().toString();
                        final String lastName = editTextLastName.getText().toString();

                        DatePicker datePicker = dialog.findViewById(R.id.date_picker);
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth()+ 1,0,0);
                        Log.d("TAG", "run: " + calendar.getTime());





                        final Date date = calendar.getTime();
                        dialog.dismiss();
                        Thread.runOnExecutor(new Runnable() {
                            @Override
                            public void run() {
                                if (phoneRegistration == false) {
                                    registerNewUser(date);
                                }else{
                                    updateTempUserNameAndBirthDate( firstName, lastName, date);
                                }
                            }
                        });
                    }
                });


            }
        });
        // if button is clicked, close the custom dialog
        /*closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Thread.waitAndRunOnUi(new Runnable() {
                    @Override
                    public void run() {
                        tabLayout.getTabAt(2).select();
                        //tabReselected();
                        MessagesFragment.showMessages(MainActivity.this, matchId);
                    }
                },defaultAnimationDuration);


            }
        });*/


        Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                dialog.show();
                dialog.getWindow().setLayout(GUI.width, GUI.height);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
        },(int)(MainActivity.defaultAnimationDuration * 2f) );

    }

    enum PhoneVerificationState{
        PhoneVerificationStatePhoneEntry,
        PhoneVerificationStateCodeEntry
    }

    private void showCodeVerificationDialog(final PhoneVerificationState state, final  String phoneNumber){
        final Dialog dialog = new Dialog(this){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
            }

            @Override
            public void onStart() {
                super.onStart();

                int height = GUI.height;
                int width = GUI.width;


                Log.d("TAG", "onStart: " + height);

                getWindow().setLayout(width, height);
            }


            @Override
            public void show() {
                super.show();
                getWindow().getDecorView().setAlpha(0);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                final ImageButton okButton = (ImageButton) findViewById(R.id.button_positive);
                findViewById(R.id.name_layout).setVisibility(View.VISIBLE);
                final EditText editTextPhoneCode = findViewById(R.id.edit_text_phone_code);
                editTextPhoneCode.requestFocus();
                editTextPhoneCode.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                            if (okButton.isEnabled()){
                                okButton.callOnClick();
                                return true;
                            }
                            return false;
                        }
                        return false;
                    }
                });
                okButton.setEnabled(false);


                TextWatcher textWatcher = new TextWatcher() {

                    public void afterTextChanged(Editable s) {
                        if (editTextPhoneCode.getText().toString().trim().equals("") == true
                                ){
                            okButton.setEnabled(false);
                        }else{
                            okButton.setEnabled(true);

                        }
                    }

                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                    }
                };

                editTextPhoneCode.addTextChangedListener(textWatcher);

                this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

                this.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    boolean started = false;
                    @Override
                    public void onGlobalLayout() {
                        Thread.waitAndRunOnUi(new Runnable() {
                            @Override
                            public void run() {
                                started = true;
                                //final View blurView = findViewById(R.id.blur_view);
                                GUI.fadeInView(getWindow().getDecorView(), 0,MainActivity.fastAnimationDuration, null);
                                GUI.shakeView(findViewById(R.id.main_view), 10, 5, MainActivity.shakeAnimationDuration, new GUI.CollapseExpandListener() {
                                    @Override
                                    public void onComplete(View view) {
                                        Thread.runOnUi(new Runnable() {
                                            @Override
                                            public void run() {
                                                //showSoftKeyboard();

                                                //editTextPhoneCode.requestFocus();
                                                //showSoftKeyboard();
                                                //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                                            }
                                        });

                                    }
                                });
                            }
                        },MainActivity.defaultAnimationDuration / 3);
                        getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    }
                });



            }



            @Override
            protected void onStop() {
                super.onStop();
            }

            private void superDismiss(){
                hideSoftKeyboard();
                super.dismiss();
            }

            private void superCancel(){
                super.cancel();
            }

            public void accept(){
                GUI.collapseView(findViewById(R.id.main_view),MainActivity.fastAnimationDuration, null);
                GUI.fadeOutView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                dismiss();
                                //matchDialogShown = false;
                            }
                        });
                    }
                });
            }

            @Override
            public void cancel() {
                GUI.collapseView(findViewById(R.id.main_view),MainActivity.fastAnimationDuration, null);
                GUI.fadeOutView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                dismiss();
                                hideProgressOverlay(null);
                                //matchDialogShown = false;
                            }
                        });
                    }
                });
            }

            @Override
            public void dismiss() {
                super.dismiss();
               /* GUI.collapseView(findViewById(R.id.main_view),MainActivity.fastAnimationDuration, null);
                GUI.fadeOutView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                superDismiss();
                                hideProgressOverlay(null);
                                //matchDialogShown = false;
                            }
                        });
                    }
                });*/
            }
        };
        dialog.setContentView(R.layout.phone_verification_dialog);
        dialog.setTitle("Title...");

        EditText editPhoneCode = dialog.findViewById(R.id.edit_text_phone_code);
        LinearLayout countryLayout = (LinearLayout)dialog.findViewById(R.id.country_code_layout);
        final EditText editTextCountryCode = dialog.findViewById(R.id.edit_text_country_code);

        TextView textView = dialog.findViewById(R.id.text_view);

        if (state.equals(PhoneVerificationState.PhoneVerificationStatePhoneEntry)){
            editPhoneCode.setHint("Phone Number");
            InputFilter[] fArray = new InputFilter[0];
            editPhoneCode.setFilters(fArray);
            countryLayout.setVisibility(View.VISIBLE);

            editPhoneCode.setInputType(InputType.TYPE_CLASS_PHONE);
            textView.setText("Please enter your phone number and click OK to receive a verification code via SMS");
        }else{
            editPhoneCode.setHint("Verification Code");
            editPhoneCode.setInputType(InputType.TYPE_CLASS_NUMBER);
            int maxLength = 6;
            countryLayout.setVisibility(View.GONE);
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(maxLength);
            editPhoneCode.setFilters(fArray);
            textView.setText("Please enter the verification code you received via SMS to continue");
        }

        ImageButton okButton = (ImageButton) dialog.findViewById(R.id.button_positive);
        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {

                        dialog.dismiss();
                        EditText editPhoneCode = dialog.findViewById(R.id.edit_text_phone_code);
                        final String phoneCode = editPhoneCode.getText().toString();
                        if (state.equals(PhoneVerificationState.PhoneVerificationStatePhoneEntry)){
                            verifyPhone("+" + editTextCountryCode.getText().toString().trim() + phoneCode.replaceFirst("^0+(?!$)", ""));
                        }else{
                            verifyCode( phoneNumber, phoneCode);
                        }


                    }
                });


            }
        });
        // if button is clicked, close the custom dialog
        /*closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Thread.waitAndRunOnUi(new Runnable() {
                    @Override
                    public void run() {
                        tabLayout.getTabAt(2).select();
                        //tabReselected();
                        MessagesFragment.showMessages(MainActivity.this, matchId);
                    }
                },defaultAnimationDuration);


            }
        });*/


        Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                dialog.show();
                dialog.getWindow().setLayout(GUI.width, GUI.height);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
        },(int)(MainActivity.defaultAnimationDuration * 2f) );

    }


    public void loginWithFacebook()
    {
        // Set permissions

        AccessToken.setCurrentAccessToken(null);

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {

                        login();

                    }


                    @Override
                    public void onCancel() {
                        Log.d("TAG", "onCancel: ");
                        if (AccessToken.getCurrentAccessToken() != null) {
                            login();
                        }else {
                            Thread.runOnUi(new Runnable() {
                                @Override
                                public void run() {
                                    hideProgressOverlay(null);

                                }
                            });
                        }
                    }

                    @Override
                    public void onError(FacebookException error) {
                        error.printStackTrace();
                        finish();
                    }
                });

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_photos","user_birthday", "user_gender"));
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                showProgressOverlay(null);

            }
        });







    }

    private void updateTempUserNameAndBirthDate(String firstName, String lastName, Date date){
        final User user = User.getCurrent();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setBirthDate(new LocalDate(date));
        Rest.getInstance().update(User.class, user);

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);
                startActivity(intent, activityOptions.toBundle());
            }
        });







    }

    private void registerNewUser(final Date birthDate){
        final User newUser = new User();

        final Bundle parameters = new Bundle();
        parameters.putString("limit", "10000");
        parameters.putString("fields", "first_name,last_name,birthday,gender,picture.width(768).height(1024)");
        Log.d("login", "sent request to get my info from FB ");
        Log.d("TAG", "run: userid " + AccessToken.getCurrentAccessToken().getUserId());
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me", parameters, HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(final GraphResponse response) {
                        try {
                            final JSONObject jsonObject = response.getJSONObject();
                            newUser.setFirstName(jsonObject.getString("first_name"));
                            newUser.setLastName(jsonObject.getString("last_name"));
                            newUser.setGender("unknown");
                            if (jsonObject.has("gender")) {
                                if (jsonObject.getString("gender").equalsIgnoreCase("male")) {
                                    newUser.setGender("man");
                                }
                            }/*else{
                                newUser.setGender("man");
                            }*/
                            List<String> genderList = new LinkedList<String>();
                            if (jsonObject.has("interested_in")) {
                                JSONArray interestedIn = jsonObject.getJSONArray("interested_in");
                                for (int i = 0; i < interestedIn.length(); i++) {
                                    String value = interestedIn.getString(i);
                                    if (value.equalsIgnoreCase("male")) {
                                        genderList.add("man");
                                    } else {
                                        genderList.add("woman");
                                    }

                                }
                            }else{
                                /*if (newUser.getGender().equals("man")){
                                    genderList.add("woman");
                                }else{
                                    genderList.add("man");
                                }*/
                            }
                            newUser.setBio("");
                            newUser.setFacebookId(AccessToken.getCurrentAccessToken().getUserId());
                            newUser.setGenderPreference(genderList.toArray(new String[0]));
                            DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
                            LocalDate localDate;
                            if (AccessToken.getCurrentAccessToken().getPermissions().contains("user_birthday")) {
                                localDate = LocalDate.parse(jsonObject.getString("birthday"), formatter);
                            }else{
                                localDate = new LocalDate(birthDate);
                            }
                            newUser.setBirthDate(localDate);

                            String photoUrl = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");
                            Log.d("login", "started downloading user photo ");
                            GUI.loadDrawable(LoginActivity.this, photoUrl, new GUI.DrawableDownloadCallback() {
                                @Override
                                public void onDone(Drawable drawable) {

                                    uploadPhotoAndSaveUser(newUser,drawable);
                                }
                            }, 768,1024);

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }).executeAsync();

    }





    private void showErrorDialog(final String message){

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {
                GUI.showAlert(LoginActivity.this, message, new Runnable() {
                    @Override
                    public void run() {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressOverlay(null);
                            }
                        });
                    }
                });
            }
        });

    }


    private void uploadPhotoAndSaveUser(final User newUser, final Drawable drawable){
       // try {
            Log.d("login", "save file started: ");
            /*File tempFile = Var.createTempImageFile(LoginActivity.this);
            Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteArray = stream.toByteArray();


            FileOutputStream fileOuputStream =
                    new FileOutputStream(tempFile);
            fileOuputStream.write(byteArray);
            fileOuputStream.close();
            newUser.setPicList(new String[]{"file:" + tempFile.getAbsolutePath()});*/
            newUser.setPicList(new String[]{""});
            newUser.setUserName("facebook_" + AccessToken.getCurrentAccessToken().getUserId()+
                    "_" + newUser.getFirstName() + "_" + newUser.getLastName());

            User.setCurrent(newUser);


            Thread.runOnExecutor(new Runnable() {
                @Override
                public void run() {
                    String passcode = ((EditText)findViewById(R.id.code_entry)).getText().toString();
                    try {
                        Rest.ServerRetVal<User> retVal = Rest.getInstance().add(User.class, newUser, passcode);
                        if (retVal.getValues() == null || retVal.getValues().size() ==0 ){
                            showErrorDialog("An error occurred");
                            return ;
                        }
                    }catch (Rest.PasscodeException e){
                        showErrorDialog(e.getMessage());
                        return;
                    }

                    Log.d("login", "uploading photo started: ");
                    //Drawable drawable1 = Drawable.createFromPath(uri.getPath());
                    String url = Rest.getInstance().uploadPhoto( drawable);
                    if (url == null){
                        showErrorDialog("User registration failed, please try again later");
                        return;
                    }
                    Log.d("login", "uploading photo ended: ");
                    Log.d("TAG", "saveData: " + url);
                    newUser.getPicList()[0] = Uri.parse(url).getLastPathSegment();
                   /* for (int i = 0 ;i < newUser.getPicList().length ; i++){
                        Uri uri = Uri.parse(newUser.getPicList()[i]);
                        if (newUser.getPicList()[i].contains("file:") == true) {
                            Log.d("login", "uploading photo started: ");
                            //Drawable drawable1 = Drawable.createFromPath(uri.getPath());
                            String url = Rest.getInstance().uploadPhoto( drawable);
                            Log.d("login", "uploading photo ended: ");
                            Log.d("TAG", "saveData: " + url);
                            newUser.getPicList()[i] = Uri.parse(url).getLastPathSegment();
                        }else {
                            newUser.getPicList()[i] = uri.getLastPathSegment();
                        }

                    }*/
                    Rest.ServerRetVal<User> retValServer = Rest.getInstance().update(User.class, newUser);
                    User retVal = retValServer.getValues().get(0);
                    User.setCurrent(retVal);
                    ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.enter_from_right, R.anim.exit_out_left);

                    Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent, activityOptions.toBundle());
                }
            });
        /*}catch (IOException e){
            e.printStackTrace();
        }*/
    }





/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_menu_done);
        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                login();




                return true;
            }
        });
        return true;
    }*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
