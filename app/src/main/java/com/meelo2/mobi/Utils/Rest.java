package com.meelo2.mobi.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.meelo2.mobi.activities.MainActivity;
import com.meelo2.mobi.api.Kvs;
import com.meelo2.mobi.api.Thread;
import com.meelo2.mobi.api.Var;
import com.meelo2.mobi.entities.Entity;
import com.meelo2.mobi.entities.HangoutLocation;
import com.meelo2.mobi.entities.Match;
import com.meelo2.mobi.entities.Message;
import com.meelo2.mobi.entities.User;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by jzvia on 05/09/2017.
 */

public class Rest {

    //private static final int CHECK_IN_STATUS_TRUE = 1;
   // private static final int CHECK_IN_STATUS_FALSE = 2;
    //private static final int CHECK_IN_STATUS_UNKNOWN = 3;


    private static Rest instance;

    //private int checkInStatus = CHECK_IN_STATUS_UNKNOWN;

    private static final String BOUNDARY = "apiclient-" + System.currentTimeMillis();
    private static final String MIME_TYPE = "multipart/form-data;boundary=" + BOUNDARY;

    private Context context;


    public static class PasscodeException extends Exception{
        public PasscodeException(String message) {
            super(message);
        }
    }

    public static class  ServerRetVal<T extends Entity>{
        enum Status{
            StatusOK(0),
            StatusError(-1),
            StatusErrorWrongPasscode(-3),
            StatusErrorWrongVerificationCode(-4),
            StatusErrorPasscodeRequired(-2);
            int code;
            Status(int code){
                this.code = code;
            }
            int getCode(){
                return code;
            }

            public static Status fromCode(int code){
                for (Status st:values()){
                    if (st.getCode() == code){
                        return st;
                    }
                }
                return Status.StatusOK;
            }
        }
        private String statusDescription;
        private Status status;
        List<T> values;

        public String getStatusDescription() {
            return statusDescription;
        }

        public Status getStatus() {
            return status;
        }

        public List<T> getValues() {
            return values;
        }

        private ServerRetVal(){

        }

        public ServerRetVal(String statusDescription, Status status, List<T> values) {
            this.statusDescription = statusDescription;
            this.status = status;
            this.values = values;
        }
    }


    private static final Class [] entityTypes = new Class[] {User.class, HangoutLocation.class, Match.class,
    Message.class};


    public static void init(Context context){
        instance = new Rest();
        instance.context = context;
        loadData();
    }

    public static Rest getInstance(){
        return instance;
    }


    private  Map<Class,Map<String, Entity>> cache = new HashMap<Class,Map<String, Entity>>();

    public  <T extends Entity> T getCached(Class clazz, String id){
        if (cache.containsKey(clazz) == false){
            return null;
        }
        return (T)cache.get(clazz).get(id);
    }

    private  <T extends Entity> T convert( Class clazz, JSONObject input) {
        T t = (T) Entity.allocate(clazz);

        try {
            t.setId(input.getString("_id"));
            if (clazz == HangoutLocation.class) {


                ((HangoutLocation) t).setName(input.getString("name"));
                Integer[] ratingsArray = new Integer[4];
                ratingsArray[0] = input.getJSONObject("ratings").getInt("general");
                ratingsArray[1] = input.getJSONObject("ratings").getInt("sex_ration");
                ratingsArray[2] = input.getJSONObject("ratings").getInt("opposite_sex_popularit");
                ratingsArray[3] = input.getJSONObject("ratings").getInt("match_ratio");
                ((HangoutLocation) t).setRatings(ratingsArray);
                ((HangoutLocation) t).setAddress(input.getString("address"));
                ((HangoutLocation) t).setBio(input.getString("bio"));
                ((HangoutLocation) t).setDescription(input.getString("description"));
                ((HangoutLocation) t).setImageUrl(input.getString("imageUrl"));

                if (input.has("location")) {
                    JSONObject location = input.getJSONObject("location");
                    if (location.has("lat")){
                        double lat = location.getDouble("lat");
                        if (location.has("lng")){
                            double lng = location.getDouble("lng");
                            ((HangoutLocation) t).setLocation(new LatLng(lat,lng));

                        }
                    }

                }

                if (input.has("distance")) {
                    ((HangoutLocation) t).setDistance((float) input.getDouble("distance"));
                }else{
                    if (getCached(HangoutLocation.class, t.getId()) != null){
                        HangoutLocation prevInstance = getCached(HangoutLocation.class, t.getId());
                        ((HangoutLocation) t).setDistance(prevInstance.getDistance());
                    }else{
                        ((HangoutLocation) t).setDistance(0f);
                    }
                }
            }



            if (clazz == Message.class) {
                Log.d("TAG", "convert: message");

                ((Message) t).setMatchId(input.getString("matchId"));
                ((Message) t).setDirection(input.getBoolean("direction"));
                ((Message) t).setText(input.getString("text"));

            }




            if (clazz == Match.class) {
                if(input.has("userA")) {
                    ((Match) t).setUserA(input.getString("userA"));
                }

                if(input.has("userB")) {
                    ((Match) t).setUserB(input.getString("userB"));
                }

                //((Match) t).setMatchTime(new Date(input.getLong("matchTime")));
                if (input.has("token")) {
                    ((User) t).setToken(input.getString("token"));

                }
                if (input.has("lastMessageId")){
                    ((Match) t).setLastMessage(input.getString("lastMessageId"));
                }
               // JSONArray pics = input.getJSONArray("picList");
                //((User) t).setPicList(new String[pics.length()]);
               // for (int i = 0; i < pics.length(); i++) {
               //     ((User) t).getPicList()[i] = pics.getString(i);
               // }


            }





            if (clazz == User.class) {
                Log.d("TAG", "convert: user");

                ((User) t).setUserName(input.getString("userName"));
                ((User) t).setFirstName(input.getString("firstName"));
                ((User) t).setLastName(input.getString("lastName"));
                String tmp = input.getString("shortDescription");
                if (input.has("shortDescription") && input.getString("shortDescription") != null &&
                        input.getString("shortDescription").equalsIgnoreCase("null") == false ) {
                    ((User) t).setBio(input.getString("shortDescription"));
                }else{
                    ((User) t).setBio("");

                }
                if (input.has("unseen_matches")){
                    JSONArray unseenMatches = input.getJSONArray("unseen_matches");
                    String[] array = new String[unseenMatches.length()];
                    for (int i = 0 ; i < unseenMatches.length() ; i++){
                        Log.d("TAG", "convert: user found unseen match");
                        array[i] = unseenMatches.getString(i);
                    }
                    ((User) t).setUnseenMatches(array);
                }

                if (input.has("meelo_token")){
                    ((User) t).setMeeloToken(input.getString("meelo_token"));
                }


                try {

                    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");

                    LocalDate localDate = LocalDate.parse(input.getString("birthdate"), formatter);
                    ((User) t).setBirthDate(localDate);
                }catch (Exception e){
                    Log.d("TAG", "convert: error converting date");
                    e.printStackTrace();
                }
                if (input.has("token")) {
                    ((User) t).setToken(input.getString("token"));

                }
                JSONArray pics = input.getJSONArray("picList");
                ((User) t).setPicList(new String[pics.length()]);
                for (int i = 0; i < pics.length(); i++) {
                    ((User) t).getPicList()[i] = pics.getString(i);
                }

                ((User) t).setGender(input.getString("sex"));
                JSONArray sexualPreferences = input.getJSONArray("sexualPreference");

                String [] genderPreferences = new String[sexualPreferences.length()];

                for (int i = 0;  i < sexualPreferences.length() ; i++){
                    genderPreferences[i] = sexualPreferences.getString(i).toLowerCase();
                }
                ((User) t).setGenderPreference(genderPreferences);
                if (User.getCurrent() != null) {
                    if (t.getId().equals(User.getCurrent().getId())) {
                        Log.d("TAG", "convert: user current updated");
                        User.setCurrent((User) t);
                    }
                }



            }

            if (cache.containsKey(clazz) == false){
                cache.put(clazz, new HashMap<String, Entity>());

            }
            cache.get(clazz).put(t.getId(), t);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        return t;
    }

    public <T extends Entity> ServerRetVal<T> get( Class clazz, String params, boolean auth){
        List<T> retList = new LinkedList<>();
        //    private static JSONArray serverCall(Context context, Class clazz,  String params, String apiVersion, String entityName) {

        return typedServerCall( clazz, null, params, auth);

    }




    private  <T extends Entity> JSONObject convertToJson( Class clazz, T input) {
        //T t = (T) Entity.allocate(clazz);

        JSONObject jsonObject = new JSONObject();







        try {
            //t.setId(input.getInt("id"));
            if (clazz == Message.class) {

                Message message = (Message) input;
                jsonObject.put("text", message.getText());
                jsonObject.put("direction", message.getDirection());
                jsonObject.put("matchId", message.getMatchId());

                return  jsonObject;
            }

            if (clazz == User.class) {

                User user = (User) input;
                jsonObject.put("shortDescription", user.getBio());
                JSONArray picList = new JSONArray();
                for (int i = 0 ; i < user.getPicList().length; i++){
                    picList.put(user.getPicList()[i]);
                }
                jsonObject.put("picList",picList);

                JSONArray sexualPreference = new JSONArray();
                for (int i = 0 ; i < user.getGenderPreference().length; i++){
                    sexualPreference.put(user.getGenderPreference()[i]);
                }

                if (user.getUnseenMatches() != null) {
                    JSONArray unseenMatches = new JSONArray();
                    for (int i = 0; i < user.getUnseenMatches().length; i++) {
                        unseenMatches.put(user.getUnseenMatches()[i]);
                    }
                    jsonObject.put("unseen_matches", unseenMatches);
                }
                jsonObject.put("sexualPreference",sexualPreference);
                jsonObject.put("sex", user.getGender());

                jsonObject.put("userName",user.getUserName());
                jsonObject.put("lastName",user.getLastName());
                jsonObject.put("firstName",user.getFirstName());
                jsonObject.put("facebook_id",user.getFacebookId());
                jsonObject.put("birthdate",user.getBirthDate().toString("dd/MM/yyyy"));

                //jsonObject.put("direction", message.getDirection());
                //jsonObject.put("matchId", message.getMatchId());
                return  jsonObject;
            }

            /*
            *
                ((User) t).setUserName(input.getString("userName"));
                ((User) t).setFirstName(input.getString("firstName"));
                ((User) t).setLastName(input.getString("lastName"));
                ((User) t).setBio(input.getString("shortDescription"));


                try {

                    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");

                    LocalDate localDate = LocalDate.parse(input.getString("birthdate"), formatter);
                    ((User) t).setBirthDate(localDate);
                }catch (Exception e){
                    Log.d("TAG", "convert: error converting date");
                    e.printStackTrace();
                }
                if (input.has("token")) {
                    ((User) t).setToken(input.getString("token"));

                }
                JSONArray pics = input.getJSONArray("picList");
                ((User) t).setPicList(new String[pics.length()]);
                for (int i = 0; i < pics.length(); i++) {
                    ((User) t).getPicList()[i] = pics.getString(i);
                }

                ((User) t).setGender(input.getString("sex"));
                JSONArray sexualPreferences = input.getJSONArray("sexualPreference");*/




        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }



    public  <T extends Entity> ServerRetVal<T> query( Class clazz,String apiName,  String params, boolean auth){
        List<T> retList = new LinkedList<>();
        //    private static JSONArray serverCall(Context context, Class clazz,  String params, String apiVersion, String entityName) {

        return typedServerCall(clazz, apiName, params, auth);

    }

    public  <T extends Entity> ServerRetVal<T> add( Class clazz, T entity, String passcode) throws PasscodeException{
        List<T> retList = new LinkedList<>();
        //    private static JSONArray serverCall(Context context, Class clazz,  String params, String apiVersion, String entityName) {

        String objString = convertToJson(clazz, entity).toString();




        try {
            ServerRetVal<T> retVal = typedServerCall(clazz, "add", "?entity=" + URLEncoder.encode(objString, "utf-8")
                    + ((passcode != null && passcode.equals("") == false)?("&passcode=" + passcode):""), true);
            if (retVal.getStatus().equals(ServerRetVal.Status.StatusErrorWrongPasscode) || retVal.getStatus().equals(ServerRetVal.Status.StatusErrorPasscodeRequired)){
                throw new PasscodeException(retVal.getStatusDescription());
            }
            return retVal;




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }




    }

    public  <T extends Entity> ServerRetVal<T> update( Class clazz, T entity){
        List<T> retList = new LinkedList<>();
        //    private static JSONArray serverCall(Context context, Class clazz,  String params, String apiVersion, String entityName) {

        String objString = convertToJson(clazz, entity).toString();

        try {
            return typedServerCall(clazz, "update", "?entity=" + URLEncoder.encode(objString, "utf-8"), true);




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }




    }




    private  JSONArray getJsonArrayForClass(JSONObject input, Class clazz) throws JSONException {


        Log.d("TAG", "getJsonArrayForClass: " + clazz.getSimpleName());
        Iterator<String> iterator = input.keys();
        while (iterator.hasNext()) {
            String key = iterator.next();
            Log.d("TAG", "getJsonArrayForClass: key " + key);
            if (key.equalsIgnoreCase(clazz.getSimpleName())) {
                return input.getJSONArray(key);
            }
        }
        Log.d("TAG", "getJsonArrayForClass: not found");

        return null;
    }


    public  <T extends Entity> List<T> convertAll(Class clazz, JSONObject jsonObject)  {
        List<T> retList = new LinkedList<>();

        if (jsonObject == null)
        {
            return retList;
        }


        try {
            for (int i = 0; i < entityTypes.length; i++) {
                Class currentClass = entityTypes[i];
                JSONArray jsonArray = getJsonArrayForClass(jsonObject, currentClass);
                if (jsonArray != null) {
                    for (int j = 0; j < jsonArray.length(); j++) {

                        if (clazz.equals(currentClass)) {
                            T t = null;
                            t = convert(currentClass, jsonArray.getJSONObject(j));
                            retList.add(t);
                        } else {
                            convert(currentClass, jsonArray.getJSONObject(j));
                        }
                    }

                }
            }
            //if (retList.size() == 0){
             //   return null;
            //}
            return retList;
        }catch (JSONException e){
            e.printStackTrace();
        }

        return retList;
    }


    public  void block( final String userId){
        genericServerCall(context, User.class, "block" , "?id="+ userId, true);

    }
    public  void like( final String userId){
        final JSONObject reply = genericServerCall(context, User.class, "like" , "?id="+ userId, true);
        try {
            JSONArray matches = reply.getJSONArray("Match");
            if (matches == null  || matches.length() == 0){
                Log.d("TAG", "like: array not found");
                return;
            }
            final JSONObject match = matches.getJSONObject(0);
            if (match != null){
                if (match.getString("_id") != null){
                    Thread.runOnUi(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Intent broadcastIntent = new Intent();
                                broadcastIntent.putExtra("id", match.getString("_id"));
                                broadcastIntent.setAction(MainActivity.meet_broadcast);
                                context.sendBroadcast(broadcastIntent);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    });

                }else{
                    Log.d("TAG", "like: match came without id");
                }

            }else{
                Log.d("TAG", "like: match is null");
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public  void unlike( final String userId){
        genericServerCall(context, User.class, "unlike" , "?id="+ userId, true);
    }



    public  HangoutLocation   getCheckedInLocation(){
        Log.d("TAG", "checkin: get location: ");
        ServerRetVal<HangoutLocation> retVal = query( HangoutLocation.class, "checkedIn", "", true);
        List<HangoutLocation> hangoutLocations =    retVal.getValues();//query( HangoutLocation.class, "checkedIn", "", true).getValues();
        if(hangoutLocations != null && hangoutLocations.size()!= 0 ){
            Log.d("TAG", "checkin: received location: ");

            //if (checkInStatus != CHECK_IN_STATUS_TRUE){
            //    checkInStatus = CHECK_IN_STATUS_TRUE;
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtra("id", hangoutLocations.get(0).getId());
                broadcastIntent.setAction(MainActivity.checked_in_broadcast);
                context.sendBroadcast(broadcastIntent);
           // }

            return hangoutLocations.get(0);

        }else /*if (checkInStatus != CHECK_IN_STATUS_FALSE)*/{
            Log.d("TAG", "checkin: received null: ");

           // checkInStatus = CHECK_IN_STATUS_FALSE;
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(MainActivity.checked_out_broadcast);
            context.sendBroadcast(broadcastIntent);
        }
        return null;
    }


    public void checkIn(final Context context, final String locationId){
        ServerRetVal<HangoutLocation> retVal = query(HangoutLocation.class, "checkIn" , "?id="+ locationId, true);
        final List<HangoutLocation> reply = retVal.getValues();
        if (reply != null && reply.size() != 0/*&& checkInStatus != CHECK_IN_STATUS_TRUE*/){
            Thread.runOnUi(new Runnable() {
                @Override
                public void run() {
                //checkInStatus = CHECK_IN_STATUS_TRUE;
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtra("id", reply.get(0).getId());
                broadcastIntent.setAction(MainActivity.checked_in_broadcast);
                context.sendBroadcast(broadcastIntent);

                }
            });
        }
    }

    public void checkOut(final Context context){
        query( HangoutLocation.class, "checkOut" , "", true);
       // if (checkInStatus != CHECK_IN_STATUS_FALSE) {
       //     checkInStatus = CHECK_IN_STATUS_FALSE;
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(MainActivity.checked_out_broadcast);
            context.sendBroadcast(broadcastIntent);
       // }

    }



    public User login( String userName, String password){


        //User user = new   User();
        //user.setSessionToken("dsfsdfsdfsdfsdfsdfsdf");
        ServerRetVal<User> retVal = query( User.class, "login" ,  "?deviceToken=" + FirebaseInstanceId.getInstance().getToken()+"&userName=" + userName + "&password=" + password, false);
        List<User> users = retVal.getValues();
        if (users != null ){
            User.setCurrent(users.get(0));
            Kvs.put("sessionToken", users.get(0).getToken());
            return users.get(0);
        }

        return null;
    }

    public boolean loginWithPhone( String phoneNumber, String passcode){


        //User user = new   User();
        //user.setSessionToken("dsfsdfsdfsdfsdfsdfsdf");
        ServerRetVal<User> retVal = query( User.class, "login_with_phone_number" ,  "?phone_number=" +  phoneNumber
                + ((passcode != null && passcode.equals("") == false)?("&passcode=" + passcode):""), false);
        if (retVal.getStatus().equals(ServerRetVal.Status.StatusOK) == false){
            return false;
        }
        return true;
    }

    public User verifyCode( String phoneNumber, String smsCode, String passcode){
        //User user = new   User();
        //user.setSessionToken("dsfsdfsdfsdfsdfsdfsdf");
        ServerRetVal<User> retVal = query( User.class, "verify_phone_number" ,  "?phone_number=" +  phoneNumber + "&sms_code=" + smsCode
                + ((passcode != null && passcode.equals("") == false)?("&passcode=" + passcode):""), false);
        if (retVal.getStatus().equals(ServerRetVal.Status.StatusOK) == false){
            return null;
        }
        return retVal.getValues().get(0);
    }



    public boolean isPasscodeRequired(){
        JSONObject jsonObject = serverCall( User.class , "isPasscodeRequired" , "", false);
        if (jsonObject != null && jsonObject.has("required")){
            try {
                boolean required = jsonObject.getBoolean("required");
                return required;
            } catch (JSONException e) {
                e.printStackTrace();
                return true;
            }
        }
        return true;
    }





    public User loginWithToken (String passcode)throws PasscodeException{


        //User user = new   User();
        //user.setSessionToken("dsfsdfsdfsdfsdfsdfsdf");

        ServerRetVal<User> retVal = query( User.class, "login" ,  "?deviceToken=" +  FirebaseInstanceId.getInstance().getToken()/*+"&facebookToken=" + AccessToken.getCurrentAccessToken().getUserId().toString()*/
                + ((passcode != null && passcode.equals("") == false)?("&passcode=" + passcode):""), true);
        //retVal = new ServerRetVal<>(retVal.getStatusDescription(),ServerRetVal.Status.StatusErrorWrongPasscode, new ArrayList<User>() );
        if (retVal.getStatus().equals(ServerRetVal.Status.StatusErrorWrongPasscode) || retVal.getStatus().equals(ServerRetVal.Status.StatusErrorPasscodeRequired) ){
            throw new PasscodeException(retVal.getStatusDescription());
        }
        List<User> users = retVal.getValues();
        if (users != null && users.size() != 0 ){
            User.setCurrent(users.get(0));
            // Kvs.put("sessionToken", users.get(0).getToken());

            Log.d("TAG", "loginWithFacebookToken: " + users.get(0).getFirstName());
            return users.get(0);
        }

        return null;
    }




    public void checkPasscode(String passcode)throws PasscodeException{
        //if (passcode!= null && passcode.equalsIgnoreCase("2018")){
       //     return ;
       // }
        ServerRetVal<User> retVal = query( User.class, "verify_passcode" ,  "?passCode=" +  passcode , false);
        if (retVal.getStatus().equals(ServerRetVal.Status.StatusOK) == false){
            throw new PasscodeException(retVal.getStatusDescription());
        }


    }



    public User registerWithFacebookToken( ){


        //User user = new   User();
        //user.setSessionToken("dsfsdfsdfsdfsdfsdfsdf");
        ServerRetVal<User> retVal = query( User.class, "login" ,  "?deviceToken=" + FirebaseInstanceId.getInstance().getToken()/*+"&facebookToken=" + AccessToken.getCurrentAccessToken().getUserId().toString()*/ , true);
        List<User> users = retVal.getValues();
        if (users != null ){
            User.setCurrent(users.get(0));
            // Kvs.put("sessionToken", users.get(0).getToken());

            Log.d("TAG", "loginWithFacebookToken: " + users.get(0).getFirstName());
            return users.get(0);
        }

        return null;
    }



    public User login(){


        //User user = new   User();
        //user.setSessionToken("dsfsdfsdfsdfsdfsdfsdf");
        ServerRetVal<User> retVal = query( User.class, "loginToken" ,  "", true);
        List<User> users = retVal.getValues();
        if (users != null ){
            User.setCurrent(users.get(0));
            //Kvs.put("sessionToken", users.get(0).getToken());
            return users.get(0);
        }

        return null;
    }


    public static void setServer(String server){
        serverAddress = server;
        Kvs.put("server", server);
    }
    public static String getServer(){
        return serverAddress;
    }

    public static void setPort(String port){
        serverPort = port;
        Kvs.put("port", port);
    }
    public static String getPort(){
        return serverPort;
    }

    public static void setDefaults(){
        setServer(defaultServer);
        setPort(defaultServerPort);
    }

    public static void loadData(){
        serverAddress = (String)Kvs.get("server");
        serverPort = (String)Kvs.get("port");
        if (serverAddress == null || serverAddress.equals("")){
            setDefaults();
        }
    }



    public final static String defaultServer = "ec2-18-217-89-47.us-east-2.compute.amazonaws.com";//"192.168.1.135";
    public final static String defaultServerPort = "3000";
    public static String serverAddress = "ec2-18-217-89-47.us-east-2.compute.amazonaws.com";//"192.168.1.135";
    public static String serverPort = "3000";
    final static String apiVersion = "v0.1";


    private <T extends Entity > ServerRetVal<T > jsonToRetVal(Class clazz, JSONObject jsonObject){
        if (jsonObject== null){
            return new ServerRetVal("Unknown Error", ServerRetVal.Status.StatusError, new ArrayList<T>());
        }
        if (jsonObject.has("error_code")){
            ServerRetVal.Status status;
            String description = "Unknown Error";
            try {
                status = ServerRetVal.Status.fromCode(jsonObject.getInt("error_code"));
                description = jsonObject.getString("error_description");
            }catch(JSONException e){
                status = ServerRetVal.Status.StatusError;
                e.printStackTrace();
            }
            return new ServerRetVal(description, status, new ArrayList<T>());

        }
        return new ServerRetVal("OK", ServerRetVal.Status.StatusOK, convertAll(clazz, jsonObject));

    }

    private <T extends Entity> ServerRetVal<T > typedServerCall( Class clazz, String apiName, String params, final boolean auth) {
        return jsonToRetVal(clazz, serverCall( clazz , apiName , params, auth));
    }

    private JSONObject genericServerCall(Context context, Class clazz, String apiName, String params, final boolean auth) {
        JSONObject jsonObject = serverCall( clazz , apiName , params, auth);
        convertAll(Entity.class, jsonObject);
        return jsonObject;
    }



        private JSONObject serverCall(Class clazz, String apiName, String params, final boolean auth) {


        String url = "http://" + serverAddress + ":" + serverPort + "/" + apiVersion + "/" + clazz.getSimpleName() + "/" +
                (apiName != null ?  apiName + "/" : "") + params;
        RequestFuture<JSONObject> future = RequestFuture.newFuture();

        Log.d("servercall", "url: \n" + url);
        JsonObjectRequest jsonObjectRequest =
                new JsonObjectRequest
                        (Request.Method.POST, url, null, future, future) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        final HashMap<String, String> headers = new HashMap<>();
                        if (auth == true){
                            String meeloToken  = (String)Kvs.get("meeloToken");
                            if (User.getCurrent() == null && meeloToken != null && meeloToken.trim().equalsIgnoreCase("") == false ){
                                headers.put("AUTHORIZATION", "meeloToken " + meeloToken);
                            } else if (User.getCurrent() != null && User.getCurrent().getMeeloToken() != null) {
                                headers.put("AUTHORIZATION", "meeloToken " + User.getCurrent().getMeeloToken());
                            }else {
                                headers.put("AUTHORIZATION", "facebookToken " + ((AccessToken.getCurrentAccessToken() != null) ? AccessToken.getCurrentAccessToken().getToken(): ""));


                            }
                        }
                        return headers;
                    }




                    @Override
                    public byte[] getBody() {
                        return null;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json";
                    }
                };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 10, 0));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonObjectRequest);
        //requestQueue.start();
        final AtomicBoolean gotReply = new AtomicBoolean(false);
        try {

            JSONObject response = future.get(); // this will block
            Log.d("servercall", "reply: \n" + response.toString(4));
            return response;
        } catch (InterruptedException e ) {
            e.printStackTrace();
            gotReply.set(true);

        } catch (ExecutionException e) {
            if (e.getCause() instanceof TimeoutError){
                Thread.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        GUI.showAlert(context, "Sorry, the service is currently unavailable. Please try again later",
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        if (context instanceof Activity){
                                            Thread.runOnUi(new Runnable() {
                                                @Override
                                                public void run() {
                                                    ((Activity)context).finish();
                                                    //gotReply.set(true);
                                                }
                                            });
                                        }else{
                                            gotReply.set(true);
                                        }
                                    }
                                });
                    }
                });

            }else{
                VolleyError volleyError = (VolleyError) e.getCause();
                Log.d("TAG", ("ve = " + volleyError.toString()));
                if (volleyError.networkResponse != null) {
                   /* Log.d("TAG","ve.networkResponse = " +
                            volleyError.networkResponse.toString());
                    Log.d("TAG","ve.networkResponse.statusCode = " +
                            volleyError.networkResponse.statusCode);
                    Log.d("TAG","ve.networkResponse.data = " +
                            new String(ve.networkResponse.data));*/
                    String stringResponse = new String(volleyError.networkResponse.data);
                    Log.d("servercall", "reply: \n" + stringResponse);
                    try {
                        return new JSONObject(stringResponse);
                    }catch (JSONException e2){
                        e2.printStackTrace();
                        gotReply.set(true);
                    }
                }else {
                    gotReply.set(true);
                }


            }
            e.printStackTrace();
        }catch (JSONException e){
            gotReply.set(true);
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        while (gotReply.get() == false){
            Thread.sleep(200);
        }
       // return serverSet;
        return null;


    }

    private List<JSONObject> readJsonFromResource(Context context, int resourceId){
        //Get Data From Text Resource File Contains Json Data.
        InputStream inputStream = context.getResources().openRawResource(resourceId);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int ctr;
        try {
            ctr = inputStream.read();
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr);
                ctr = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            // Parse the data into jsonobject to get original data in form of json.
            //JSONObject jObject = new JSONObject(
           //         byteArrayOutputStream.toString());
            JSONArray jsonArray = new JSONArray(byteArrayOutputStream.toString());

         /*   JSONObject jObjectResult = jObject.getJSONObject("Categories");
            JSONArray jArray = jObjectResult.getJSONArray("Category");
            String cat_Id = "";
            String cat_name = "";
            ArrayList<String[]> data = new ArrayList<String[]>();
            for (int i = 0; i < jArray.length(); i++) {
                cat_Id = jArray.getJSONObject(i).getString("cat_id");
                cat_name = jArray.getJSONObject(i).getString("cat_name");
                Log.v("Cat ID", cat_Id);
                Log.v("Cat Name", cat_name);
                data.add(new String[] { cat_Id, cat_name });*/
           // }
            LinkedList<JSONObject> retVal = new LinkedList<>();
            for (int i = 0 ; i < jsonArray.length() ; i++){
                retVal.add(jsonArray.getJSONObject(i));
            }
            return retVal;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    public static void addAuthHeader(HashMap<String, String> headers)  {
        if (User.getCurrent().getMeeloToken() != null) {
            headers.put("AUTHORIZATION", "meeloToken " + User.getCurrent().getMeeloToken());
        }else {
            Log.d("TAG", "addAuthHeader: " + AccessToken.getCurrentAccessToken().getToken());
            headers.put("AUTHORIZATION", "facebookToken " + AccessToken.getCurrentAccessToken().getToken());
        }


    }

    public static void addAcceptJsonHeader(HashMap<String, String> headers)  {
        headers.put("Accept", "application/json");
    }

    public static void addContentTypeJsonHeader(HashMap<String, String> headers)  {
        headers.put("Content-Type", "application/json");
    }

    public static void addContentTypeMultipartFormData(HashMap<String, String> headers)  {
        headers.put("Content-Type", "application/json");
    }

    public static String buildFilePartHeaderString(String fileName){
        String retVal = "--" + BOUNDARY + "\r\nContent-Disposition: form-data; name=\"file\"; filename=\"" + fileName + "\"\r\n\r\n";
        return retVal;
    }

    public static String buildFilePartFooterString(){
        return "\r\n" + "--" + BOUNDARY + "--\r\n";
    }

    public static byte[] buildFilePartBuffer(byte[] fileData, String fileName)throws IOException{
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        dataOutputStream.writeBytes(buildFilePartHeaderString(fileName));
        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(fileData);
        int bytesAvailable = fileInputStream.available();

        int maxBufferSize = 1024 * 1024;
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        // read file and write it into form...
        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            dataOutputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        dataOutputStream.writeBytes(buildFilePartFooterString());
        return byteArrayOutputStream.toByteArray();

    }


   /* class MultipartRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;
        private final Map<String, String> mHeaders;
        private final String mMimeType;
        private final byte[] mMultipartBody;

        public MultipartRequest(String url, Map<String, String> headers, String mimeType, byte[] multipartBody, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(Method.PUT, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
            this.mHeaders = headers;
            this.mMimeType = mimeType;
            this.mMultipartBody = multipartBody;
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            return (mHeaders != null) ? mHeaders : super.getHeaders();
        }

        @Override
        public String getBodyContentType() {
            return mMimeType;
        }

        @Override
        public byte[] getBody() throws AuthFailureError {
            return mMultipartBody;
        }

        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            try {
                return Response.success(
                        response,
                        HttpHeaderParser.parseCacheHeaders(response));
            } catch (Exception e) {
                return Response.error(new ParseError(e));
            }
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }*/


    public  String uploadPhoto(final Drawable drawable){
        final RequestFuture<JSONObject> uploadFuture = RequestFuture.newFuture();

        try {
            Log.d("login", "save file started: ");
            final File file = Var.createTempImageFile(context);
            Bitmap bitmap = Bitmap.createScaledBitmap(((BitmapDrawable) drawable).getBitmap(), 768, 1024, true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);

            byte[] byteArray = stream.toByteArray();


            FileOutputStream fileOuputStream =
                    new FileOutputStream(file);
            fileOuputStream.write(byteArray);
            fileOuputStream.close();


            String url = "http://" + serverAddress + ":" + serverPort + "/" + apiVersion + "/" + "upload" + "/" + "user_photo/";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, uploadFuture, uploadFuture) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> uploadRequestHeaders = new HashMap<>();
                    addAuthHeader(uploadRequestHeaders);
                    addAcceptJsonHeader(uploadRequestHeaders);
                    return uploadRequestHeaders;
                }

                @Override
                public String getBodyContentType() {
                    return MIME_TYPE;
                }

                @Override
                public byte[] getBody() {
                    try {
                        return buildFilePartBuffer(Var.readFile(file), file.getName());
                    } catch (IOException e) {
                        return null;
                    }
                }


            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(request);
            try {
                JSONObject jsonObject = uploadFuture.get(); // this will block
                Log.d("TAG", "uploadFile: response " + jsonObject);
                if (jsonObject.has("file")) {
                    return jsonObject.getString("file");
                }
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }





}
