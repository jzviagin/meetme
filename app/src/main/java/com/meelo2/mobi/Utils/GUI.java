package com.meelo2.mobi.Utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meelo2.mobi.GUI.ZoomRecyclerView;
import com.meelo2.mobi.MeeloApp;
import com.meelo2.mobi.app.R;
import com.meelo2.mobi.activities.MainActivity;
import com.meelo2.mobi.api.Thread;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import java.util.concurrent.atomic.AtomicBoolean;

import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;

/**
 * Created by jzvia on 05/09/2017.
 */

public class GUI
{


    public static int width;
    public static int statusBarHeight;
    public static  int navKeysHeight;
    public static int height;

    static{

        final  WindowManager windowManager = (WindowManager) MeeloApp.getContext().getSystemService(Context.WINDOW_SERVICE);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
        height = displayMetrics.heightPixels;
        Log.d("TAG", "static initializer: " + statusBarHeight);
    }


    public enum Quality
    {
        FULL, RGB_565, CONTAINER_565, WIDTH_565, HALF_WIDTH_565, QUARTER_WIDTH_565
    }
    private static float roundedCornersTransformationCornerRadius = 0;
    private final static Transformation roundedCornersTransformation = new Transformation() {
        @Override
        public Bitmap transform(Bitmap source) {
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

            int width = source.getWidth();
            int height = source.getHeight();

//			if (roundedImageWidth > 0 && roundedImageHeight > 0) {
//				width = roundedImageWidth;
//				height = roundedImageHeight;
//			}

            Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            float radius = roundedCornersTransformationCornerRadius;
            canvas.drawRoundRect( new RectF(0, 0, width, height), radius, radius, paint);

            if (source != output) {
                source.recycle();
            }
            return output;
        }

        @Override
        public String key() {
            return "rounded_corners";
        }
    };

    private static int maskResId;
    private static ImageView shapeTransformationImageView;


    private final static Transformation customShapeTransformation = new Transformation() {
        @Override
        public Bitmap transform(Bitmap source) {
            Bitmap output = applyFilter(MainActivity.getInstance(), source, maskResId,shapeTransformationImageView.getLayoutParams().width,shapeTransformationImageView.getLayoutParams().height, shapeTransformationImageView.getMatrix());
            if (source != output) {
                source.recycle();
            }
            return output;
        }

        @Override
        public String key() {
            return "transparency_map";
        }
    };








    public interface DrawableDownloadCallback{
        public void onDone(Drawable drawable);
    }

    static Target target;


        public static void loadDrawable(final Context ctx, String url, final DrawableDownloadCallback callback) {
            loadDrawable(ctx,url, callback, width,height);
        }


        public static void loadDrawable(final Context ctx, String url, final DrawableDownloadCallback callback, final int width, final int height){

        target = new Target(){

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, com.squareup.picasso.Picasso.LoadedFrom from) {


                Thread.runOnExecutor(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("TAG", "onBitmapLoaded: ");
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(ctx.getResources(), cloneBitmap(ctx, bitmap));
                        callback.onDone(bitmapDrawable);
                    }
                });




            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.d("TAG", "onBitmapFailed: ");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.d("TAG", "onPrepareLoad: ");

            }
        };
        com.squareup.picasso.Picasso.with(ctx)
                .load(url)
                .resize(width, height)
                .centerCrop()
                .into(target);


    }




    private static RequestCreator getRequestCreator(String url, View view, Quality quality)
    {
        RequestCreator rc = com.squareup.picasso.Picasso.with(view.getContext()).load(url);
        switch (quality)
        {
            case RGB_565:
                rc = rc.config(Bitmap.Config.RGB_565);
                break;
            case CONTAINER_565:
                rc = rc.config(Bitmap.Config.RGB_565).resize(view.getWidth(), view.getHeight());
                break;
            case WIDTH_565:
                rc = rc.config(Bitmap.Config.RGB_565).resize(width, 0);
                break;
            case HALF_WIDTH_565:
                rc = rc.config(Bitmap.Config.RGB_565).resize(width / 2, 0);
                break;
            case QUARTER_WIDTH_565:
                rc = rc.config(Bitmap.Config.RGB_565).resize(width / 4, 0);
                break;
        }
        return rc;
    }

    private static RequestCreator getRequestCreatorResource(int resId, View view, Quality quality)
    {
        RequestCreator rc = com.squareup.picasso.Picasso.with(view.getContext()).load(resId);
        switch (quality)
        {
            case RGB_565:
                rc = rc.config(Bitmap.Config.RGB_565);
                break;
            case CONTAINER_565:
                rc = rc.config(Bitmap.Config.RGB_565).resize(view.getWidth(), view.getHeight());
                break;
            case WIDTH_565:
                rc = rc.config(Bitmap.Config.RGB_565).resize(width, 0);
                break;
            case HALF_WIDTH_565:
                rc = rc.config(Bitmap.Config.RGB_565).resize(width / 2, 0);
                break;
            case QUARTER_WIDTH_565:
                rc = rc.config(Bitmap.Config.RGB_565).resize(width / 4, 0);
                break;
        }
        return rc;
    }


    public static AlertDialog showAlertMessage(Context context, String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        try {
            AlertDialog alertDialog = builder.show();
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.BLACK);
            return alertDialog;
        } catch (Throwable t) {
            return null;
        }
    }

    public static abstract class CollapseExpandListener{
        Object O;
        public abstract void onComplete(View view);
        public CollapseExpandListener(){
            O = null;
        }
        public CollapseExpandListener(Object param){
            O = param;
        }
        public Object getParam(){
            return O;
        }
    }



    public static void fadeOutView(final View v, final float to , final int duration, final CollapseExpandListener listener) {

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {


                ObjectAnimator fadeOut = ObjectAnimator.ofFloat(v, "alpha", 1f, to);
                fadeOut.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.setAlpha(input );
                        return super.getInterpolation(input);
                    }
                });
                animationSet.play(fadeOut);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });
                animationSet.start();
            }
        });

    }

    public static void rotateView(final View v, final int duration, final CollapseExpandListener listener) {

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {


                ObjectAnimator rotate = ObjectAnimator.ofFloat(v, "rotation", v.getRotation(), v.getRotation()+ 180f);
                rotate.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.setRotation(input );
                        return super.getInterpolation(input);
                    }
                });
                animationSet.play(rotate);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });
                animationSet.start();
            }
        });

    }


    public  static Bitmap takeColorContrast(Bitmap src, double value) {
        // src image size
        int width = src.getWidth();
        int height = src.getHeight();
        // create output bitmap with original size
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        // color information
        int A, R, G, B;
        int pixel;
        // get contrast value
        double contrast = Math.pow((100 + value) / 100, 2);

        // scan through all pixels
        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                // get pixel color
                pixel = src.getPixel(x, y);
                A = Color.alpha(pixel);
                // apply filter contrast for every channel R, G, B
                R = Color.red(pixel);
                R = (int)(((((R / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if(R < 0) { R = 0; }
                else if(R > 255) { R = 255; }

                G = Color.green(pixel);
                G = (int)(((((G / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if(G < 0) { G = 0; }
                else if(G > 255) { G = 255; }

                B = Color.blue(pixel);
                B = (int)(((((B / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if(B < 0) { B = 0; }
                else if(B > 255) { B = 255; }

                // set new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        // return final image
        return bmOut;
    }


    public static Bitmap blurBitmap(Context context, Bitmap input) {

        Bitmap bitmap = Bitmap.createScaledBitmap(input, input.getWidth(), input.getHeight(), false);
        Bitmap outputBitmap = Bitmap.createBitmap(bitmap);

        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, bitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(20f);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }



    public static void rotateView(final View v, final float from, final float to, final int duration, final CollapseExpandListener listener) {

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {


                ObjectAnimator rotate = ObjectAnimator.ofFloat(v, "rotation", from, to );
                rotate.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.setRotation(input );
                        return super.getInterpolation(input);
                    }
                });
                animationSet.play(rotate);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });
                animationSet.start();
            }
        });

    }

    public static void shakeView(final View v, final float degrees, final int count, final int totalDuration, final CollapseExpandListener listener) {
        Thread.runOnExecutor(new Runnable() {
            AtomicBoolean rotationComplete = new AtomicBoolean(false);
            @Override
            public void run() {
                int singleDuration = totalDuration /(count * 2 + 2);
                float currentRotation = 0;
                float nextRotation = degrees;
                Log.d("TAG", "run: shake started");
                for (int i = 0; i < count * 2 + 2; i++){
                    rotationComplete.set(false);
                    rotateView(v, currentRotation, nextRotation, singleDuration, new CollapseExpandListener() {
                        @Override
                        public void onComplete(View view) {
                            rotationComplete.set(true);
                        }
                    });
                    int k = 0;
                    while (rotationComplete.get() == false){
                        k++;
                    }

                    if (currentRotation == 0){
                        if (nextRotation > 0){
                            currentRotation = degrees;
                            nextRotation = 0;
                        }else{
                            currentRotation = -degrees;
                            nextRotation = 0;
                        }
                    }else{
                        if (currentRotation > 0){
                            currentRotation = 0;
                            nextRotation = -degrees;
                        }
                        else {
                            currentRotation = 0;
                            nextRotation = degrees;
                        }
                    }

                }

                Log.d("TAG", "run: shake done");



                if (listener!= null){
                    listener.onComplete(v);

                }

            }
        });
    }


    public static void fadeInView(final View v, final  float from ,final int duration, final CollapseExpandListener listener) {

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {

                //v.setVisibility(View.GONE);

                Log.d("TAG", "onGlobalLayout: start animation");
                ObjectAnimator fadeIn = ObjectAnimator.ofFloat(v, "alpha", from, 1f);
                fadeIn.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.setAlpha(input );
                        return super.getInterpolation(input);
                    }
                });
                animationSet.play(fadeIn);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });
                animationSet.start();



            }
        });

    }



    public static void expandView(final View v, final int duration, final CollapseExpandListener listener) {

    Thread.runOnUi(new Runnable() {
        @Override
        public void run() {

            v.setVisibility(View.INVISIBLE);
            v.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            v.requestLayout();
            v.measure(ViewGroup.MeasureSpec.makeMeasureSpec(((ViewGroup)v.getParent()).getWidth(), ViewGroup.MeasureSpec.EXACTLY), ViewGroup.MeasureSpec.makeMeasureSpec(8000, ViewGroup.MeasureSpec.AT_MOST));
            final int targetHeight = v.getMeasuredHeight();
            v.setVisibility(View.VISIBLE);
            Log.d("TAG", "run: targetHeight" + targetHeight);
            v.getLayoutParams().height = 1;
            v.forceLayout();

            ObjectAnimator scaleY = ObjectAnimator.ofFloat(v, "scaleY", 0f, 1f);
            scaleY.setDuration(duration);
            final AnimatorSet animationSet = new AnimatorSet();
            animationSet.setInterpolator(new LinearInterpolator(){
                @Override
                public float getInterpolation(float input) {
                    v.getLayoutParams().height = (int)(input * (float) targetHeight);
                    if (v.getLayoutParams().height == 0)
                    {
                        v.getLayoutParams().height  = 1;
                    }
                    v.requestLayout();
                    return super.getInterpolation(1);
                }
            });
            animationSet.play(scaleY);

            animationSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    v.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    v.requestLayout();
                    if (listener != null){
                        Thread.runOnExecutor(new Runnable() {
                            @Override
                            public void run() {
                                listener.onComplete(v);
                            }
                        });

                    }
                }
            });

            Thread.waitAndRunOnUi(new Runnable() {
                @Override
                public void run() {
                    animationSet.start();

                }
            },1);
        }
    });

}


    public static void expandViewToMatchParent(final View v, final int duration, final CollapseExpandListener listener) {

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {

                v.setVisibility(View.INVISIBLE);
                v.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                v.requestLayout();
                v.measure(ViewGroup.MeasureSpec.makeMeasureSpec(((ViewGroup)v.getParent()).getWidth(), ViewGroup.MeasureSpec.EXACTLY), ViewGroup.MeasureSpec.makeMeasureSpec(8000, ViewGroup.MeasureSpec.AT_MOST));
                final int targetHeight = v.getMeasuredHeight();
                v.setVisibility(View.VISIBLE);

                ObjectAnimator scaleY = ObjectAnimator.ofFloat(v, "scaleY", 0f, 1f);
                scaleY.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.getLayoutParams().height = (int)(input * (float) targetHeight);
                        if (v.getLayoutParams().height == 0)
                        {
                            v.getLayoutParams().height  = 1;
                        }
                        v.requestLayout();
                        return super.getInterpolation(1f);
                    }
                });
                animationSet.play(scaleY);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                        v.requestLayout();
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });
                animationSet.start();
            }
        });

    }


    public static void scaleTo(final View v, final float scale, final int duration, final CollapseExpandListener listener) {

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {

                float initialScale = v.getScaleX();
                if (initialScale == 0){
                    initialScale = 1;
                }



                ObjectAnimator scaleY = ObjectAnimator.ofFloat(v, "scaleY", initialScale, scale);
                ObjectAnimator scaleX = ObjectAnimator.ofFloat(v, "scaleX", initialScale, scale);
                scaleY.setDuration(duration);
                scaleX.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                       // v.setScaleX(input);
                       // v.setScaleY(input);
                        return super.getInterpolation(input);
                    }
                });
                animationSet.play(scaleY);
                animationSet.play(scaleX);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.requestLayout();
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });
                animationSet.start();
            }
        });

    }


    public interface SelectedItemCallback{
        public void onSelected(int position);
    }

    public static void resizeRecyclerViewToHeight(final RecyclerView recyclerView, final int height, final int position, final SelectedItemCallback selectedItemCallback) {

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {



                final int initialHeight = recyclerView.getLayoutParams().height;



                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();



                boolean isCompletelyVisible = (linearLayoutManager.findFirstCompletelyVisibleItemPosition() <= position) &&
                        (linearLayoutManager.findLastCompletelyVisibleItemPosition() >= position);
                final int realPosition = height > initialHeight ? (
                        linearLayoutManager.findViewByPosition(position) != null ?
                                position:
                                linearLayoutManager.findFirstVisibleItemPosition()
                ):
                (
                        isCompletelyVisible == true ?
                                position:
                                linearLayoutManager.findViewByPosition(linearLayoutManager.findFirstCompletelyVisibleItemPosition()) != null?
                                    linearLayoutManager.findFirstCompletelyVisibleItemPosition():
                                    linearLayoutManager.findFirstVisibleItemPosition()
                );


                final View selectedView = linearLayoutManager.findViewByPosition(realPosition);

                if (height > initialHeight){
                    Log.d("TAG", "run: realposition enlarge");
                }else{
                    Log.d("TAG", "run: realposition reduce" );
                }
                Log.d("TAG", "run: realposition orig " + position);
                Log.d("TAG", "run: realposition " + realPosition);


                final float viewRatio = (float)selectedView.getMeasuredWidth() / (float) selectedView.getMeasuredHeight();
                final int viewFinalWidth = (int)(height * viewRatio);

                final int initialOffset = linearLayoutManager.getDecoratedLeft(selectedView);

                ObjectAnimator scaleY = ObjectAnimator.ofFloat(recyclerView, "scaleY", 0f, 1f);
                scaleY.setDuration(100);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){

                    @Override
                    public float getInterpolation(float input) {
                        recyclerView.getLayoutParams().height = initialHeight + (int)(input * (float) (height - initialHeight));
                        double newHeight = (double) initialHeight + ((double) input * (double) (height - initialHeight));
                        if (recyclerView.getLayoutParams().height == 0)
                        {
                            recyclerView.getLayoutParams().height  = 1;


                        }










                        int finalOffset = (recyclerView.getMeasuredWidth() - viewFinalWidth)/2;

                        int currentOffset = initialOffset - (int)((initialOffset - finalOffset) * input);

                        Log.d("TAG", "getInterpolation: initial offset " + initialOffset);
                        Log.d("TAG", "getInterpolation: final offset " + finalOffset);
                        Log.d("TAG", "getInterpolation: current offset " + currentOffset);
                        Log.d("TAG", "getInterpolation: view width " + viewFinalWidth);
                        Log.d("TAG", "getInterpolation: recycler width " + recyclerView.getMeasuredWidth());



                        if (position != -1) {
                            linearLayoutManager.scrollToPositionWithOffset(realPosition, currentOffset);
                            if (recyclerView instanceof ZoomRecyclerView){
                               // ((ZoomRecyclerView)recyclerView).setZoom();
                            }

                        }
                        recyclerView.requestLayout();
                        return super.getInterpolation(1);
                    }
                });
                animationSet.play(scaleY);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        selectedItemCallback.onSelected(realPosition);



                    }
                });
                animationSet.start();
            }
        });

    }



    public static void collapseViewHorizontal(final View v, final int duration, final  CollapseExpandListener listener) {
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {

                final int initialWidth = v.getMeasuredWidth();
                v.setVisibility(View.VISIBLE);
                ObjectAnimator scaleX = ObjectAnimator.ofFloat(v, "scaleX", 1f, 0f);
                scaleX.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.play(scaleX);
                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.setVisibility(View.GONE);
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });

                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.getLayoutParams().width = initialWidth - (int)(initialWidth * input);
                        if (v.getLayoutParams().width == 0)
                        {
                            v.getLayoutParams().width  = 1;
                        }
                        v.requestLayout();
                        return super.getInterpolation(0f);
                    }
                });

                animationSet.start();

            }
        });


    }




    public static void expandViewHorizontal(final View v, final int duration, final CollapseExpandListener listener) {

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {

                v.setVisibility(View.INVISIBLE);
                v.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                v.requestLayout();
                v.measure( ViewGroup.MeasureSpec.makeMeasureSpec(8000, ViewGroup.MeasureSpec.AT_MOST), ViewGroup.MeasureSpec.makeMeasureSpec(((ViewGroup)v.getParent()).getHeight(), ViewGroup.MeasureSpec.EXACTLY));
                final int targetWidth = v.getMeasuredWidth();
                v.setVisibility(View.VISIBLE);
                //v.getLayoutParams().width = targetWidth;

                //v.forceLayout();

                ObjectAnimator scaleX = ObjectAnimator.ofFloat(v, "scaleX", 0f, 1f);
                scaleX.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.getLayoutParams().width = (int)(input * (float) targetWidth);
                        if (v.getLayoutParams().width == 0)
                        {
                            v.getLayoutParams().width  = 1;
                        }
                        v.requestLayout();
                        return super.getInterpolation(1f);
                    }
                });
                animationSet.play(scaleX);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        //v.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                        //v.requestLayout();
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });
                animationSet.start();
            }
        });

    }



    public static void collapseTabHorizontal(final TabLayout tabLayout, final int duration, int tabPosition, final  CollapseExpandListener listener) {
        final View v = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(tabPosition);
        int tabCount = tabLayout.getTabCount() + 1;
        final int initialWidth = tabLayout.getWidth() / tabCount;
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {

                ((LinearLayout.LayoutParams)v.getLayoutParams()).weight = 0;
                v.forceLayout();

                //final int initialWidth = v.getMeasuredWidth();
                v.setVisibility(View.VISIBLE);
                ObjectAnimator scaleX = ObjectAnimator.ofFloat(v, "scaleX", 1f, 0f);
                scaleX.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.play(scaleX);
                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.setVisibility(View.GONE);
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });

                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.getLayoutParams().width = initialWidth - (int)(initialWidth * input);
                        if (v.getLayoutParams().width == 0)
                        {
                            v.getLayoutParams().width  = 1;
                        }
                        v.requestLayout();
                        return super.getInterpolation(0f);
                    }
                });

                animationSet.start();

            }
        });


    }



    public static void expandTabHorizontal(final TabLayout tabLayout, final int duration, int tabPosition, final CollapseExpandListener listener) {

        final View v = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(tabPosition);
        int tabCount = tabLayout.getTabCount();
        final int targetWidth = tabLayout.getWidth() / tabCount;

        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {

                v.setVisibility(View.INVISIBLE);
                ((LinearLayout.LayoutParams)v.getLayoutParams()).weight = 0;

                //v.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                //v.requestLayout();
                //v.measure( ViewGroup.MeasureSpec.makeMeasureSpec(8000, ViewGroup.MeasureSpec.AT_MOST), ViewGroup.MeasureSpec.makeMeasureSpec(((ViewGroup)v.getParent()).getHeight(), ViewGroup.MeasureSpec.EXACTLY));
                //final int targetWidth = v.getMeasuredWidth();
                v.setVisibility(View.VISIBLE);
                v.getLayoutParams().width = targetWidth;


                v.forceLayout();

                ObjectAnimator scaleX = ObjectAnimator.ofFloat(v, "scaleX", 0f, 1f);
                scaleX.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.getLayoutParams().width = (int)(input * (float) targetWidth);
                        if (v.getLayoutParams().width == 0)
                        {
                            v.getLayoutParams().width  = 1;
                        }
                        v.requestLayout();
                        return super.getInterpolation(1f);
                    }
                });
                animationSet.play(scaleX);

                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        //v.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                        //v.requestLayout();
                        ((LinearLayout.LayoutParams)v.getLayoutParams()).weight = 1;
                        ((LinearLayout.LayoutParams)v.getLayoutParams()).width = 0;
                        v.requestLayout();
                        if (listener != null){
                            Log.d("selectTab", "onAnimationEnd: ");
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("selectTab", "run: ");
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });
                animationSet.start();
            }
        });

    }

    public static void collapseView(final View v, final int duration, final  CollapseExpandListener listener) {
        Thread.runOnUi(new Runnable() {
            @Override
            public void run() {

                final int initialHeight = v.getMeasuredHeight();
                v.setVisibility(View.VISIBLE);
                ObjectAnimator scaleY = ObjectAnimator.ofFloat(v, "scaleY", 1f, 0f);
                scaleY.setDuration(duration);
                final AnimatorSet animationSet = new AnimatorSet();
                animationSet.play(scaleY);
                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.setVisibility(View.GONE);
                        if (listener != null){
                            Thread.runOnExecutor(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onComplete(v);
                                }
                            });

                        }
                    }
                });

                animationSet.setInterpolator(new LinearInterpolator(){
                    @Override
                    public float getInterpolation(float input) {
                        v.getLayoutParams().height = initialHeight - (int)(initialHeight * input);
                        if (v.getLayoutParams().height == 0)
                        {
                            v.getLayoutParams().height  = 1;
                        }
                        v.requestLayout();
                        return super.getInterpolation(0f);
                    }
                });

                animationSet.start();

            }
        });


    }

    public static Bitmap applyFilter(Context context, int srcResId, int maskResId, int width, int height, Matrix matrix){
        Bitmap bitmap1= BitmapFactory.decodeResource(context.getResources(), srcResId);
        return  applyFilter(context, bitmap1, maskResId, width, height, matrix);


    }


    public static Bitmap drawableToBitmap (Context context, int resId, int width, int height) {
        Drawable drawable = context.getResources().getDrawable(resId);
        return drawableToBitmap(context,drawable,width, height);
    }

    public static Bitmap drawableToBitmap (Context context, Drawable drawable, int width, int height) {



        if (drawable instanceof BitmapDrawable) {
            return (((BitmapDrawable)drawable).getBitmap());
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());


        drawable.draw(canvas);

        return bitmap;
    }


    public static Bitmap cloneBitmap(Context context, Bitmap input){

        Log.d("TAG", "cloneBitmap: " + input.getHeight() +  " w " + input.getWidth());
        Bitmap bitmap = Bitmap.createBitmap(input.getWidth(), input.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), input);
        bitmapDrawable.setBounds(0,0, input.getWidth(), input.getHeight());
        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        bitmapDrawable.draw(canvas);

        return bitmap;

    }


    public static Bitmap convertToAlphaMask(Bitmap mask){
        Bitmap alphaMask = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Bitmap.Config.ALPHA_8);
        Canvas canvas = new Canvas(alphaMask);
        canvas.drawBitmap(mask, 0.0f, 0.0f, null);
        return alphaMask;
    }


    public static Bitmap applyFilter(Context context, Bitmap bitmap, int maskResId, int width, int height, Matrix matrix){

        Bitmap bitmap2=  drawableToBitmap(context, maskResId, width, height);//  BitmapFactory.decodeResource(context.getResources(), maskResId);

        Bitmap bitmap1 = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        Log.d("TAG", "applyFilter: "+ bitmap2);

        Bitmap resultingImage=Bitmap.createBitmap(width, height, bitmap1.getConfig());

        Canvas canvas = new Canvas(resultingImage);

        Paint paint = new Paint();
        canvas.drawBitmap(convertToAlphaMask(bitmap2), 0, 0, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
        canvas.drawBitmap(bitmap1, 0, 0, paint);

        return resultingImage;
    }




    public static void loadRoundedCornersImage(String url, ImageView imageView, Quality quality, float cornerRadius, int imageWidth, int imageHeight) {
        if (url == null || url.trim().equalsIgnoreCase("") || imageView == null) return;
        synchronized (roundedCornersTransformation) {
            roundedCornersTransformationCornerRadius = cornerRadius;
            getRequestCreator(url, imageView, quality)
                    .transform(roundedCornersTransformation)
                    .resize(imageWidth, imageHeight)
                    .centerCrop()
                    .into(imageView);
        }
    }

    public static void loadRoundedCornersImageResource(int resId, ImageView imageView, Quality quality, float cornerRadius, int imageWidth, int imageHeight) {
        if (imageView == null) return;
        synchronized (roundedCornersTransformation) {
            roundedCornersTransformationCornerRadius = cornerRadius;
            getRequestCreatorResource(resId, imageView, quality)
                    .transform(roundedCornersTransformation)
                    .resize(imageWidth, imageHeight)
                    .centerCrop()
                    .into(imageView);
        }
    }




    public static  void loadImage(Context context,String url, ImageView imageView, Quality quality){
        int width = 0;
        int height = 0;
        switch (quality)
        {
            case RGB_565:
                break;
            case CONTAINER_565:
                width = imageView.getMeasuredWidth();
                height = imageView.getMeasuredHeight();
                break;
            case WIDTH_565:
                width = GUI.width;
                height = GUI.height;
                break;
            case HALF_WIDTH_565:
                width = GUI.width/2;
                height = GUI.height/2;
                break;
            case QUARTER_WIDTH_565:
                width = GUI.width/4;
                height = GUI.height/4;
                break;
        }
        if (width != 0) {
            Picasso.with(context).load(url).resize(width, height).centerCrop().into(imageView);
        }else{
            Picasso.with(context).load(url).into(imageView);
        }
    }


    public static void loadImageWithTransformation(final Context context, String url, ImageView imageView, Quality quality, final int maskResId/*, float cornerRadius, int imageWidth, int imageHeight*/) {
        if (url == null || url.trim().equalsIgnoreCase("") || imageView == null) return;
        synchronized (customShapeTransformation) {
            //roundedCornersTransformationCornerRadius = cornerRadius;
            GUI.maskResId = maskResId;
            shapeTransformationImageView = imageView;
            getRequestCreator(url, imageView, quality)
                    .transform(customShapeTransformation)
                    //.resize(imageWidth, imageHeight)
                   // .centerCrop()
                    .into(imageView);
        }
    }



    public static void blurInView(final Activity activity, final ViewGroup viewGroup, final BlurView blurView, final long duration){
        Thread.runOnExecutor(new Runnable() {
            @Override
            public void run() {


                for (int i = 1 ; i < 26 ; i++){

                    final int radius = i;
                    Thread.runOnUi(new Runnable() {
                        @Override
                        public void run() {
                            final Drawable windowBackground = ContextCompat.getDrawable(activity, R.drawable.big_heart);
                            Log.d("TAG", "run: setting rad" + radius);
                            blurView.setupWith(viewGroup)
                                    .windowBackground(windowBackground)
                                    .blurAlgorithm(new RenderScriptBlur(activity))
                                    .blurRadius(radius);

                            //blurView.updateBlur();
                        }
                    });


                    Thread.sleep(duration / 25);
                }
            }
        });

    }


    public static void showConfirmDialog(Context context, final String confirmationText, final Runnable action){


        final Dialog dialog = new Dialog(context){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
            }

            @Override
            public void onStart() {
                super.onStart();

                int height = GUI.height;
                int width = GUI.width;


                Log.d("TAG", "onStart: " + height);

                getWindow().setLayout(width, height);
            }


            @Override
            public void show() {
                super.show();
                getWindow().getDecorView().setAlpha(0);
                this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

                this.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    boolean started = false;
                    @Override
                    public void onGlobalLayout() {
                        Thread.waitAndRunOnUi(new Runnable() {
                            @Override
                            public void run() {
                                started = true;
                                //final View blurView = findViewById(R.id.blur_view);
                                GUI.fadeInView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, null);
                                GUI.shakeView(findViewById(R.id.main_view), 10, 5, MainActivity.shakeAnimationDuration, new GUI.CollapseExpandListener() {
                                    @Override
                                    public void onComplete(View view) {

                                    }
                                });
                            }
                        },MainActivity.defaultAnimationDuration / 3);
                        getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    }
                });

            }

            @Override
            protected void onStop() {
                super.onStop();
            }

            private void superDismiss(){
                super.dismiss();
            }

            @Override
            public void dismiss() {
                GUI.collapseView(findViewById(R.id.main_view),MainActivity.fastAnimationDuration, null);
                GUI.fadeOutView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                superDismiss();
                            }
                        });
                    }
                });
            }
        };
        dialog.setContentView(R.layout.confirm_dialog);
        dialog.setTitle("Title...");



        ImageButton closeButton = (ImageButton) dialog.findViewById(R.id.button_negative);
        ImageButton okButton = (ImageButton) dialog.findViewById(R.id.button_positive);
        TextView textView = (TextView)dialog.findViewById(R.id.text_view);
        textView.setText(confirmationText);
        // if button is clicked, close the custom dialog
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Thread.waitAndRunOnUi(action, MainActivity.defaultAnimationDuration);


            }
        });


        Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                dialog.show();
                dialog.getWindow().setLayout(GUI.width, GUI.height);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
        },(int)(1) );


    }



    public static void showAlert(Context context, final String confirmationText, final Runnable action){


        final Dialog dialog = new Dialog(context){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
            }

            @Override
            public void onStart() {
                super.onStart();

                int height = GUI.height;
                int width = GUI.width;


                Log.d("TAG", "onStart: " + height);

                getWindow().setLayout(width, height);
            }


            @Override
            public void show() {
                super.show();
                getWindow().getDecorView().setAlpha(0);
                this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

                this.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    boolean started = false;
                    @Override
                    public void onGlobalLayout() {
                        Thread.waitAndRunOnUi(new Runnable() {
                            @Override
                            public void run() {
                                started = true;
                                //final View blurView = findViewById(R.id.blur_view);
                                GUI.fadeInView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, null);
                                GUI.shakeView(findViewById(R.id.main_view), 10, 5, MainActivity.shakeAnimationDuration, new GUI.CollapseExpandListener() {
                                    @Override
                                    public void onComplete(View view) {

                                    }
                                });
                            }
                        },MainActivity.defaultAnimationDuration / 3);
                        getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    }
                });

            }

            @Override
            protected void onStop() {
                super.onStop();
            }

            private void superDismiss(){
                super.dismiss();
            }

            @Override
            public void dismiss() {
                GUI.collapseView(findViewById(R.id.main_view),MainActivity.fastAnimationDuration, null);
                GUI.fadeOutView(getWindow().getDecorView(), 0, MainActivity.fastAnimationDuration, new GUI.CollapseExpandListener() {
                    @Override
                    public void onComplete(View view) {
                        Thread.runOnUi(new Runnable() {
                            @Override
                            public void run() {
                                superDismiss();
                            }
                        });
                    }
                });
            }
        };
        dialog.setContentView(R.layout.alert_dialog);
        dialog.setTitle("Title...");



        ImageButton okButton = (ImageButton) dialog.findViewById(R.id.button_positive);
        TextView textView = (TextView)dialog.findViewById(R.id.text_view);
        textView.setText(confirmationText);
        // if button is clicked, close the custom dialog
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Thread.waitAndRunOnUi(action, MainActivity.defaultAnimationDuration);


            }
        });


        Thread.waitAndRunOnUi(new Runnable() {
            @Override
            public void run() {
                dialog.show();
                dialog.getWindow().setLayout(GUI.width, GUI.height);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
        },(int)(1) );


    }




}
