package com.meelo2.mobi;

/**
 * Created by jzvia on 18/12/2017.
 */

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.meelo2.mobi.Utils.Rest;
import com.meelo2.mobi.app.R;

import java.io.InputStream;


public class PdfFragment extends BaseNonRootFragment {

    private String title;
    private  String fileName;


    {
        toolbarResId = R.id.app_bar_layout;
    }




    public static void showPdf(Context context, String fileName, String title){

        final PdfFragment pdfFragment = new PdfFragment();
        pdfFragment.fileName = fileName;
        pdfFragment.title = title;
        pdfFragment.init(R.layout.pdf_fragment,context);
    }




    @Override
    protected void postShowInit() {}

    @Override
    protected void preShowInit() {

        ((TextView)getToolbar().findViewById(R.id.title_name)).setText(title);
        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    context.getPackageName(), 0);
            String version = info.versionName;
            ((TextView)getToolbar().findViewById(R.id.title_versoin)).setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        InputStream inputStream = context.getResources().openRawResource(
                context.getResources().getIdentifier(fileName,
                        "raw", context.getPackageName()));

        ((PDFView)rootView.findViewById(R.id.pdf_view)).fromStream(inputStream).load();
        ((TextView)getToolbar().findViewById(R.id.title_name)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditText)rootView.findViewById(R.id.edit_text_server)).setText(Rest.getServer());
                ((EditText)rootView.findViewById(R.id.edit_text_port)).setText(Rest.getPort());
                rootView.findViewById(R.id.server_layout).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.button_save).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Rest.setServer(((EditText)rootView.findViewById(R.id.edit_text_server)).getText().toString());
                        Rest.setPort(((EditText)rootView.findViewById(R.id.edit_text_port)).getText().toString());
                        rootView.findViewById(R.id.server_layout).setVisibility(View.GONE);
                    }
                });
                rootView.findViewById(R.id.button_defaults).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Rest.setDefaults();
                        rootView.findViewById(R.id.server_layout).setVisibility(View.GONE);
                    }
                });
            }
        });




    }
}



