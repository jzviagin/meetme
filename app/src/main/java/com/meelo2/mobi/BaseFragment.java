package com.meelo2.mobi;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.meelo2.mobi.GUI.UIcontroller;
import com.meelo2.mobi.app.R;

/**
 * Created by jzvia on 09/01/2018.
 */

public class BaseFragment extends Fragment{
    protected int toolbarResId = 0;
    protected View rootView;

    protected int defaultBackgroundResId = R.drawable.rain_window;

    //protected boolean usesCustomBackground = false;

    private View toolbarView;

    private Drawable drawable;

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }


    protected void createRootView(int resId, Context context, ViewGroup parent){
        rootView = (LayoutInflater.from(context).inflate(resId, parent, false));

        setBackground(context);
        if (toolbarView == null){
            toolbarView = rootView.findViewById(toolbarResId);
        }

        if (toolbarView != null){
            if (toolbarView.getParent() != null){
                ((ViewGroup) toolbarView.getParent()).removeView(toolbarView);

            }

        }

    }




    public boolean hasToolbar(){
        return toolbarResId != 0;
    }




    protected LinearLayout createEmptyLayout( LayoutInflater inflater, ViewGroup container){
        Log.d("TAG", "createEmptyLayout: ");

        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.empty_layout, container, false);

        return linearLayout;
    }


    public void scrollToBottom(){

    }


    public View getToolbar(){
        if (toolbarView == null){
            Log.d("TAG", "getToolbar: " + "toolbar is null");
        }
        return toolbarView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser == true ) {
            if (rootView!= null){
                rootView.setVisibility(View.VISIBLE);
            }
            setBackground(getContext());
        }else {
            if (rootView!= null){
                //rootView.setVisibility(View.INVISIBLE);
            }
        }

        Log.d("TAG", "setUserVisibleHint: "  + this + " state: " + isVisibleToUser);
    }


    protected final void setBackgroundDrawable(Context context, Drawable drawable){
        this.drawable = drawable;
        setBackground(context);
    }

    protected final Drawable getBackgroundDrawable(Context context) {
        if (drawable == null) {
            Log.d("TAG", "setBackground: " + "default");
            if (defaultBackgroundResId != 0) {
                Drawable drawable1 = ContextCompat.getDrawable(context, defaultBackgroundResId);
                return drawable1;
            }else{
                return null;
            }
           // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //    drawable1.setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            //}

        }else{
            Log.d("TAG", "setBackground: " + "not default");
            return drawable;
        }

    }

    protected final  void setBackground(Context context){
        if (context != null ) {
            Drawable drawable = getBackgroundDrawable(context);
            if (drawable != null) {

                Log.d("TAG", "setBackground: " + this + " drawable " + drawable);
                UIcontroller.getInstance().setBackground(drawable);
            }

        }
    }


    public void refreshContent(){

    }


}
